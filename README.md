Stitch iOS
==========
iOS client for Stitch.

Building
--------
1. Install Cocoapods if you don't have it: `sudo gem install cocoapods`
2. `cd Stitch`
3. `pod install`
4. Open Stitch.xcworkspace (the workspace, not the project)
5. Build and Run from Xcode.

Connecting to Development Server
--------------------------------
To connect to a development stitchd server from an iOS device, you must run an HTTP proxy that proxies requests to the name *dev* to the machine running a development version of stitchd.

Proxy
-----
1. Add an entry for *dev* in /etc/hosts to the IP address of the development machine running stitchd.
2. Install squid and run using: `./proxy.sh`.

Stitch Server
-------------
1. Run a development version of stitch on a machine that has the addressed pointed to by *dev* in the /etc/hosts files the squid proxy is running on.

iOS Device
----------
1. Set your iOS device's HTTP Proxy (in WiFi settings) to the IP address of the machine running squid and port 3128.

Xcode
-----
1. Update code in WebAPI.swift to point at https://dev:8080
2. Build and run on your iOS device.

To see the squid access log, run `tail -f /usr/local/var/logs/access.log`
