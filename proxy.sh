#!/bin/bash
set -e
brew install squid
/usr/local/opt/squid/sbin/squid -N -d 1 -f squid.conf
