//
//  RecentEventsTests.swift
//  Stitch
//
//  Created by Doug Richardson on 8/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import SwiftyUUID
import XCTest

@testable import Stitch

private func event(name : String, _ owner : String) -> Event {
    return Event(id: UUID().CanonicalString(), name: name, owner : owner, created : NSDate(), anyoneCanPost : true)
}

class RecentEventsTests: XCTestCase {
    
    var tmpDbFile : NSURL!
    var recents : RecentEvents!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        tmpDbFile = temporaryFilename()
        recents = RecentEvents(path: tmpDbFile)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        do {
            try NSFileManager.defaultManager().removeItemAtURL(tmpDbFile)
        } catch let e {
            print("Failed to remove tmp file \(tmpDbFile). \(e)")
        }
        tmpDbFile = nil
        recents = nil
        super.tearDown()
    }

    func testSaveAndLoad() {
        // Save and load one event
        let e1 = event("e1", "owner1_id")
        XCTAssertEqual(recents.allEvents().count, 0)
        recents.accessedEvent(e1)
        var events = recents.allEvents()
        XCTAssertEqual(events.count, 1)
        XCTAssertEqual(events[0], e1)
        
        // Save and load another event, verifying the older event appears later in the
        // all events array.
        let e2 = event("e2", "owner2_id")
        recents.accessedEvent(e2)
        events = recents.allEvents()
        XCTAssertEqual(events.count, 2)
        XCTAssertEqual(events[0], e2)
        XCTAssertEqual(events[1], e1)
        
        // Access e1 again, it should still appear at the end of the array.
        recents.accessedEvent(e1)
        XCTAssertEqual(events.count, 2)
        XCTAssertEqual(events[0], e2)
        XCTAssertEqual(events[1], e1)
    }
    
    func testRemove() {
        // Save and load one event
        let e1 = event("e1", "owner1_id")
        let e2 = event("e1", "owner1_id")
        XCTAssertEqual(recents.allEvents().count, 0)
        recents.accessedEvent(e1)
        recents.accessedEvent(e2)
        var events = recents.allEvents()
        XCTAssertEqual(events.count, 2)
        recents.removeEvent(e1)
        events = recents.allEvents()
        XCTAssertEqual(events.count,1)
        XCTAssertEqual(events[0], e2)
    }
}
