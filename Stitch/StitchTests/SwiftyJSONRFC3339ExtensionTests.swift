//
//  SwiftyJSONRFC3339ExtensionTests.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import SwiftyJSON
import XCTest
import UIKit

@testable import Stitch

class SwiftyJSONRFC3339ExtensionTests: XCTestCase {

    func testRFC3339() {
        let json : SwiftyJSON.JSON = ["time" : "2001-01-01T00:00:00+00:00"]
        let referenceDate = NSDate(timeIntervalSinceReferenceDate: 0)
        XCTAssertEqual(json["time"].dateRFC3339!, referenceDate)
    }
    
    func testRFC3339BadDates() {
        let json : SwiftyJSON.JSON = ["time" : "2001-01-01T00:00:00"]
        XCTAssertNil(json["time"].dateRFC3339)
    }
}
