//
//  RetryTests.swift
//  Stitch
//
//  Created by Doug Richardson on 8/20/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import XCTest

@testable import Stitch

class RetryTests: XCTestCase {

    func testRetryCount() {
        var counter = Int(0)
        BackoffRetryWithMaxTries(5, initialDelay: 0, maxDelay: 0) { (tryAgain) -> () in
            tryAgain()
            counter++
        }
        NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 0.1))
        XCTAssertEqual(counter, 5)
    }
    
    func testRetryDelay() {
        let start = CFAbsoluteTimeGetCurrent()
        var counter = 0
        BackoffRetryWithMaxTries(4, initialDelay: 0.01, maxDelay: 0.01) { (tryAgain) -> () in
            tryAgain()
            counter++
            if counter == 4 {
                CFRunLoopStop(CFRunLoopGetCurrent())
            }
        }
        CFRunLoopRun()
        let end = CFAbsoluteTimeGetCurrent()
        let diff = end - start
        XCTAssertGreaterThanOrEqual(diff, 0.03) // 0.03 because we only delay 3 times for 4 retries (i.e., no delay before first try).
    }
    
    func testRetryExponentialBackkoff() {
        let start = CFAbsoluteTimeGetCurrent()
        var counter = 0
        
        // 0 + 0.01 + 0.02 + 0.04 + 0.08 + 0.1 + 0.1
        
        var one = CFAbsoluteTime(0)
        var two = CFAbsoluteTime(0)
        var three = CFAbsoluteTime(0)
        var four = CFAbsoluteTime(0)
        var five = CFAbsoluteTime(0)
        var six = CFAbsoluteTime(0)
        var seven = CFAbsoluteTime(0)
        
        BackoffRetryWithMaxTries(7, initialDelay: 0.01, maxDelay: 0.1) { (tryAgain) -> () in
            counter++
            switch counter {
            case 1:
                one = CFAbsoluteTimeGetCurrent()
            case 2:
                two = CFAbsoluteTimeGetCurrent()
            case 3:
                three = CFAbsoluteTimeGetCurrent()
            case 4:
                four = CFAbsoluteTimeGetCurrent()
            case 5:
                five = CFAbsoluteTimeGetCurrent()
            case 6:
                six = CFAbsoluteTimeGetCurrent()
            case 7:
                seven = CFAbsoluteTimeGetCurrent()
                CFRunLoopStop(CFRunLoopGetCurrent())
                return
            default:
                assert(false)
            }
            tryAgain()
        }
        CFRunLoopRun()
        XCTAssertGreaterThanOrEqual(one - start, 0.00)
        XCTAssertGreaterThanOrEqual(two - one, 0.01)
        XCTAssertGreaterThanOrEqual(three - two, 0.02)
        XCTAssertGreaterThanOrEqual(four - three, 0.04)
        XCTAssertGreaterThanOrEqual(five - four, 0.08)
        XCTAssertGreaterThanOrEqual(six - five, 0.1)
        XCTAssertGreaterThanOrEqual(seven - six, 0.1)
    }

}
