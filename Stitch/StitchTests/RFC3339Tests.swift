//
//  RFC3339Tests.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import XCTest

@testable import Stitch


func RFC3339StringToDate(string : String) -> NSDate? {
    return RFC3339Formatter.dateFromString(string)
}

func RFC3339FracStringToDate(string : String) -> NSDate? {
    return RFC3339FractionalSecondsFormatter.dateFromString(string)
}

class RFC3339Tests: XCTestCase {

    func testNoFractionalSeconds() {
        let referenceDate = NSDate(timeIntervalSinceReferenceDate: 0)
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00Z")!, referenceDate)
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00z")!, referenceDate)
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00+00:00")!, referenceDate)
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00-00:00")!, referenceDate)
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00+01:00")!, referenceDate.dateByAddingTimeInterval(-3600))
        XCTAssertEqual(RFC3339StringToDate("2001-01-01T00:00:00-01:00")!, referenceDate.dateByAddingTimeInterval(3600))
    }
    
    func testFractionalSeconds() {
        let referenceDate = NSDate(timeIntervalSinceReferenceDate: 0)
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000Z")!, referenceDate)
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000z")!, referenceDate)
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000+00:00")!, referenceDate)
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000-00:00")!, referenceDate)
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000+01:00")!, referenceDate.dateByAddingTimeInterval(-3600))
        XCTAssertEqual(RFC3339FracStringToDate("2001-01-01T00:00:00.000000000-01:00")!, referenceDate.dateByAddingTimeInterval(3600))
        XCTAssertEqualWithAccuracy(RFC3339FracStringToDate("2001-01-01T00:00:00.100000000Z")!.timeIntervalSinceReferenceDate, 0.1, accuracy: 0.0005)
        XCTAssertEqualWithAccuracy(RFC3339FracStringToDate("2001-01-01T00:00:00.010000000Z")!.timeIntervalSinceReferenceDate, 0.01, accuracy: 0.0005)
        XCTAssertEqualWithAccuracy(RFC3339FracStringToDate("2001-01-01T00:00:00.001000000Z")!.timeIntervalSinceReferenceDate, 0.001, accuracy: 0.0005)
        XCTAssertEqualWithAccuracy(RFC3339FracStringToDate("2001-01-01T00:00:00.001Z")!.timeIntervalSinceReferenceDate, 0.001, accuracy: 0.0005)
    }
    
    func testCombinedParser() {
        let referenceDate = NSDate(timeIntervalSinceReferenceDate: 0)
        XCTAssertEqual(DateFromRFC3339String("2001-01-01T00:00:00.000000000Z")!, referenceDate)
        XCTAssertEqual(DateFromRFC3339String("2001-01-01T00:00:00Z")!, referenceDate)
    }
    
    func testBadDates() {
        XCTAssertNil(RFC3339StringToDate(""))
        XCTAssertNil(RFC3339StringToDate("2010-10-10"))
        XCTAssertNil(RFC3339StringToDate("10:11:12"))
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12")) // no timezone
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12Z1")) // junk after timezone
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12+10:000")) // junk after numeric timezone
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12+100:00")) // junk numeric timezone
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:1210:00")) // missing + or -
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12+0:0")) // junk numeric timezone
        XCTAssertNil(RFC3339StringToDate("2001-01-01t00:00:00Z")) // lower case t not allowed (this is more restrictive than RFC 3339).
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12-a")) // junk timezone
        XCTAssertNil(RFC3339StringToDate("2010-10-10T10:11:12-:")) // junk timezone
    }
    
    func testBadDatesFrac() {
        XCTAssertNil(RFC3339FracStringToDate("2001-01-01T00:00:00.Z")) // malformed fractional part
        XCTAssertNil(RFC3339FracStringToDate("2001-01-01T00:00:00Z")) // missing fractional part
    }
}
