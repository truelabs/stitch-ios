//
//  Analytics.swift
//  Stitch
//
//  Created by John Hwang on 7/16/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

// Bitcode disabled because of Google Analytics bitcode issue with Xcode 7:
// https://code.google.com/p/analytics-issues/issues/detail?id=671

class Analytics {
    private let tracker : GAITracker
    
    static let sharedInstance = {
        
        return Analytics()
        }()
    
    private init() {
        
        
        #if DEBUG
            let trackingID = "UA-60108717-4"
            let interval : NSTimeInterval = 20
            #else
            let trackingID = "UA-60108717-5"
            let interval : NSTimeInterval = 120
        #endif
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.dispatchInterval = interval
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        gai.logger = GAI_XCG_LogAdapter(logLevel: .Warning)
        
        tracker = gai.trackerWithTrackingId(trackingID)
        
    }
    
    func screenView(screenName : String) {
        tracker.set(kGAIScreenName, value: screenName)
        tracker.send(GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject])
    }
    
    func eventWithCategory(category : EventCategory, action : String, label : String?, value : Int? = nil) {
        let event = GAIDictionaryBuilder.createEventWithCategory(category.rawValue, action: action, label: label, value: value)
        tracker.send(event.build() as [NSObject : AnyObject])
    }
}

enum EventCategory : String {
    case UIAction = "ui_action"
    case UINotification = "ui_notification"
    case Settings = "settings"
}

class GAI_XCG_LogAdapter : NSObject, GAILogger {
    
    var logLevel : GAILogLevel
    
    init(logLevel : GAILogLevel) {
        self.logLevel = logLevel
    }
    
    func verbose(message: String!) {
        if logLevel.rawValue < GAILogLevel.Verbose.rawValue {
            return
        }
        log.debug(message)
    }
    
    func info(message: String!) {
        if logLevel.rawValue < GAILogLevel.Info.rawValue {
            return
        }
        log.info(message)
    }
    
    func warning(message: String!) {
        if logLevel.rawValue < GAILogLevel.Warning.rawValue {
            return
        }
        log.warning(message)
    }
    
    func error(message: String!) {
        if logLevel.rawValue < GAILogLevel.Error.rawValue  {
            return
        }
        log.error(message)
    }
}
