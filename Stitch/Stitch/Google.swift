//
//  Google.swift
//  Stitch
//
//  Created by Doug Richardson on 10/9/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Foundation

private var configured : Bool = false

// Configure the GGLContext. This should be done once (and will assert if called
// again). This method performs configuration required by Analytics and
// Google Cloud Messaging.
func ConfigureGoogleAPI() {
    assert(!configured, "Do not try to configure more than once.")
    configured = true
    
    // Configure tracker from GoogleService-Info.plist.
    var configureError : NSError?
    GGLContext.sharedInstance().configureWithError(&configureError)
    assert(configureError == nil, "Error configuring Google services: \(configureError)")
    if let e = configureError {
        log.severe("Error configuring google services. \(e)")
    }
}
