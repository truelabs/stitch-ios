//
//  EventListTableViewController.swift
//  Stitch
//
//  Created by Douglas Richardson on 4/29/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import iAd

final class EventListTableViewController: UITableViewController {
    
    var iAdOn = false
    var addButton: UIBarButtonItem!
    var editButton: UIBarButtonItem!
    var doneButton: UIBarButtonItem!
    var settingsButton: UIBarButtonItem!
    
    private let NearbyEventCell = TableViewCellReuseIdAndType<UITableViewCell>(reuseId: "NearbyEventCell")
    private let EventCell = TableViewCellReuseIdAndType<UITableViewCell>(reuseId: "EventCell")
    private let PastEventCell = NibTableViewCellReuseIdAndType<EventListTableViewCell>(reuseId: "PastEventCell")
    private let SettingsCell = TableViewCellReuseIdAndType<UITableViewCell>(reuseId: "SettingsCell")
    
    private enum Section {
        case Events
        case PastEvents
        case AddEvent
        case Miscellaneous
    }
    
    var recentEvents : [Event] = []
    
    private enum Miscellaneous {
        case Settings
    }
    
    private let miscellaneous : [Miscellaneous] = []
    
    private var sections : [Section] = [.AddEvent, .Miscellaneous]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearsSelectionOnViewWillAppear = true
        PastEventCell.registerNib(tableView)
        
        navigationItem.title = LocalizedString("EVENTS")
        addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("addEvent"))
        editButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: Selector("editEvent"))
        doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("doneEditing"))
        settingsButton = UIBarButtonItem(image: UIImage(named: "SettingsToolbarIcon"), style: .Plain, target: self, action: Selector("editSettings"))
        navigationItem.rightBarButtonItem = settingsButton
        navigationItem.leftBarButtonItem = editButton
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Google Analytics
        Analytics.sharedInstance.screenView("Event-Home")
        
        reloadRecents()
        checkAdvertisements()
        
        //iAd code
        self.canDisplayBannerAds = iAdOn
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Events:
            return 1
        case .PastEvents:
            return recentEvents.count
        case .AddEvent:
            return 1
        case .Miscellaneous:
            return miscellaneous.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch sections[indexPath.section] {
        case .Events:
            let cell = NearbyEventCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("NEARBY_EVENTS")
            let imageLocation = UIImage(named: "Location")
            cell.imageView?.image = imageLocation
            cell.accessoryType = .DisclosureIndicator
            return cell
        case .PastEvents:
            let cell = PastEventCell.dequeueFromTableView(tableView)
            cell.eventLabel?.text = recentEvents[indexPath.row].name
            let placeholderImage = UIImage(named: "Photos")
            var thumbnailURL : NSURL?
            if let url = RecentEvents.sharedInstance.getThumbnailURLForEventId(recentEvents[indexPath.row].id){
                thumbnailURL = NSURL(string: url)
            }
            
            if let url = thumbnailURL {
                cell.thumbnailImage.af_setImageWithURL(url, placeholderImage: placeholderImage)
            } else {
                cell.thumbnailImage.af_cancelImageRequest()
                cell.thumbnailImage.image = placeholderImage
            }
            
            cell.accessoryType = .DisclosureIndicator
            cell.editingAccessoryType = .DetailButton
            return cell
        case .AddEvent:
            let cell = EventCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("ADD_EVENT")
            cell.textLabel?.textAlignment = .Center
            cell.textLabel?.textColor = tableView.tintColor
            return cell
        case .Miscellaneous:
            let cell = SettingsCell.dequeueFromTableView(tableView)
            switch(miscellaneous[indexPath.row]) {
            case .Settings:
                cell.textLabel?.text = LocalizedString("SETTINGS")
                cell.accessoryType = .DisclosureIndicator
                return cell
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .Events:
            return false
        case .PastEvents:
            return true
        case .AddEvent:
            return false
        case .Miscellaneous:
            switch(miscellaneous[indexPath.row]) {
            case .Settings:
                return false
            }
        }
    }
    
    override func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        switch sections[indexPath.section] {
        case .Events:
            return nil
        case .PastEvents:
            return LocalizedString("LEAVE_EVENT")
        case .AddEvent:
            return nil
        case .Miscellaneous:
            switch(miscellaneous[indexPath.row]) {
            case .Settings:
                return nil
            }
        }
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section] {
        case .PastEvents:
            let event = recentEvents[indexPath.row]
            let vc = EventDetailsTableViewController(mode: EventDetailsTableViewController.Mode.Edit(event))
            vc.notificationsEnabled = RecentEvents.sharedInstance.getSubscribedForEventId(event.id)
            vc.delegate = self
            let nav = UINavigationController(rootViewController: vc)
            presentViewController(nav, animated: true, completion: nil)
        default:
            break
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch sections[indexPath.section] {
        case .PastEvents:
            return 70
        default:
            return tableView.rowHeight
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            tableView.beginUpdates()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            //Remove event from recent events
            RecentEvents.sharedInstance.removeEvent(recentEvents[indexPath.row])
            self.recentEvents.removeAtIndex(indexPath.row)
            tableView.endUpdates()
            //Disable edit button if there are no events to edit
            if recentEvents.count == 0 {
                reloadRecents()
                doneEditing()
            }
        default:
            assert(false)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section] {
        case .Events:
//            let vc = NearbyEventListTableViewController()
//            navigationController?.pushViewController(vc, animated: true)
            break
        case .PastEvents:
            let vc = ActivityFeedViewController(event: recentEvents[indexPath.row])
            navigationController?.pushViewController(vc, animated: true)
        case .AddEvent:
            addEvent()
        case .Miscellaneous:
            switch(miscellaneous[indexPath.row]) {
            case .Settings:
                let vc = SettingsTableViewController()
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
//    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch sections[section] {
//        case .Events:
//            return LocalizedString("ALL_EVENTS_TITLE")
//        case .PastEvents:
//            return LocalizedString("PAST_EVENTS_TITLE")
//        case .AddEvent:
//            return nil
//        case .Miscellaneous:
//            return nil
//        }
//    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section] {
        case .Events:
            return nil
        case .PastEvents:
            return nil
        case .AddEvent:
            return LocalizedString("FIND_LINK")
        case .Miscellaneous:
            return nil
        }
    }
    
    private func pushEvent(event : Event) {
        let vc = ActivityFeedViewController(event:event)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Events
    func addEvent() {
        let vc = EventDetailsTableViewController(mode: .Create)
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_add_event", label: "event_home")
        presentViewController(nav, animated: true, completion: nil)
    }
    
    func editEvent(){
        setEditing(true)
        navigationItem.leftBarButtonItem = doneButton
    }
    
    func doneEditing(){
        setEditing(false)
        navigationItem.leftBarButtonItem = editButton
    }
    
    func editSettings(){
        let vc = SettingsTableViewController()
        let subNav = UINavigationController(rootViewController: vc)
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("settingsDone"))
        self.presentViewController(subNav, animated: true, completion: nil)
    }
    
    func settingsDone(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func setEditing(editing : Bool) {
        tableView.setEditing(editing, animated: true)
        addButton.enabled = !editing

    }
    
    // MARK: Data fetching
    func reloadRecents() {
        recentEvents = RecentEvents.sharedInstance.allEvents() ?? []
        let existingRecentEventIndex = sections.indexOf({$0 == .PastEvents})
        
        if recentEvents.count > 0 {
            //Enable edit button if there are events to edit
            editButton.enabled = true
            
            // Have recents. If the section is there, reload it; otherwise, insert it.
            if let recentEventIndex = existingRecentEventIndex {
                // Recents section already exists, reload it.
                tableView.reloadSections(NSIndexSet(index: recentEventIndex), withRowAnimation: .None)
            } else {
                // Need to add recents. Put before Miscellaneous, which is at the end.
                if let miscIndex = sections.indexOf({$0 == .Miscellaneous}) {
                    tableView.beginUpdates()
                    sections.insert(.PastEvents, atIndex: miscIndex)
                    tableView.insertSections(NSIndexSet(index: miscIndex), withRowAnimation: .None)
                    tableView.endUpdates()
                } else {
                    assert(false, "Couldn't find Miscellaneous index")
                }
            }
        } else {
            //Disable edit button if there are no events to edit
            editButton.enabled = false
            
            if let idx = existingRecentEventIndex {
                // Recents is empty but there is a recents section in the table view, so remove it.
                tableView.beginUpdates()
                sections.removeAtIndex(idx)
                tableView.deleteSections(NSIndexSet(index: idx), withRowAnimation: UITableViewRowAnimation.None)
                tableView.endUpdates()
            }
        }
    }
    
    func checkAdvertisements() {
        //log.debug("Asking about admob")
        ClientConfig.sharedInstance.admobEnabled { (enabled) -> () in
            //log.debug("Show Admob: \(enabled)")
        }
        
        //log.debug("Asking about iAd")
        ClientConfig.sharedInstance.iAdEnabled { (enabled) -> () in
            log.debug("Show iAds: \(enabled)")
            self.iAdOn = enabled
        }
    }
    
}

// MARK: EventDetailsTableViewControllerDelegate
extension EventListTableViewController : EventDetailsTableViewControllerDelegate {
    func eventDetails(controller: EventDetailsTableViewController, didUpdateEvent event : Event) {
        
        RecentEvents.sharedInstance.accessedEvent(event)
        RecentEvents.sharedInstance.setSubscribed(controller.notificationsEnabled, forEventId: event.id)
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        switch controller.mode {
        case .Create:
            // TODO: Insert recent instead of reloading everything. Or maybe just reload in viewDidDisappear.
            pushEvent(event)
        case .Edit(_):
            // TODO: Reload single recent that changed, not entire section.
            reloadRecents()
        }
    }
    
    func eventDetailsDidCancel(controller: EventDetailsTableViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
