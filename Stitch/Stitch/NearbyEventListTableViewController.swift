//
//  NearbyEventListTableViewController.swift
//  Stitch
//
//  Created by John Hwang on 7/8/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON

class NearbyEventListTableViewController: UITableViewController, EventDetailsTableViewControllerDelegate {

    private let EventCell = TableViewCellReuseIdAndType<UITableViewCell>(reuseId: "EventCell")
    
    var events : [Event] = []

	init() {
		super.init(style: .Grouped)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	required init!(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        clearsSelectionOnViewWillAppear = true
        
        navigationItem.title = LocalizedString("NEARBY_EVENTS")
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("addEvent"))
        navigationItem.rightBarButtonItem = addButton
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: Selector("refreshEvents"), forControlEvents: .ValueChanged)
        self.refreshControl = refreshControl
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
		
		// Google Analytics
		Analytics.sharedInstance.screenView("Event-Nearby")
		
		refreshEvents()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = EventCell.dequeueFromTableView(tableView)
        cell.textLabel?.text = events[indexPath.row].name
        cell.accessoryType = .DisclosureIndicator
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		pushEvent(events[indexPath.row])
    }

	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return LocalizedString("NEARBY_HEADER")
	}
	
	override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		return LocalizedString("NEARBY_FOOTER")
	}
	
    private func pushEvent(event : Event) {
        //let vc = EventCollectionViewController(event: event)
        //navigationController?.pushViewController(vc, animated: true)
		log.debug("PUSH EVENT TODO")
    }

    // MARK: Events
	
	func enterPin() {
		let alert = UIAlertController(title: LocalizedString("ENTER_PIN"), message: nil, preferredStyle: .Alert)
		let join = UIAlertAction(title: LocalizedString("JOIN"), style: UIAlertActionStyle.Default) { (action) -> Void in
//			if let tf = alert.textFields?.first {
//				
//				//let pin = tf.text
//				//TODO: Check if event pin is correct with backend
//				
//			}
		}
		let cancel = UIAlertAction(title: LocalizedString("CANCEL"), style: UIAlertActionStyle.Cancel, handler: nil)
		alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
			textField.placeholder = LocalizedString("EVENT_PIN")
			textField.keyboardType = UIKeyboardType.PhonePad
			textField.secureTextEntry = true
			textField.text = nil
		}
		alert.addAction(cancel)
		alert.addAction(join)
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
    func addEvent() {
        let vc = EventDetailsTableViewController(mode: .Create)
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
		//Google Analytics
		Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_add_event", label: "event_nearby")
        presentViewController(nav, animated: true, completion: nil)
    }
	
    // MARK: EventDetailsTableViewControllerDelegate
    func eventDetails(controller: EventDetailsTableViewController, didUpdateEvent event : Event, newEvent : Bool) {
        // TODO: Try putting pushEventName into completion handler for dismiss to see what kind of
        // visual effect you get.
        controller.dismissViewControllerAnimated(true, completion: nil)
        pushEvent(event)
    }
    
    func eventDetailsDidCancel(controller: EventDetailsTableViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Data fetching
    func refreshEvents() {
        
        log.debug("Refreshing Events")
        LocationManager.sharedInstance.currentLocation { (location, alert) -> () in
            if let a = alert {
                self.navigationController?.presentViewController(a, animated: true, completion: nil)
                self.refreshControl?.endRefreshing()
                return
            }
            
            if let coord = location?.coordinate {
                let url = WebAPI.EventsURLNearLatitude(coord.latitude, longitude: coord.longitude)
                WebAPI.manager.request(.GET, url).responseSwiftyJSON {
                    (req, res, json, error) in
                    
                    if let a = json?.array {
                        var events : [Event] = []
                        for jsonEvent in a {
                            if let e = Event(json: jsonEvent) {
                                events.append(e)
                            }
                        }
                        self.events = events
                        self.tableView.reloadData()
                    } else {
                        log.error("Error getting event list. \(error)")
                    }
                    self.refreshControl?.endRefreshing()
                }
            } else {
                log.error("Unexpectedly didn't get location. \(location)")
            }
        }
    }
}
