//
//  Video.swift
//  Stitch
//
//  Created by Doug Richardson on 9/23/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import SwiftyJSON

struct Video {
    let contentType : String
    let width : Int
    let height : Int
    let bitsPerSecond : Int
    let URL : String
    
    init(contentType : String, width : Int, height : Int, bitsPerSecond : Int, URL : String) {
        self.contentType = contentType
        self.width = width
        self.height = height
        self.bitsPerSecond = bitsPerSecond
        self.URL = URL
    }
}

extension Video {
    init?(json: JSON?) {
        if let contentType = json?["content_type"].string {
            self.contentType = contentType
        } else {
            log.error("Video.init(json:): missing string url")
            return nil
        }
        
        if let width = json?["width"].int {
            self.width = width
        } else {
            log.error("Video.init(json:): missing int width")
            return nil
        }
        
        if let height = json?["height"].int {
            self.height = height
        } else {
            log.error("Video.init(json:): missing int height")
            return nil
        }
        
        if let bitsPerSecond = json?["bits_per_second"].int {
            self.bitsPerSecond = bitsPerSecond
        } else {
            log.error("Video.init(json:): missing int bitsPerSecond")
            return nil
        }
        
        if let url = json?["url"].string {
            self.URL = url
        } else {
            log.error("Video.init(json:): missing string url")
            return nil
        }
    }
}

extension Video : BestAssetCandidate {
}


