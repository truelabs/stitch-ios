//
//  UTI.swift
//  Stitch
//
//  Created by Doug Richardson on 9/29/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import MobileCoreServices

func UTIFromMime(mime : String) -> String? {
    guard let identifier = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mime, nil) else {
        return nil
    }
    
    let id = identifier.takeRetainedValue()
    return id as String
}

func ExtensionFromUTI(uti : String) -> String? {
    guard let ext = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassFilenameExtension) else {
        return nil
    }
    
    let e = ext.takeRetainedValue()
    return e as String
}

func UTIFromExtension(ext : String, conformingToUTI : String?) -> String? {
    guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, conformingToUTI) else {
        return nil
    }
    let cfUti = uti.takeRetainedValue()
    return cfUti as String
}