//
//  EventListTableViewCell.swift
//  Stitch
//
//  Created by John Hwang on 9/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell, NibTableViewCell {

    
    @IBOutlet var thumbnailImage: UIImageView!
    @IBOutlet var eventLabel: UILabel!
    
    static func nibName() -> String {
        return "EventListTableViewCell"
    }
}
