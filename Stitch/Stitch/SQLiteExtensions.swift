//
//  DatabaseExtensions.swift
//  Stitch
//
//  Created by Doug Richardson on 9/24/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import SQLite

// https://github.com/stephencelis/SQLite.swift/issues/194
extension Connection {
    var userVersion: Int {
        get { return Int(scalar("PRAGMA user_version") as! Int64) }
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}