//
//  PhotoCell.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/28/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import Alamofire

final class PhotoCell: UICollectionViewCell {
    let imageView = UIImageView()
    let indicatorView = UIImageView()
    private let selectionOverlay = UIView()
    private let selectionCheck = UIImageView()
    private let highlightOverlay = UIView()
    
    var request : Request?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        
        indicatorView.contentMode = .ScaleAspectFit
        
        selectionOverlay.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
        selectionCheck.image = UIImage(named: "Check")
        
        highlightOverlay.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.25)
        
        resetAll()
        
        contentView.addSubview(imageView)
        contentView.addSubview(indicatorView)
        contentView.addSubview(selectionOverlay)
        contentView.addSubview(selectionCheck)
        contentView.addSubview(highlightOverlay)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func resetAll() {
        imageView.image = nil
        indicatorView.image = nil
        request?.cancel()
        request = nil
        selectionOverlay.hidden = true
        selectionCheck.hidden = true
        highlightOverlay.hidden = true
    }
    
    override func prepareForReuse() {
        //log.debug("Prepare for reuse. Existing request: \(request)")
        resetAll()
    }
    
    override func layoutSubviews() {
        let b = contentView.bounds
        imageView.frame = b
        
        let isize = CGSizeMake(24, 24)
        let iframe = CGRectMake(CGRectGetMaxX(b)-isize.width, CGRectGetMaxY(b)-isize.height, isize.width, isize.height);
        indicatorView.frame = iframe
        
        selectionOverlay.frame = b
        
        let csize = selectionCheck.image?.size ?? CGSizeZero
        let pad = CGFloat(4.0)
        let cframe = CGRectMake(CGRectGetMaxX(b)-csize.width-pad, CGRectGetMaxY(b)-csize.height-pad, csize.width, csize.height);
        selectionCheck.frame = cframe
        
        highlightOverlay.frame = b
    }
    
    override var selected: Bool {
        didSet {
            selectionOverlay.hidden = !selected
            selectionCheck.hidden = !selected
        }
    }
    
    override var highlighted: Bool {
        didSet {
            highlightOverlay.hidden = !highlighted
        }
    }
}