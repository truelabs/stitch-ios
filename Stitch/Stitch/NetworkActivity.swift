//
//  NetworkActivity.swift
//  Stitch
//
//  Created by Douglas Richardson on 4/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

// each call to networkActivityStarted must be balanced by networkActivityStopped
private var networkActivityStartedCount = 0

func networkActivityStarted() {
    assert(NSThread.isMainThread())
    networkActivityStartedCount++
    UIApplication.sharedApplication().networkActivityIndicatorVisible = networkActivityStartedCount > 0
}

func networkActivitedStopped() {
    assert(NSThread.isMainThread())
    networkActivityStartedCount--
    if networkActivityStartedCount < 0 {
        log.error("Unbalanced network activity calls. \(networkActivityStartedCount)")
    }
    UIApplication.sharedApplication().networkActivityIndicatorVisible = networkActivityStartedCount > 0
}
