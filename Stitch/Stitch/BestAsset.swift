//
//  BestAsset.swift
//  Stitch
//
//  Created by Doug Richardson on 9/23/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

protocol BestAssetCandidate {
    // The width, in pixels.
    var width : Int { get }
}

func BestAssetForPixelWidth<T:BestAssetCandidate>(pixelWidth : Int, candidates : [T]) -> T? {
    let widthDifferences = candidates.map { (abs(pixelWidth - $0.width), $0) }
    let smallestDifference = widthDifferences.minElement { $0.0 < $1.0 }
    return smallestDifference?.1
}

func BestAssetForPointWidth<T:BestAssetCandidate>(pointWidth : CGFloat, candidates : [T], contentScaleFactor : CGFloat) -> T? {
    let pixelWidth = Int(pointWidth * contentScaleFactor)
    return BestAssetForPixelWidth(pixelWidth, candidates: candidates)
}

extension UIView {
    func bestAssetForPointWidth<T:BestAssetCandidate>(pointWidth : CGFloat, candidates : [T]) -> T? {
        
        let scale : CGFloat
        if let s = self.window?.screen.scale {
            scale = s
        } else {
            //log.debug("View is not in a window, using main screen scale")
            scale = UIScreen.mainScreen().scale
        }
        
        return BestAssetForPointWidth(pointWidth, candidates: candidates, contentScaleFactor: scale)
    }
}
