//
//  CollectionViewCellReuseIdAndType.swift
//  Stitch
//
//  Created by Doug Richardson on 8/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol CollectionViewCellReuse {
    typealias CellType : UICollectionViewCell
    func register(collectionView : UICollectionView!)
    func dequeueCell(collectionView : UICollectionView, indexPath : NSIndexPath) -> CellType
}

struct CollectionViewCellReuseIdAndType<T:UICollectionViewCell> : CollectionViewCellReuse {
    let reuseId : String
    
    typealias CellType = T
    
    init(reuseId: String) {
        self.reuseId = reuseId
    }
    
    func register(collectionView : UICollectionView!) {
        collectionView.registerClass(T.self, forCellWithReuseIdentifier: reuseId)
    }
    
    func dequeueCell(collectionView : UICollectionView, indexPath : NSIndexPath) -> T {
        // If you get a crash on the next line you probably forgot to call register
        let cell : AnyObject = collectionView.dequeueReusableCellWithReuseIdentifier(reuseId, forIndexPath: indexPath)
        return cell as! T
    }
}

struct NibCollectionViewCellReuseIdAndType<T where T:NibCollectionViewCell, T:UICollectionViewCell> : CollectionViewCellReuse {
    let reuseId : String
    let nibViewSize : CGSize
    
    typealias CellType = T
    
    init(reuseId: String) {
        self.reuseId = reuseId
        
        let nib = UINib(nibName: T.nibName(), bundle: nil)
        let topLevelObjects = nib.instantiateWithOwner(nil, options: nil)
        assert(topLevelObjects.count == 1, "nibs contain 1 top level UICollectionViewCell object")
        let cell = topLevelObjects.first as! UIView
        nibViewSize = cell.bounds.size
    }
    
    func register(collectionView : UICollectionView!) {
        collectionView.registerNib(UINib(nibName: T.nibName(), bundle: nil), forCellWithReuseIdentifier: reuseId)
    }
    
    func dequeueCell(collectionView : UICollectionView, indexPath : NSIndexPath) -> T {
        // If you get a crash on the next line you probably forgot to call register
        let cell : AnyObject = collectionView.dequeueReusableCellWithReuseIdentifier(reuseId, forIndexPath: indexPath)
        return cell as! T
    }
}

protocol NibCollectionViewCell {
    static func nibName() -> String
}

