//
//  DownloadMedia.swift
//  Stitch
//
//  Created by Doug Richardson on 9/25/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Alamofire
import Foundation
import Photos

func DownloadToPhotos(url : String, completion : (ErrorType?) -> ()) {
    assert(NSThread.isMainThread())
    
    let dest = cacheDirectory().URLByAppendingPathComponent(NSUUID().UUIDString)
    
    let destination : Request.DownloadFileDestination = { (tempURL, response) in
        return dest
    }
    
    networkActivityStarted()
    
    WebAPI.manager.download(.GET, url, destination: destination).progress {
        (bytesRead, totalBytesRead, totalBytesExpectedToRead) in
        // Not called on main thread
        log.debug("Progress: \(totalBytesRead) of \(totalBytesExpectedToRead)")
    }.response {
        (request, response, _, error) in
        
        assert(NSThread.isMainThread())
        
//        log.debug("request: \(request), response: \(response)")
        debugPrint(request, response)
        
        networkActivitedStopped()
        
        if let e = error {
            log.error("Error downloading photo: \(e)")
            completion(e)
            return
        }
        
        guard let path = dest.path else {
            log.error("Unable to get path to photo that downloaded.")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        // defer block makes sure file gets deleted. If one of the other blocks move the file, this will
        // fail, and that's fine. At that point, it is someone else's responsibility to remove the file.
        defer {
            do {
                try NSFileManager().removeItemAtPath(path)
            } catch let e {
                log.error("removeItemAtPath \(path). It's okay if this fails because the file may have been moved. Error: \(e)")
            }
        }
        
        
        guard let res = response else {
            log.error("Response is nil")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        guard let contentType = res.allHeaderFields["Content-Type"] as? String else {
            log.error("Missing content type")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        log.debug("Content-Type is \(contentType)")
        
        guard let uti = UTIFromMime(contentType) else {
            log.error("Could not convert \(contentType) to UTI")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        guard let ext = ExtensionFromUTI(uti) else {
            log.error("Could not get file extension from \(uti)")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        let fileURLWithoutExt = NSURL(fileURLWithPath: path, isDirectory: false)
        let fileURL = fileURLWithoutExt.URLByAppendingPathExtension(ext)
        
        let isImage = contentType.hasPrefix("image/")
        let isVideo = contentType.hasPrefix("video/")
        guard isImage || isVideo else {
            log.error("Unhandled content type \(contentType)")
            completion(makeError("UNRECOGNIZED_CONTENT_TYPE"))
            return
        }
        
        do {
            // This is going to cause the defer block above to fail. Now it's up to us
            // to remove the file when we're done. Make sure you don't return without cleaning it up.
            try NSFileManager().moveItemAtURL(fileURLWithoutExt, toURL: fileURL)
        } catch let e as NSError {
            completion(e)
            return
        } catch let e {
            log.error("Error moving file to one with extension. \(e)")
            completion(makeError("DOWNLOAD_FAILURE_TITLE"))
            return
        }
        
        log.debug("file is \(fileURL)")
        
        
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
            if isImage {
                let req = PHAssetChangeRequest.creationRequestForAssetFromImageAtFileURL(fileURL)
                log.debug("Add image change request: \(req)")
            } else if isVideo {
                let req = PHAssetChangeRequest.creationRequestForAssetFromVideoAtFileURL(fileURL)
                log.debug("Add video change request: \(req)")
            } else {
                assert(false) // shouldn't get here, check before performChanges block.
            }
        }, completionHandler: { (success, error) -> Void in
            log.debug("perform changes completion: \(success), \(error)")
            
            do {
                try NSFileManager().removeItemAtURL(fileURL)
            } catch let e {
                log.error("After saving media, error removing file at \(fileURL). \(e)")
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                if success {
                    completion(nil)
                } else {
                    completion(error)
                }
            }
        })
    }
}

