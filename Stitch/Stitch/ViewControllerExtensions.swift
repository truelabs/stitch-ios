//
//  ViewControllerExtensions.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/28/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertTitle(title : String, message : String? = nil) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: LocalizedString("OK"), style: .Default, handler: nil)
        vc.addAction(action)
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func showAlertTitle(title : String, error : ErrorType?) {
        var message : String?
        if let e = error as? NSError {
            message = e.localizedDescription
        }
        showAlertTitle(title, message: message)
    }
}
