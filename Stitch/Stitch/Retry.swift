//
//  Retry.swift
//  Stitch
//
//  Created by Doug Richardson on 8/20/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

/**
Retry retryBlock up to maxTries, exponentially backing off from minDelay to maxDelay.

- parameter maxTries: The maximum number of times to retry.
- parameter initialDelay: The initial delay in seconds, if the first try isn't successful.
- parameter maxDelay: The maximum delay in seconds until the next retry.
- parameter retryBlock: The block to retry. If it fails, it should call the tryAgain block.
*/
func BackoffRetryWithMaxTries(maxTries : Int, initialDelay : Double, maxDelay : Double, retryBlock : (tryAgain : () ->()) -> ()) {
    var counter = 0
    BackoffRetryWithInitialDelay(initialDelay, maxDelay : maxDelay) { (tryAgain) -> () in
        if counter >= maxTries {
            return
        }
        counter++
        retryBlock(tryAgain: tryAgain)
    }
}

/**
Run retryBlock every time tryAgain is called, but after an exponentially increasing delay.

- parameter initialDelay: The initial delay in seconds, if the first try isn't successful.
- parameter maxDelay: The maximum delay in seconds until the next retry.
- parameter retryBlock: The block to retry. If it fails, it should call the tryAgain block.
*/
func BackoffRetryWithInitialDelay(initialDelay : Double, maxDelay : Double, retryBlock : (tryAgain : () ->()) -> ()) {
    BackoffRetryInternal(0, initialDelay: initialDelay, maxDelay: maxDelay, retryBlock: retryBlock)
}

private func BackoffRetryInternal(i : Int, initialDelay : Double, maxDelay : Double, retryBlock : (tryAgain : () ->()) -> ()) {
    retryBlock() {
        BackoffRetryI(i, initialDelay: initialDelay, maxDelay: maxDelay) {
            BackoffRetryInternal(i+1, initialDelay: initialDelay, maxDelay: maxDelay, retryBlock: retryBlock)
        }
    }
}

/**
Delay for the ith retry.

- parameter i: The zero based retry counter. That is, the first retry is i==0.
- parameter initialDelay: The initial delay in seconds, if the first try isn't successful.
- parameter maxDelay: The maximum delay in seconds until the next retry.
- parameter block: The block to run.
*/
func BackoffRetryI(i : Int, initialDelay : Double, maxDelay : Double, block : () -> ()) {
    let delay = min(initialDelay * pow(2.0, Double(i)), maxDelay)
    let delayNS = Int64(delay * Double(NSEC_PER_SEC))
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delayNS), dispatch_get_main_queue(), block)
}
