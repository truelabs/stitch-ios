//
//  TextFieldTableViewCell.swift
//  Stitch
//
//  Created by John Hwang on 8/31/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell, NibTableViewCell {

    @IBOutlet var textField: UITextField!
    
    static func nibName() -> String {
        return "TextFieldTableViewCell"
    }
    
}
