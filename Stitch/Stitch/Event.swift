//
//  Event.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Event {
    let id : String
    let name : String
    let owner : String
    let created : NSDate
    let anyoneCanPost : Bool
    
    init(id : String, name : String, owner : String, created : NSDate, anyoneCanPost : Bool) {
        self.id = id
        self.name = name
        self.owner = owner
        self.created = created
        self.anyoneCanPost = anyoneCanPost
    }
}

// Event to dictionary for Alamofire Query parameters.
extension Event {    
    func toParams() -> [String: AnyObject] {
        let params : [String: AnyObject] = [
            "id" : id,
            "name" : name,
            "owner" : owner,
            "created" : RFC3339Formatter.stringFromDate(created),
            "anyone_can_post" : anyoneCanPost,
        ]
        
        return params
    }
}

// Event from JSON
extension Event {
    init?(json: JSON?) {
        if let id = json?["id"].string {
            self.id = id
        } else {
            log.error("Event.init(json:): missing id")
            return nil
        }
        
        if let owner = json?["owner"].string {
            self.owner = owner
        } else {
            log.error("Event.init(json:): missing owner")
            return nil
        }
        
        if let name = json?["name"].string {
            self.name = name
        } else {
            log.error("Event.init(json:): missing name")
            return nil
        }
        
        let jsonCreated = json?["created"]
        if let created = jsonCreated?.dateRFC3339 {
            self.created = created
        } else {
            log.error("Event.init(json:): created not valid RFC3339 date \(jsonCreated)")
            return nil
        }
        
        if let anyoneCanPost = json?["anyone_can_post"].bool {
            self.anyoneCanPost = anyoneCanPost
        } else {
            log.error("Event.init(json:): missing name")
            return nil
        }
    }
}

// Event permissions
extension Event {
    func canPost(deviceId : String) -> Bool {
        return anyoneCanPost || isOwner(deviceId)
    }
    
    func canCurrentUserPost() -> Bool {
        return canPost(WebAPI.deviceId)
    }
    
    func isOwner(deviceId : String) -> Bool {
        return owner == deviceId
    }
    
    func amIOwner() -> Bool {
        return isOwner(WebAPI.deviceId)
    }
}

extension Event : Equatable {
}

func ==(lhs: Event, rhs: Event) -> Bool {
    let equal = lhs.id == rhs.id && lhs.name == rhs.name && lhs.owner == rhs.owner && lhs.anyoneCanPost == rhs.anyoneCanPost
    if !equal {
        return false
    }
    
    // Since dates are serialized and unserialized in various formats, and since an NSDate is essentially
    // a float under the hood, do a more lax comparison.
    let diff = lhs.created.timeIntervalSinceDate(rhs.created)
    return diff < 1.0
}

extension Event : CustomDebugStringConvertible {
    
    /// A textual representation of `self`, suitable for debugging.
    var debugDescription: String {
        get {
            return "event: id=\(id), name=\(name), owner=\(owner), created=\(created), anyoneCanPost=\(anyoneCanPost)"
        }
    }
}