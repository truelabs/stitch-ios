//
//  VideoProcessing.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import AVFoundation
import MobileCoreServices
import UIKit

// If onComplete is called with a videoPath, the caller owns the file and must remove it
// if necessary.
func transcodeVideo(videoPath : String, onComplete : (NSError?, String?, String?) ->()) {
    
    let videoProcessingError = makeError("ERROR_PROCESSING_VIDEO")
    
    // Now that the video is saved to the users photo library, transcode and upload.
    let asset = AVURLAsset(URL: NSURL(fileURLWithPath: videoPath), options: nil)
    let preset = AVAssetExportPresetMediumQuality
    let compatiblePresets = AVAssetExportSession.exportPresetsCompatibleWithAsset(asset)
    if !compatiblePresets.contains(preset) {
        log.error("Error transcoding video. \(preset) not supported")
        onComplete(videoProcessingError, nil, nil)
        return
    }
    
    guard let sess = AVAssetExportSession(asset: asset, presetName: preset) else {
        onComplete(videoProcessingError, nil, nil)
        return
    }
    
    transcodeFromExportSession(sess, outputUTI: AVFileTypeMPEG4, onComplete: onComplete)
}

func transcodeFromExportSession(sess : AVAssetExportSession, outputUTI : String, onComplete : (NSError?, String?, String?) ->()) {
    let videoProcessingError = makeError("ERROR_PROCESSING_VIDEO")
    
    // Now that the video is saved to the users photo library, transcode and upload.
    
    let dir = NSTemporaryDirectory() + "/transcodes"
    do {
        try NSFileManager.defaultManager().createDirectoryAtPath(dir, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
        log.error("Error creating transcodes directory at \(dir). Error: \(error)")
        onComplete(error, nil, nil)
        return
    }
    
    guard let outputMIMEUnmanaged = UTTypeCopyPreferredTagWithClass(outputUTI, kUTTagClassMIMEType) else {
        log.error("Error getting MIME type for video UTI \(outputUTI)")
        onComplete(videoProcessingError, nil, nil)
        return
    }
    let outputMIME = outputMIMEUnmanaged.takeRetainedValue() as String
    
    let transcodedVideoPath = dir + "/" + NSProcessInfo.processInfo().globallyUniqueString
    log.debug("Using output path \(transcodedVideoPath)")
    
    let removeTranscodedVideo = { () -> () in
        do {
            try NSFileManager.defaultManager().removeItemAtPath(transcodedVideoPath)
        } catch let error as NSError {
            log.error("Error removing transcoded video file at path \(transcodedVideoPath). Error: \(error)")
        }
    }
    
    let onCompleteMainThread = { (error : NSError?, path : String?, mime: String?) in
        dispatch_async(dispatch_get_main_queue()) {
            onComplete(error, path, mime)
        }
    }
    
    sess.shouldOptimizeForNetworkUse = false
    sess.outputFileType = outputUTI
    sess.outputURL = NSURL(fileURLWithPath: transcodedVideoPath)
    sess.exportAsynchronouslyWithCompletionHandler {
        switch sess.status {
        case .Failed:
            log.error("AVAssetExportSession Failed. Error: \(sess.error)")
            removeTranscodedVideo()
            onCompleteMainThread(videoProcessingError, nil, nil)
        case .Cancelled:
            log.info("AVAssetExportSession cancelled")
            removeTranscodedVideo()
            onCompleteMainThread(videoProcessingError, nil, nil)
        case .Completed:
            log.debug("Asset Export completed")
            logFileSize(transcodedVideoPath)
            onCompleteMainThread(nil, transcodedVideoPath, outputMIME)
            
        default:
            log.error("UNEXPECTED EXPORT STATUS: \(sess.status)")
        }
    }
}

func extractImageFromVideo(videoPath : String, onComplete : (UIImage?, NSError?) -> ()) {
    let asset = AVURLAsset(URL: NSURL(fileURLWithPath: videoPath), options: nil)
    extractImageFromVideoAsset(asset, onComplete: onComplete)
}

func extractImageFromVideoAsset(asset : AVAsset, onComplete : (UIImage?, NSError?) -> ()) {
    dispatch_async(dispatch_get_global_queue(Dispatch.DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
        
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        var e : NSError?
        let cgImage: CGImage!
        do {
            cgImage = try generator.copyCGImageAtTime(CMTimeMake(0, 1000), actualTime: nil)
        } catch let error as NSError {
            e = error
            cgImage = nil
        } catch {
            fatalError()
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            if cgImage == nil {
                onComplete(nil, e)
                return
            }
            
            let image = UIImage(CGImage: cgImage)
            onComplete(image, nil)
        }
    }
}

// If onComplete is called with a audioUrl, the caller owns the file and must remove it
// if necessary.
func exportAudio(url : NSURL, onComplete : (error : NSError?, audioPath : String?, mime : String?) -> ()) {
    let exportError = makeError("ERROR_EXPORTING_AUDIO")
    
    // Now that the video is saved to the users photo library, transcode and upload.
    let asset = AVURLAsset(URL: url, options: nil)
    let preset = AVAssetExportPresetAppleM4A
    let compatiblePresets = AVAssetExportSession.exportPresetsCompatibleWithAsset(asset)
    if !compatiblePresets.contains(preset) {
        log.error("Error exporting audio. \(preset) not supported")
        onComplete(error: exportError, audioPath: nil, mime: nil)
        return
    }
    
    guard let sess = AVAssetExportSession(asset: asset, presetName: preset) else {
        log.error("Error getting export session.")
        onComplete(error: exportError, audioPath: nil, mime: nil)
        return
    }
    
    transcodeFromExportSession(sess, outputUTI: AVFileTypeAppleM4A, onComplete: onComplete)
}
