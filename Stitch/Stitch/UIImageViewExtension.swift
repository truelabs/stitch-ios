//
//  UIImageViewExtension.swift
//  Stitch
//
//  Created by John Hwang on 9/29/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import AlamofireImage

extension UIImageView {
    func setImageURL(urlStrOpt : String?) {
        if let urlStr = urlStrOpt, let url = NSURL(string: urlStr) {
            af_setImageWithURL(url)
            return
        }
        
        af_cancelImageRequest()
        image = nil
    }
}