//
//  Branch.swift
//  Stitch
//
//  Created by Doug Richardson on 11/10/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Branch

func BranchInstance() -> Branch {
    #if DEBUG
        return Branch.getTestInstance()
    #else
        return Branch.getInstance()
    #endif
}