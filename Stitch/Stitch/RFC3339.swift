//
//  RFC3339.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

/// Returns a global RFC3339 NSDateFormatter. As of iOS 7, NSDateFormatter's are
/// thread safe.
var RFC3339Formatter : NSDateFormatter = {
    let enUSPOSIXLocale = NSLocale(localeIdentifier:"en_US_POSIX")
    let f = NSDateFormatter()
    f.locale = enUSPOSIXLocale
    f.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssXXX"
    f.timeZone = NSTimeZone(forSecondsFromGMT:0)
    return f
}()

/// Returns a global RFC3339 NSDateFormatter with fractional seconds.
/// As of iOS 7, NSDateFormatter's are thread safe.
var RFC3339FractionalSecondsFormatter : NSDateFormatter = {
    let enUSPOSIXLocale = NSLocale(localeIdentifier:"en_US_POSIX")
    let f = NSDateFormatter()
    f.locale = enUSPOSIXLocale
    f.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSXXX"
    f.timeZone = NSTimeZone(forSecondsFromGMT:0)
    return f
}()

/// Parse a date from a string, trying both the RFC3339 and RFC3339 fractional
/// second formats.
func DateFromRFC3339String(str : String) -> NSDate? {
    if let d = RFC3339Formatter.dateFromString(str) {
        return d
    }
    if let d = RFC3339FractionalSecondsFormatter.dateFromString(str) {
        return d
    }
    return nil
}


