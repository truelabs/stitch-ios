//
//  LocalizedString.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/20/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

func LocalizedString(key : String) -> String {
    return NSLocalizedString(key, comment: "")
}

// Even though the strings parameter is CVarArgType, you should only pass in strings that
// have been properly formatted for the current locale.
func LocalizedFormatString(formatKey : String, strings : CVarArgType...) -> String {
    return withVaList(strings) { (pointer : CVaListPointer) -> String in
        return NSString(format: LocalizedString(formatKey), arguments: pointer) as String
    }
}
