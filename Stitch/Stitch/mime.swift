//
//  mime.swift
//  Stitch
//
//  Created by Doug Richardson on 8/21/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import MobileCoreServices

func UTTypeToMIME(uti : String) -> String? {
    guard let mimeUnmanaged = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType) else {
        return nil
    }
    let mime = mimeUnmanaged.takeRetainedValue() as String
    return mime
}
