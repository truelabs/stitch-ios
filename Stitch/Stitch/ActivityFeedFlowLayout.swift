//
//  ActivityFeedFlowLayout.swift
//  Stitch
//
//  Created by Doug Richardson on 9/25/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class ActivityFeedFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        minimumLineSpacing = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
