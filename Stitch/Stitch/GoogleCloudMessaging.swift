//
//  GoogleCloudMessaging.swift
//  Stitch
//
//  Created by Doug Richardson on 10/9/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Foundation

protocol PushNotificationManagerDelegate : class {
    func pushNotificationManager(manager : PushNotificationManager, tokenDidUpdate token: String)
    func pushNotificationManager(manager : PushNotificationManager, tokenUpdateError error: NSError)
}

class PushNotificationManager : NSObject, GGLInstanceIDDelegate {
    
    weak var delegate : PushNotificationManagerDelegate?
    
    private let instanceID = GGLInstanceID()
    var lastDeviceToken : NSData?
    
    override init() {
        super.init()
        
        let config = GGLInstanceIDConfig()
        config.delegate = self
        config.logLevel = .Debug
        
        instanceID.startWithConfig(config)
    }
    
    func registerAPNSDeviceToken(deviceToken : NSData) {
        
        lastDeviceToken = deviceToken

        #if APNS_DEVELOPMENT
            let sandbox = true
            #elseif APNS_PRODUCTION
            let sandbox = false
            #else
            // Define either -D APNS_DEVELOPMENT or -D APNS_PRODUCTION.
            error
            #endif
        
        log.debug("registering device token with sandbox=\(sandbox): \(deviceToken)")
        
        let registrationOptions = [
            kGGLInstanceIDRegisterAPNSOption : deviceToken,
            kGGLInstanceIDAPNSServerTypeSandboxOption : sandbox
        ]
        
        let gcmSenderID = GGLContext.sharedInstance().configuration.gcmSenderID
        
        instanceID.tokenWithAuthorizedEntity(gcmSenderID, scope: kGGLInstanceIDScopeGCM, options: registrationOptions) { (token, error) -> Void in
            assert(NSThread.isMainThread())
            
            log.debug("GCM got token \(token), error \(error)")
            
            if let e = error {
                log.error("Error getting token, \(e)")
                self.delegate?.pushNotificationManager(self, tokenUpdateError: e)
                return
            }
            
            guard let t = token else {
                log.severe("Token nil and error nil, didn't expect this.")
                return
            }
            
            self.delegate?.pushNotificationManager(self, tokenDidUpdate: t)
        }
    }
    
    func onTokenRefresh() {
        log.debug("token refresh")
        
        guard let deviceToken = lastDeviceToken else {
            log.error("No device token cached in token refresh")
            return
        }
        
        registerAPNSDeviceToken(deviceToken)
    }
}

