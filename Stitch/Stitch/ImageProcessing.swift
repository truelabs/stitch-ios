//
//  ImageProcessing.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import UIKit

func scaledDownImage(image : UIImage, toMaxEdge maxEdgeI : Int) -> UIImage {
    let maxEdge = CGFloat(maxEdgeI)
    let scale = maxEdge / max(image.size.width, image.size.height)
    
    if scale >= 1.0 {
        log.debug("Dont need to scale down. Scale factor is \(scale)")
        return image
    }
    
    let scaledSize = CGSizeMake(round(scale * image.size.width), round(scale * image.size.height));
    UIGraphicsBeginImageContext(scaledSize);
    image.drawInRect(CGRectMake(0, 0, scaledSize.width, scaledSize.height))
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return scaledImage
}
