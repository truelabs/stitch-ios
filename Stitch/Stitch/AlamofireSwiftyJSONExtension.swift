//
//  AlamofireExtension.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension Alamofire.Request {
    func responseSwiftyJSON(completionHandler: (NSURLRequest?, NSHTTPURLResponse?, SwiftyJSON.JSON?, NSError?) -> Void) -> Self {
        
        return responseJSON() { response in
            
            assert(NSThread.isMainThread())
            
            if response.result.isSuccess {
                if let json : AnyObject = response.result.value {
                    let swiftyJson = SwiftyJSON.JSON(json)
                    completionHandler(response.request, response.response, swiftyJson, nil)
                } else {
                    log.error("responseSwiftyJSON: not AnyObject: \(response.result.value)")
                    completionHandler(response.request, response.response, nil, makeError("GENERIC_SERVICE_FAILURE"))
                }
            } else {
                let error : NSError
                if let e = response.result.error {
                    error = e
                } else {
                    assert(false, "Failure response, but no error.")
                    error = makeError("GENERIC_SERVICE_FAILURE")
                }
                
                log.error("Failed to get JSON. \(error)")
                completionHandler(response.request, response.response, nil, error)
            }
        }
    }
}
