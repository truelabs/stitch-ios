//
//  WebAPI.swift
//  Stitch
//
//  Created by Douglas Richardson on 4/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUUID
import SwiftyBase64
import UIKit
import CoreLocation

var WebAPI = WebAPISessionFromUsersEnvironment()

func WebAPISessionFromUsersEnvironment() -> WebAPISession {
    let apibase : String
    if UserPreferences().useDeveloperClientConfig {
        apibase = "https://v7-dot-stitch-1026.appspot.com"
    } else {
        apibase = "https://stitch-1026.appspot.com"
    }
    
    return WebAPISession(deviceId: DeviceId, devicePassword : DevicePassword, apibase: apibase)
}

struct WebAPISession {
    
    let deviceId : String
    let devicePassword : String
    let manager : Alamofire.Manager
    
    init(deviceId : String, devicePassword : String, apibase : String) {
        log.debug("WebAPI session with deviceId=\(deviceId), apibase=\(apibase)")
        
        self.deviceId = deviceId
        self.devicePassword = devicePassword
        
        baseURL = apibase
        eventsURL = baseURL + "/events"
        deviceURL = baseURL + "/devices/" + deviceId
        pairingURL = baseURL + "/pair"
        
        let conf = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        manager = Manager(configuration: conf)
    }
    
    let baseURL : String
    let eventsURL : String
    let deviceURL : String
    let pairingURL : String
    
    let staticWebSiteBaseURL = "https://app.stitchpic.us"
    
    func ShareEventURL(eventId : String) -> URLStringConvertible {
        return staticWebSiteBaseURL + "/?event=" + eventId + "&s=ios";
    }
    
    func EventsURLNearLatitude(latitude : Double, longitude : Double) -> URLStringConvertible {
        return eventsURL + "?filter=near&latitude=\(latitude)&longitude=\(longitude)"
    }

    func EventURL(eventId : String) -> URLStringConvertible {
        return eventsURL + "/" + eventId
    }
    
    func EventCaptures(eventId : String) -> URLStringConvertible {
        return EventURL(eventId).URLString + "/captures"
    }
    
    func EventCapture(eventId : String, deviceId : String, contentMd5 : String) -> URLStringConvertible {
        return EventCaptures(eventId).URLString + "/" + deviceId + "/" + contentMd5
    }
    
    func CaptureData(eventId : String, deviceId : String, contentMd5 : String) -> URLStringConvertible {
        return EventCapture(eventId, deviceId: deviceId, contentMd5: contentMd5).URLString + "/data"
    }
    
    func CaptureDataUploadUrl(eventId : String, deviceId : String, contentMd5 : String) -> URLStringConvertible {
        return EventCapture(eventId, deviceId: deviceId, contentMd5: contentMd5).URLString + "/datauploadurl"
    }
    
    func UploadComplete(eventId : String, deviceId : String, contentMd5 : String) -> URLStringConvertible {
        return EventCapture(eventId, deviceId: deviceId, contentMd5: contentMd5).URLString + "/uploadcomplete"
    }
    
    func DataUploadRequest(eventId : String, deviceId : String, contentMd5 : String, contentType : String) -> Request {
        var params = [String : AnyObject]()
        params["contenttype"] = contentType
        return manager.request(.GET, CaptureDataUploadUrl(eventId, deviceId: deviceId, contentMd5: contentMd5), parameters: params, encoding: .URL, headers: authHeaders())
    }
    
    func VideoURL(assetID : String) -> URLStringConvertible {
        return baseURL + "/data/" + assetID
    }
    
    func printCacheUsage() {
        let mb = {
            (inBytes : Int) -> String in
            let b = Double(inBytes)
            return "\(b / 1000000.0)"
        }
        if let c = manager.session.configuration.URLCache {
            let diskUsage = mb(c.currentDiskUsage)
            let diskCap = mb(c.diskCapacity)
            let memUsage = mb(c.currentMemoryUsage)
            let memCap = mb(c.memoryCapacity)
            log.debug("HTTP Cache Usage (MB): disk=\(diskUsage)/\(diskCap), memory=\(memUsage)/\(memCap)")
        } else {
            log.debug("no HTTP cache configured")
        }
    }
    
    func authHeaders() -> [String: String] {
        return ["Device-Id" : deviceId, "Device-Password":devicePassword]
    }
    
    func putDevice(completion : (NSError?) -> ()) {
        var device = [String: AnyObject]()
        device["username"] = DeviceUsername
        device["password"] = devicePassword
        device["device_token"] = DeviceToken
        
        manager.request(.PUT, deviceURL, parameters: device, encoding: .JSON).validate().response {
            (_, _, _, err : NSError?) -> Void in
            completion(err)
        }
    }
    
    func putPairWithTV(eventId : String, code : String, completion : (NSError?) -> ()) {
        let putValueURL = pairingURL + "/codes/" + code + "/value"
        let data = eventId.dataUsingEncoding(NSUTF8StringEncoding)!
        
        manager.upload(.PUT, putValueURL, data: data).response {
            (_, response, _, error) -> Void in
            if let e = error {
                completion(e)
                return
            }
            
            if response?.statusCode < 200 || response?.statusCode >= 300 {
                completion(makeError("PAIRING_FAILED"))
                return
            }
            
            completion(nil)
        }
    }
    
    func registerDeviceWithRetries() {
        BackoffRetryWithInitialDelay(0.2, maxDelay: 30.0) { (tryAgain) -> () in
            WebAPI.putDevice() { (error) -> () in
                if error != nil {
                    log.error("Failed to register device. \(error)")
                    tryAgain()
                    return
                }
                log.debug("Registered device \(DeviceId)")
            }
        }
    }
    
    func putEvent(event : Event, completion : (NSError?) -> Void) {
        manager.request(.PUT, EventURL(event.id), parameters: event.toParams(), encoding: .JSON, headers: authHeaders()).validate().response { (_, response, _, error) -> Void in
            if let e = error {
                log.error("response error: \(e)")
            }
            completion(error)
        }
    }
    
    func getEventById(eventId : String, completion : (Event?, [Capture]?, NSError?) -> ()) {
        manager.request(.GET, WebAPI.EventURL(eventId)).responseSwiftyJSON { (req, res, json, error) -> Void in
            if error != nil {
                completion(nil, nil, error)
                return
            }
            
            var captures = [Capture]()
            if let capturesJSON = json?["captures"].array {
                for captureJSON in capturesJSON {
                    if let capture = Capture(json:captureJSON) {
                        captures.append(capture)
                    } else {
                        log.error("ignoring invalid capture in array")
                    }
                }
            } else {
                log.warning("response missing captures array")
            }
            
            let event = Event(json: json?["event"])
            
            if event == nil {
                log.error("error converting json to Event")
                completion(nil, nil, makeError("ERROR_FETCHING_EVENT"))
                return
            }
            
            completion(event, captures, nil)
        }
    }
    
    func putUploadCompleteEventId(eventId : String, contentMd5Hex : String, completion : (NSError?) -> Void) {
        let url = UploadComplete(eventId, deviceId: deviceId, contentMd5: contentMd5Hex)
        log.debug("PUT UploadComplete: \(url)")
        manager.request(.PUT, url, headers: authHeaders()).validate().response {
            (_, response, _, error) -> Void in
            if let e = error {
                log.error("Upload complete request failed. \(e)")
            }
            completion(error)
            
        }
    }
    
    private func doEventSubscriber(method : Alamofire.Method, eventId : String, completion : (NSError?) -> Void) {
        let url = EventURL(eventId).URLString + "/subscribers/" + DeviceId
        manager.request(method, url, headers: authHeaders()).validate().response {
            (_, response, _, error) -> Void in
            if let e = error {
                log.error("Failed to \(method) event subscriber. \(e)")
            }
            completion(error)
        }
    }
    
    func putEventSubscriber(eventId : String, completion : (NSError?) -> Void) {
        doEventSubscriber(.PUT, eventId: eventId, completion: completion)
    }
    
    func deleteEventSubscriber(eventId : String, completion : (NSError?) -> Void) {
        doEventSubscriber(.DELETE, eventId: eventId, completion: completion)
    }
    
    func ShareURL(eventId : String, deviceId : String, contentMd5 : String) -> URLStringConvertible {
        return staticWebSiteBaseURL + "/?eid=" + eventId + "&cmd=" + contentMd5 + "&s=ios"
    }
    
    func getShortURL(longURL: String, completion : (String?, NSError?) -> ()){
        let params = [
            "longUrl": longURL
        ]
        
        //Encrypt Google URL shortner API key: AIzaSyBejzQt1VSaRGBcbEem6vWwoyN-fx_C30E
        let part4 = "BcbEem"
        let part1 = "AIzaSy"
        let part5 = "6vWwoy"
        let part2 = "BejzQt"
        let part3 = "1VSaRG"
        let part7 = "C30E"
        let part6 = "N-fx_"
        let key = String(format: "%@%@%@%@%@%@%@", part1, part2, part3, part4, part5, part6, part7)
        let url = "https://www.googleapis.com/urlshortener/v1/url?key=\(key)"
        
        manager.request(.POST, url, parameters: params, encoding: .JSON).responseSwiftyJSON { (req, res, json, error) -> Void in
            if error != nil {
                completion(nil, error)
                return
            }
            
            if let shortURL = json?["id"].string {
                completion(shortURL, nil)
                return
            }
            
            log.warning("response didn't contain shortURL: \(json?.rawString())")
            completion(nil, makeError("ERROR_FETCHING_URL"))
            return
        }
    }
    
    func deleteCaptureByEventId(eventId : String, deviceId : String, contentMd5 : String, completion : (NSError?) -> ()) {
        let url = EventCapture(eventId, deviceId: deviceId, contentMd5: contentMd5)
        manager.request(.DELETE, url, headers: authHeaders()).validate().response { (_, _, _, error) -> Void in
            completion(error)
        }
    }
}

func WebAPIUniqueId() -> String {
    // Create a new ID using RFC 4122 version 4 random bytes UUID.
    return SwiftyUUID.UUID().CanonicalString()
}
