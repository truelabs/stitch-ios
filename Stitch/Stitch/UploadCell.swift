//
//  UploadCell.swift
//  Stitch
//
//  Created by Doug Richardson on 8/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol UploadCellDelegate : class {
    func uploadCellButtonPressed(cell : UploadCell)
}

class UploadCell: UICollectionViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var progress: UIProgressView!
    @IBOutlet var button : UIButton!
    @IBOutlet var imageView : UIImageView!
    var uploadItem : UploadQueueItem?
    
    weak var delegate : UploadCellDelegate?
    
    @IBAction func buttonPressed(sender : UIButton) {
        delegate?.uploadCellButtonPressed(self)
    }
}

extension UploadCell : NibCollectionViewCell {
    static func nibName() -> String {
        return "UploadCell"
    }
}
