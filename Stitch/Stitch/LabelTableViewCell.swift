//
//  LabelTableViewCell.swift
//  Stitch
//
//  Created by Doug Richardson on 10/3/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell, NibTableViewCell {
    @IBOutlet var label : UILabel!
    
    static func nibName() -> String {
        return "LabelTableViewCell"
    }
}
