//
//  MWPDelegate.swift
//  Stitch
//
//  Created by John Hwang on 7/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import MWPhotoBrowser

protocol MWPDelegateDelegate : class {
    func delegateDelegate(del : MWPDelegate, didSelectItems selectedItems: Set<UInt>)
}

class MWPDelegate: NSObject, MWPhotoBrowserDelegate {
    let assets : [PHAsset]
    let photoSize : CGSize
    let thumbSize : CGSize
    var selectedItems = Set<UInt>()
    
    weak var delegate : MWPDelegateDelegate?
    
    init(photos : [PHAsset], photoSize : CGSize, thumbSize : CGSize) {
        assets = photos
        self.photoSize = photoSize
        self.thumbSize = thumbSize
    }
    
    func numberOfPhotosInPhotoBrowser(photoBrowser: MWPhotoBrowser) -> UInt {
        return UInt(assets.count)
    }

    func numberOfPhotosSelected(photoBrowser: MWPhotoBrowser) -> UInt {
        return UInt(selectedItems.count)
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser, photoAtIndex index: UInt) -> MWPhotoProtocol! {
        let photo = MWPhoto(asset: assets[Int(index)], targetSize: photoSize)
        return photo
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser!, thumbPhotoAtIndex index: UInt) -> MWPhotoProtocol! {
        let photo = MWPhoto(asset: assets[Int(index)], targetSize: thumbSize)
        return photo
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser!, isPhotoSelectedAtIndex index: UInt) -> Bool {
        return selectedItems.contains(index)
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser!, photoAtIndex index: UInt, selectedChanged selected: Bool) {
        log.debug("Selection changed. index=\(index), selected=\(selected)")
        if selected {
            selectedItems.insert(index)
            
            if assets[Int(index)].duration > AppDelegate.sharedInstance.videoLimit {
                let alert = UIAlertController(title: LocalizedString("ERROR_VIDEO_DURATION_TITLE"), message: LocalizedString("ERROR_VIDEO_DURATION"), preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: LocalizedString("OK"), style: .Cancel, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.selectedItems.remove(index)
                    photoBrowser.setPhotoSelected(false, atIndex: index)
                })
                alert.addAction(cancelAction)
                
//                let editAction = UIAlertAction(title: LocalizedString("OK"), style: UIAlertActionStyle.Default, handler: {
//                    (alert: UIAlertAction) -> Void in
//                    PHImageManager.defaultManager().requestAVAssetForVideo(assets[Int(index)], options: nil) {
//                        asset,audioMix,info in
//                        
//                        guard let urlAsset = asset as? AVURLAsset else {
//                            return
//                        }
//                        
//                        guard let urlPath = urlAsset.URL.path else {
//                            return
//                        }
//                        
//                        let video = UIVideoEditorController()
//                        video.videoPath = urlPath
//                        video.videoMaximumDuration = AppDelegate.sharedInstance.videoLimit
//                        photoBrowser.presentViewController(video, animated: true, completion: nil)
//                    }
//                })
//                alert.addAction(editAction)
                
                photoBrowser.presentViewController(alert, animated: true, completion: nil)
                
            } else {
                selectedItems.insert(index)
            }
        } else {
            selectedItems.remove(index)
        }
    }
    
    func photoBrowserDidFinishModalPresentation(photoBrowser : MWPhotoBrowser!) {
        log.debug("Selected the following items: \(selectedItems)")
        delegate?.delegateDelegate(self, didSelectItems: selectedItems)
        photoBrowser.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
