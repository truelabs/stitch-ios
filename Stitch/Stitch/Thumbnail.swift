//
//  Thumbnail.swift
//  Stitch
//
//  Created by Doug Richardson on 9/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import SwiftyJSON

struct Thumbnail {
    let width : Int
    let height : Int
    let URL : String
    
    init(width : Int, height : Int, URL : String) {
        self.width = width
        self.height = height
        self.URL = URL
    }
}

// Thumbnail from JSON
extension Thumbnail {
    init?(json: JSON?) {
        if let width = json?["width"].int {
            self.width = width
        } else {
            log.error("Thumbnail.init(json:): missing int width")
            return nil
        }
        
        if let height = json?["height"].int {
            self.height = height
        } else {
            log.error("Thumbnail.init(json:): missing int height")
            return nil
        }
        
        if let url = json?["url"].string {
            self.URL = url
        } else {
            log.error("Thumbnail.init(json:): missing string url")
            return nil
        }
    }
}

extension Thumbnail : BestAssetCandidate {
}

