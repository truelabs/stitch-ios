//
//  SwiftyJSONRFC3339Extension.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - RFC 3339 timestamps.

extension SwiftyJSON.JSON {
    
    // Optional date
    var dateRFC3339: NSDate?  {
        get {
            switch self.type {
            case .String:
                if let possibleTimeStr = self.object as? String {
                    return DateFromRFC3339String(possibleTimeStr)
                } else {
                    return nil
                }
            default:
                return nil
            }
        }
        set {
            if let v = newValue {
                self.object = RFC3339Formatter.stringFromDate(v)
            } else {
                self.object = NSNull()
            }
        }
    }
}
