//
//  Capture.swift
//  Stitch
//
//  Created by Doug Richardson on 9/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Capture {
    let ownerId : String
    let ownerName : String
    let contentMD5 : String
    let time : NSDate
    let thumbnails : [Thumbnail]
    let transcodes : [Video]
    
    init(ownerId : String, ownerName : String, contentMD5 : String, time : NSDate, thumbnails : [Thumbnail], transcodes : [Video]) {
        self.ownerId = ownerId
        self.ownerName = ownerName
        self.contentMD5 = contentMD5
        self.time = time
        self.thumbnails = thumbnails
        self.transcodes = transcodes
    }
}

// Capture from JSON
extension Capture {
    init?(json: JSON?) {
        if let ownerId = json?["owner"].string {
            self.ownerId = ownerId
        } else {
            log.error("Capture.init(json:): missing string owner")
            return nil
        }
        
        if let ownerName = json?["owner_name"].string {
            self.ownerName = ownerName
        } else {
            log.error("Capture.init(json:): missing string owner_name")
            return nil
        }
        
        if let contentMD5 = json?["content_md5"].string {
            self.contentMD5 = contentMD5
        } else {
            log.error("Capture.init(json:): missing string content_md5")
            return nil
        }
        
        if let time = json?["time"].dateRFC3339 {
            self.time = time
        } else {
            log.error("Capture.init(json:): invalid RFC3339 time")
            return nil
        }
        
        if let thumbnailsJSON = json?["thumbnails"].array {
            var thumbnails = [Thumbnail]()
            for thumbnailJSON in thumbnailsJSON {
                if let thumbnail = Thumbnail(json:thumbnailJSON) {
                    thumbnails.append(thumbnail)
                } else {
                    log.error("Capture.init(json:): invalid thumbnail in array")
                    return nil
                }
            }
            self.thumbnails = thumbnails
        } else {
            log.error("Capture.init(json:): missing thumbnails array")
            return nil
        }
        
        if let transcodesJSON = json?["transcodes"].array {
            var transcodes = [Video]()
            for transcodeJSON in transcodesJSON {
                if let video = Video(json: transcodeJSON) {
                    transcodes.append(video)
                } else {
                    log.error("Capture.init(json:): invalid video in transcodes array")
                    return nil
                }
            }
            self.transcodes = transcodes
        } else {
            self.transcodes = []
        }
    }
}