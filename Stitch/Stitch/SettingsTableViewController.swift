//
//  SettingsTableViewController.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import DeviceGuru
import MessageUI
import UIKit

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    private enum Rows {
        case Username
        case Version
        case SendFeedback
        case UseDeveloperClientConfig
        case UploadMediaOverCellular
    }
    
    private var sections : [[Rows]] = [
        [.Username], [.Version, .SendFeedback],
        [.UploadMediaOverCellular],
    ]
    
    #if DEBUG
    private let developerSection : [Rows] = [
        .UseDeveloperClientConfig
    ]
    #endif
    
    private let userPrefs = UserPreferences()
    private let defaultCell = NibTableViewCellReuseIdAndType<LabelDetailTableViewCell>(reuseId: "Default")
    private let sendFeedbackCell = NibTableViewCellReuseIdAndType<LabelTableViewCell>(reuseId: "SendFeedback")
    private let switchCell = NibTableViewCellReuseIdAndType<LabelSwitchTableViewCell>(reuseId: "Switch")
    
    init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("SETTINGS")
        
        defaultCell.registerNib(tableView)
        sendFeedbackCell.registerNib(tableView)
        switchCell.registerNib(tableView)
        
        
        #if DEBUG
            sections.append(developerSection)
        #endif
    }
    
    override func viewWillAppear(animated: Bool) {
        
        // Google Analytics
        Analytics.sharedInstance.screenView("Settings")
        
        if let indexPath = selectedRowIndexPath {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            selectedRowIndexPath = nil
        }
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }

    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section][indexPath.row] {
        case .Username:
            return true
        case .SendFeedback:
            return true
        default:
            return false
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section][indexPath.row] {
            
        case .Username:
            let cell = defaultCell.dequeueFromTableView(tableView)
            
            let ccc = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "howdy")
            if let c = ccc.detailTextLabel?.textColor {
                log.debug("Color: \(c)")
                var white = CGFloat(0)
                var alpha = CGFloat(0)
                let whiteOK = c.getWhite(&white, alpha: &alpha)
                log.debug("white: \(whiteOK), \(white), \(alpha)")
            }
            cell.label.text = LocalizedString("USERNAME")
            cell.detailLabel.text = DeviceUsername
            cell.accessoryType = .DisclosureIndicator
            return cell
            
        case .Version:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("VERSION")
            cell.detailLabel.text = appVersionString()
            cell.accessoryType = .None
            return cell
            
        case .SendFeedback:
            let cell = sendFeedbackCell.dequeueFromTableView(tableView)
            cell.label.textColor = tableView.tintColor
            cell.label.text = LocalizedString("SEND_FEEDBACK")
            return cell
            
        case .UseDeveloperClientConfig:
            let cell = switchCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("USE_DEV_CLIENT_CONFIG")
            cell.switchView.on = userPrefs.useDeveloperClientConfig
            cell.switchView.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            cell.switchView.addTarget(self, action: Selector("useDeveloperClientConfigSwitchChanged:"), forControlEvents: .ValueChanged)
            return cell
            
        case .UploadMediaOverCellular:
            let cell = switchCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("USE_CELLULAR_DATA_FOR_UPLOADS")
            cell.switchView.on = !userPrefs.disableCellularDataForUploads
            cell.switchView.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            cell.switchView.addTarget(self, action: Selector("useCellularDataForUploadsSwitchChanged:"), forControlEvents: .ValueChanged)
            return cell
        }
    }
    
    var selectedRowIndexPath : NSIndexPath?
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRowIndexPath = indexPath
        
        switch sections[indexPath.section][indexPath.row] {
        case .Username:
            let vc = SingleTextFieldController()
            vc.textFieldPlaceholder = LocalizedString("USERNAME_SETUP")
            vc.textFieldValue = DeviceUsername
            vc.autocorrectionType = .No
            vc.navigationItem.title = LocalizedString("USERNAME")
            vc.footerText = LocalizedString("USERNAME_DESC")
            vc.gaScreen = "Username"
            vc.textChangedCallback = { (newValue) -> () in
                let username = newValue ?? ""
                if username == "" {
                    let alert = UIAlertController(title: LocalizedString("ERROR_SAVING"), message: LocalizedString("USERNAME_EMPTY"), preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("OK"), style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    DeviceUsername = username
                    WebAPI.registerDeviceWithRetries()
                    self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
                }
            }
            navigationController?.pushViewController(vc, animated: true)
            
        case .Version:
            break
        case .SendFeedback:
            let vc = MFMailComposeViewController()
            let appName = (NSBundle.mainBundle().infoDictionary?["CFBundleName"] as? String) ?? ""
            let iOSVersion = UIDevice.currentDevice().systemName + " " + UIDevice.currentDevice().systemVersion
            vc.setSubject(LocalizedString("FEEDBACK_SUBJECT"))
            let modelIdentifier = hardwareString()
            let modelMarketing = hardwareDescription() ?? ""
            let body = NSString(format: LocalizedString("FEEDBACK_BODY_FORMAT_STRING"), appName, appVersionString(), modelMarketing, modelIdentifier, iOSVersion)
            vc.setMessageBody(body as String, isHTML: false)
            vc.setToRecipients(["stitch@funsocialapps.com"])
            vc.mailComposeDelegate = self
            presentViewController(vc, animated: true, completion: nil)
        case .UploadMediaOverCellular:
            break
        case .UseDeveloperClientConfig:
            break
        }
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        if result.rawValue == MFMailComposeResultFailed.rawValue {
            log.error("Failed to send feedback e-mail. \(error)")
        }
    }
    
    func useDeveloperClientConfigSwitchChanged(sender : UISwitch) {
        userPrefs.useDeveloperClientConfig = sender.on
        WebAPI = WebAPISessionFromUsersEnvironment()
    }
    
    func useCellularDataForUploadsSwitchChanged(sender : UISwitch) {
        userPrefs.disableCellularDataForUploads = !sender.on
    }
}

private func appVersionString() -> String {
    let shortVersion = (NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    let version = (NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String) ?? ""
    return "\(shortVersion) (\(version))"
}
