//
//  HTTPEntityFingerprint.swift
//  Stitch
//
//  Created by Douglas Richardson on 7/21/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation


// HTTPEntityFingerprint uses entity headers to identify the version of an object returned from
// an HTTP server. This can be used to implement application level caching of, for example,
// deserizlied JSON dictionaries.
struct HTTPEntityFingerprint {
    let contentMD5 : String?
    let etag : String?
    let lastModified : String?
}

extension HTTPEntityFingerprint {
    init?(response : NSURLResponse?) {
        if let httpResponse = response as? NSHTTPURLResponse {
            let h = httpResponse.allHeaderFields
            contentMD5 = h["Content-MD5"] as? String
            etag = h["Etag"] as? String
            lastModified = h["Last-Modified"] as? String
            
            if contentMD5 == nil && etag == nil && lastModified == nil {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func debugString() -> String {
        let md5 = contentMD5 ?? ""
        let e = etag ?? ""
        let mod = lastModified ?? ""
        return "Content-MD5=\(md5), Etag=\(e), Last-Modified=\(mod)"
    }
    
    static func match(first : HTTPEntityFingerprint?, second : HTTPEntityFingerprint?) -> Bool {
        if let a = first, b = second {
            if let md5A = a.contentMD5, md5B = b.contentMD5 {
                return md5A == md5B
            }
            if let etagA = a.etag, etagB = b.etag {
                return etagA == etagB
            }
            if let lastA = a.lastModified, lastB = b.lastModified {
                return lastA == lastB
            }
        }
        
        return false
    }
}