//
//  SingleTextFieldController.swift
//  Stitch
//
//  Created by John Hwang on 8/31/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON

class SingleTextFieldController: UITableViewController {
    
    var textFieldPlaceholder : String?
    var textFieldValue : String?
    var headerText: String?
    var footerText : String?
    var trimWhitespace = true
    var newSetup : Bool?
    var doneButton : UIBarButtonItem?
    var cancelButton : UIBarButtonItem?
    var textChangedCallback : ((String?) -> ())?
    var autocorrectionType = UITextAutocorrectionType.Default
    var keyboardType = UIKeyboardType.Default
    var gaScreen = "Not Set";
    
    private let textFieldCell = NibTableViewCellReuseIdAndType<TextFieldTableViewCell>(reuseId: "TextField")
    
    init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if newSetup == true {
            doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("setupDone"))
            navigationItem.rightBarButtonItem = doneButton
            doneButton!.enabled = false
            cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancelPressed"))
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.setHidesBackButton(true, animated: false)
        }
        
        textFieldCell.registerNib(tableView)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        // Google Analytics
        Analytics.sharedInstance.screenView(gaScreen)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if newSetup != true {
            textChangedCallback?(textFieldValue)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = textFieldCell.dequeueFromTableView(tableView)
        cell.textField.placeholder = textFieldPlaceholder
        cell.textField.text = textFieldValue
        cell.textField.addTarget(self, action: Selector("textFieldChanged:"), forControlEvents: .EditingChanged)
        cell.textField.clearButtonMode = .Always
        cell.textField.autocorrectionType = autocorrectionType
        cell.textField.keyboardType = keyboardType
        cell.textField.becomeFirstResponder()
        return cell
    }

    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerText
    }
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return footerText
    }
    
    // MARK: UIControl on change handlers
    
    func textFieldChanged(sender : UITextField) {
        if trimWhitespace {
            let trimmedValue = sender.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            textFieldValue = trimmedValue
        } else {
            textFieldValue = sender.text
        }
        
        // Enable/disable done button based on if text is entered
        if textFieldValue != "" {
            doneButton?.enabled = true
        } else {
            doneButton?.enabled = false
        }
    }
    
    func setupDone() {
        textChangedCallback?(textFieldValue)
        navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancelPressed() {
        navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
}
