//
//  CallToActionCell.swift
//  Stitch
//
//  Created by John Hwang on 8/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol CallToActionCellDelegate : class {
    func callToActionActionPressed(cell : CallToActionCell)
    func callToActionDismissPressed(cell : CallToActionCell)
}

class CallToActionCell: UICollectionViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var icon: UIImageView!
    
    weak var delegate : CallToActionCellDelegate?
    //Google Analytics
    var ga_action = "none"
    
    @IBAction func dismissPressed() {
        delegate?.callToActionDismissPressed(self)
    }
    
    override var highlighted : Bool {
        didSet {
            if highlighted {
                backgroundColor = selectedColor
            } else {
                UIView.animateWithDuration(0.25) {
                    self.backgroundColor = self.nibBackgroundColor
                }
            }
        }
    }
    
    override var selected : Bool {
        didSet {
            if selected {
                delegate?.callToActionActionPressed(self)
            }
        }
    }
    
    var selectedColor : UIColor?
    var nibBackgroundColor : UIColor?
    override func awakeFromNib() {
        nibBackgroundColor = backgroundColor
        var hue = CGFloat(0), bright = CGFloat(0), sat = CGFloat(0), alpha = CGFloat(0)
        nibBackgroundColor?.getHue(&hue, saturation: &sat, brightness: &bright, alpha: &alpha)
        selectedColor = UIColor(hue: hue, saturation: sat, brightness: bright * 0.8, alpha: alpha)
    }
}

extension CallToActionCell : NibCollectionViewCell {
    static func nibName() -> String {
        return "CallToActionCell"
    }
}