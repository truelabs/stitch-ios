//
//  LabelSwitchTableViewCell.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelSwitchTableViewCell: UITableViewCell, NibTableViewCell {
    @IBOutlet var label : UILabel!
    @IBOutlet var switchView : UISwitch!
    
    static func nibName() -> String {
        return "LabelSwitchTableViewCell"
    }
}
