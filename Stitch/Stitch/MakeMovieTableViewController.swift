//
//  MakeMovieTableViewController.swift
//  Stitch
//
//  Created by John Hwang on 9/8/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import MediaPlayer
import MWPhotoBrowser

protocol MakeMovieTableViewControllerDelegate : class {
    func makeMovieDidComplete(controller : MakeMovieTableViewController)
    func makeMovieDidCancel(controller : MakeMovieTableViewController)
}

class MakeMovieTableViewController: UITableViewController {
    
    var mediaDetailText = LocalizedString("SELECT")
    var musicDetailText = LocalizedString("NO_SONG_SELECTED")
    var mwpDelegate : MWPDelegate?
    weak var delegate : MakeMovieTableViewControllerDelegate?
    
    private enum Section {
        case Media
        case Music
        case IncludeSharedByText
        case Make
    }
    
    private var sections : [Section] = [
        .Media, .Music, .IncludeSharedByText, .Make
    ]

    private let makeCell = NibTableViewCellReuseIdAndType<LabelTableViewCell>(reuseId: "MakeCell")
    private let mediaCell = NibTableViewCellReuseIdAndType<LabelDetailTableViewCell>(reuseId: "Media")
    private let musicCell = NibTableViewCellReuseIdAndType<LabelDetailTableViewCell>(reuseId: "Music")
    private let switchCell = NibTableViewCellReuseIdAndType<LabelSwitchTableViewCell>(reuseId: "Switch")
    
    init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = LocalizedString("MAKE_MOVIE")
        
        makeCell.registerNib(tableView)
        mediaCell.registerNib(tableView)
        musicCell.registerNib(tableView)
        switchCell.registerNib(tableView)
        
        let cancel = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancel"))
        navigationItem.rightBarButtonItem = cancel
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let screenName = "Make-Movie"
        // Google Analytics
        Analytics.sharedInstance.screenView(screenName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Media:
            return 1
        case .Music:
            return 1
        case .IncludeSharedByText:
            return 1
        case .Make:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .Media:
            return true
        case .Music:
            return true
        case .Make:
            return true
        default:
            return false
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch sections[indexPath.section] {
        
        case .Media:
            let cell = mediaCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("MEDIA")
            cell.detailLabel.text = mediaDetailText
            return cell
        case .Music:
            let cell = musicCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("MUSIC")
            cell.detailLabel.text = musicDetailText
            return cell
        case .IncludeSharedByText:
            let cell = switchCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("SHARED_BY_TEXT")
            cell.switchView.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            cell.switchView.addTarget(self, action: Selector("includeTextSwitchChanged:"), forControlEvents: .ValueChanged)
            cell.switchView.on = false
            return cell
        case .Make:
            let cell = makeCell.dequeueFromTableView(tableView)
            cell.label.text = LocalizedString("MAKE_MOVIE")
            cell.label.textAlignment = .Center
            cell.label.textColor = tableView.tintColor
            return cell
            
        }
    }
    
    var selectedRowIndexPath : NSIndexPath?
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedRowIndexPath = indexPath
        
        switch sections[indexPath.section] {
        case .Media:
            pickMedia()
        case .Music:
            let picker = MPMediaPickerController(mediaTypes: .Music)
            picker.delegate = self
            presentViewController(picker, animated: true, completion: nil)
        case .IncludeSharedByText:
            break
        case .Make:
            makeMovie()
        }
    }

    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        
        switch sections[section] {
        case .Media:
            return LocalizedString("ADD_PHOTOS_FOOTER")
        case .Music:
            return LocalizedString("ADD_MUSIC_FOOTER")
        case .IncludeSharedByText:
            return LocalizedString("SHARED_BY_TEXT_FOOTER")
        case .Make:
            return nil
        }
    }
    
    func makeMovie(){
        delegate?.makeMovieDidComplete(self)
    }
    
    func cancel() {
        delegate?.makeMovieDidCancel(self)
    }

    func includeTextSwitchChanged(sender : UISwitch) {
        log.debug("Display text switch tapped")
        //TODO: Add text as parameter to make movie
    }
}

extension MakeMovieTableViewController : MWPDelegateDelegate {
    func pickMedia() {
        
        //This is just to show something
        //TODO: Load photos of current event, NOT photos library
        
        var assets = [PHAsset]()
        
        let screen : UIScreen
        if let windowScreen = self.view.window?.screen {
            screen = windowScreen
        } else {
            log.info("Couldn't get window's screen. Falling back to main screen.")
            screen = UIScreen.mainScreen()
        }
        
        //Get photos from photos library
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        // TODO: set predicate to only include Image and Videos
        let fetchResults = PHAsset.fetchAssetsWithOptions(options)
        
        fetchResults.enumerateObjectsUsingBlock({ (assetObj, _, _) -> Void in
            assets.append(assetObj as! PHAsset)
        })
        
        let scale = CGFloat(screen.scale)
        // Sizing is very rough... more thought required in a real implementation
        let imageSize = CGFloat(max(screen.bounds.size.width, screen.bounds.size.height) * 1.5)
        let imageTargetSize = CGSizeMake(imageSize * scale, imageSize * scale)
        let thumbTargetSize = CGSizeMake(imageSize / 3.0 * scale, imageSize / 3.0 * scale)
        
        mwpDelegate = MWPDelegate(photos: assets, photoSize: imageTargetSize, thumbSize: thumbTargetSize)
        mwpDelegate?.delegate = self
        
        let browser = MWPhotoBrowser(delegate: mwpDelegate)
        browser.displayActionButton = false
        browser.displaySelectionButtons = true
        browser.enableGrid = true
        browser.startOnGrid = true
        browser.alwaysShowControls = true
        
        let nav = UINavigationController(rootViewController: browser)
        presentViewController(nav, animated: true, completion: nil)
        
    }
    
    func delegateDelegate(del : MWPDelegate, didSelectItems selectedItems: Set<UInt>) {
        //TODO: Actually pass selected items to make movie
        log.debug("Selected \(selectedItems.count) items")
        
        mediaDetailText = (selectedItems.count).description + " " + LocalizedString("SELECTED")
        tableView.reloadRowsAtIndexPaths([selectedRowIndexPath!], withRowAnimation: .None)
    }
}

extension MakeMovieTableViewController : MPMediaPickerControllerDelegate {
    
    func pickMusic(sender : UIView) {
        let picker = MPMediaPickerController(mediaTypes: .Music)
        picker.showsCloudItems = false
        picker.delegate = self
        presentViewController(picker, animated: true, completion: nil)
    }
    
    func mediaPicker(mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        let showError = { (error : NSError?) -> Void in
            let vc = alertWithTitle(LocalizedString("CANNOT_USE_SONG"), error: error)
            mediaPicker.presentViewController(vc, animated: true, completion: nil)
        }
        
        guard let item = mediaItemCollection.items.first else {
            showError(makeError("NO_SONG_SELECTED"))
            return
        }
        
        // item.assetURL is only to be used with AVFoundation framework.
        let assetURLObj : AnyObject? = item[MPMediaItemPropertyAssetURL]
        let assetURL : NSURL! = assetURLObj as? NSURL
        
        if assetURL == nil {
            log.error("Error getting URL for asset")
            showError(makeError("ERROR_DRM_CONTENT"))
            return
        }
        
        // Get song title
        let assetPropertyTitleObj : AnyObject? = item[MPMediaItemPropertyTitle]
        let assetPropertyTitle : String! = assetPropertyTitleObj as? String
        musicDetailText = assetPropertyTitle
        
        tableView.reloadRowsAtIndexPaths([selectedRowIndexPath!], withRowAnimation: .None)

        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController) {
        tableView.deselectRowAtIndexPath(selectedRowIndexPath!, animated: true)
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
    }
}
