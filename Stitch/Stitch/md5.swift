//
//  NSDataExtension.swift
//  Stitch
//
//  Created by Doug Richardson on 8/25/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

extension NSData {
    func md5() -> [UInt8] {
        var hash = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        CC_MD5(self.bytes, CC_LONG(self.length), &hash)
        return hash
    }
}