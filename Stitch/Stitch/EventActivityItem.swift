//
//  EventActivityItem.swift
//  Stitch
//
//  Created by Douglas Richardson on 7/2/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class EventActivityItem : NSObject, UIActivityItemSource {
    let event : Event
    init(event : Event) {
        self.event = event
    }
    
    func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject {
        return NSURL(string:"http://www.example.com")!
    }
    
    func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
        return LocalizedFormatString("SHARE_EVENT_BODY_FORMAT", strings: event.name ?? "", WebAPI.ShareEventURL(event.id).URLString)
    }
    
    func activityViewController(activityViewController: UIActivityViewController, subjectForActivityType activityType: String?) -> String {
        return LocalizedFormatString("SHARE_EVENT_SUBJECT_FORMAT", strings: event.name ?? "")
    }
}