//
//  RecentEvents.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SQLite

final class RecentEvents {
    static let sharedInstance : RecentEvents = {
        let path = applicationSupportDirectory().URLByAppendingPathComponent("recentEvents.db")
        return RecentEvents(path: path)!
    }()
    
    private var db : Connection!
    
    private let events = Table("events")
    private let id = Expression<String>("id")
    private let name = Expression<String>("name")
    private let owner = Expression<String>("owner")
    private let created = Expression<String>("created")
    private let anyoneCanPost = Expression<Bool>("anyoneCanPost")
    private let accessed = Expression<String>("accessed")
    private let callToActionState = Expression<CallToActionState?>("callToActionState")
    private let thumbnailURL = Expression<String?>("thumbnailURL")
    private let subscribed = Expression<Bool>("subscribed")
    private let timeOutstandingLocalNotificationPresented = Expression<Double?>("timeOutstandingLocalNotificationPresented")
    
    required init?(path : NSURL) {
        do {
            db = try Connection(path.absoluteString, readonly: false)
        } catch let e {
            log.error("Error opening database connection. \(e)")
            return nil
        }
        
        if db.userVersion == 0 {
            log.debug("Migration 1")
            do {
                try db.transaction() {
                    try self.db.run(self.events.create() { t in
                        t.column(self.id, primaryKey: true)
                        t.column(self.owner)
                        t.column(self.name)
                        t.column(self.created)
                        t.column(self.anyoneCanPost)
                        t.column(self.accessed)
                        t.column(self.callToActionState)
                        t.column(self.thumbnailURL)
                    })
                    try self.db.run(self.events.createIndex([self.accessed], unique: false, ifNotExists: false))
                    self.db.userVersion = 1
                }
            } catch let e {
                log.error("Error running migration 1: \(e)")
                return nil
            }
        }
        
        if db.userVersion == 1 {
            log.debug("Migration 2")
            do {
                try db.transaction() {
                    try self.db.run(self.events.addColumn(self.subscribed, defaultValue: false))
                    self.db.userVersion = 2
                }
            } catch let e {
                log.error("Error running migration 2: \(e)")
                return nil
            }
        }
        
        if db.userVersion == 2 {
            log.debug("Migration 3")
            do {
                try db.transaction() {
                    try self.db.run(self.events.addColumn(self.timeOutstandingLocalNotificationPresented))
                    self.db.userVersion = 3
                }
            } catch let e {
                log.error("Error running migration 3: \(e)")
                return nil
            }
        }
    }
    
    func accessedEvent(event : Event) {
        let accessed = self.accessed
        let id = self.id
        let db = self.db
        let name = self.name
        let owner = self.owner
        let events = self.events
        let anyoneCanPost = self.anyoneCanPost
        let created = self.created
        
        do {
            try db.transaction(.Deferred) {
                let createdTime = RFC3339FractionalSecondsFormatter.stringFromDate(event.created)
                let thisRowQuery = events.filter(id == event.id)
                if db.pluck(thisRowQuery) != nil {
                    let update = thisRowQuery.update(
                        name <- event.name,
                        owner <- event.owner,
                        anyoneCanPost <- event.anyoneCanPost,
                        created <- createdTime)
                    let updated = try db.run(update)
                    assert(updated == 1)
                } else {
                    let accessedTime = RFC3339FractionalSecondsFormatter.stringFromDate(NSDate())
                    let insert = events.insert(
                        id <- event.id,
                        name <- event.name,
                        owner <- event.owner,
                        anyoneCanPost <- event.anyoneCanPost,
                        created <- createdTime,
                        accessed <- accessedTime)
                    try db.run(insert)
                }
            }
        } catch let r as Result {
            log.error("failed with \(r)")
        } catch let e {
            log.error("unknown failure: \(e)")
        }
    }

    //Remove event from recent events
    func removeEvent(event : Event){
        
        let eventID = events.filter(id == event.id)
        do {
            try db.run(eventID.delete())
        } catch let e {
            log.error("delete failed. \(e)")
        }
        
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "remove_event", label: "recently_joined_events")
        
    }
    
    func allEvents() -> [Event] {
        var r : [Event] = []

        for e in db.prepare(events.order(accessed.desc).select(*)) {
            let createdDate = RFC3339FractionalSecondsFormatter.dateFromString(e[created])!
            let event = Event(id: e[id], name: e[name], owner: e[owner], created: createdDate, anyoneCanPost: e[anyoneCanPost])
            r.append(event)
        }
        return r
    }
    
    func callToActionStateForEventId(eventId : String) -> CallToActionState? {
        let query = events.select(callToActionState).filter(id == eventId)
        if let s = db.pluck(query) {
            return s.get(callToActionState)
        }
        return nil
    }
    
    func setCallToActionState(state : CallToActionState, forEventId eventId : String) {
        do {
            let sql = events.filter(id == eventId).update(callToActionState <- state)
            let updated = try db.run(sql)
            assert(updated == 1)
        } catch let e {
            log.error("Error setting call to action state for event \(eventId). \(e)")
        }
    }

    func getThumbnailURLForEventId(eventId : String) -> String? {
        let query = events.select(thumbnailURL).filter(id == eventId)
        if let s = db.pluck(query) {
            return s[thumbnailURL]
        }
        return nil
    }
    
    func setThumbnailURL(url : String?, forEventId eventId : String) {
        do {
            let sql = events.filter(id == eventId).update(thumbnailURL <- url)
            let updated = try db.run(sql)
            assert(updated == 1)
        } catch let e {
            log.error("Error setting thumbnail for event \(eventId). \(e)")
        }
    }
    
    func getSubscribedForEventId(eventId : String) -> Bool {
        let query = events.select(subscribed).filter(id == eventId)
        if let s = db.pluck(query) {
            return s[subscribed]
        }
        return false
    }
    
    func setSubscribed(subscribed : Bool, forEventId eventId : String) {
        do {
            let sql = events.filter(id == eventId).update(self.subscribed <- subscribed)
            let updated = try db.run(sql)
            assert(updated == 1)
        } catch let e {
            log.error("Error setting subscribed for event \(eventId). \(e)")
        }
    }
    
    func nameForEventId(eventId : String) -> String? {
        let query = events.select(name).filter(id == eventId)
        if let s = db.pluck(query) {
            return s[name]
        }
        return nil
    }
    
    func timeOutstandingLocalNotificationPresentedForEventId(eventId : String) -> NSDate? {
        let query = events.select(timeOutstandingLocalNotificationPresented).filter(id == eventId)
        guard let time = db.pluck(query)?[timeOutstandingLocalNotificationPresented] else {
            return nil
        }
        return NSDate(timeIntervalSinceReferenceDate: time)
    }
    
    func setTimeOutstandingLocalNotificationPresented(date : NSDate?, forEventId eventId : String) {
        do {
            let newValue : Double?
            if let d = date {
                newValue = d.timeIntervalSinceReferenceDate
            } else {
                newValue = nil
            }
            let sql = events.filter(id == eventId).update(self.timeOutstandingLocalNotificationPresented <- newValue)
            let updated = try db.run(sql)
            assert(updated == 1)
        } catch let e {
            log.error("Error setting timeOutstandingLocalNotificationPresented for event \(eventId). \(e)")
        }
    }
}


extension CallToActionState : Value {
    // Database serialization values.
    private static let START = "Start"
    private static let SHOWING_ADD_PHOTOS = "ShowingAddPhotos"
    private static let SHOWING_ALLOW_NOTIFICATIONS = "ShowingAllowNotifications"
    private static let SHOWING_INVITE_FRIENDS = "ShowingInviteFriends"
    private static let SHOWING_MAKE_MOVIE = "ShowingMakeMovie"
    private static let WAITING_TO_SHOW_MAKE_MOVIE = "WaitingToShowMakeMovie"
    private static let END = "End"
    
    static var declaredDatatype: String {
        return String.declaredDatatype
    }
    static func fromDatatypeValue(datatypeValue: String) -> CallToActionState {
        switch datatypeValue {
        case START:
            return .Start
        case SHOWING_ADD_PHOTOS:
            return .ShowingAddPhotos
        case SHOWING_ALLOW_NOTIFICATIONS:
            return .ShowingAllowNotifications
        case SHOWING_INVITE_FRIENDS:
            return .ShowingInviteFriends
        case SHOWING_MAKE_MOVIE:
            return .ShowingMakeMovie
        case WAITING_TO_SHOW_MAKE_MOVIE:
            return .WaitingToShowMakeMovie
        case END:
            return .End
        default:
            assert(false, "Invalid datatypeValue \(datatypeValue)")
            return .End
        }
    }
    var datatypeValue: String {
        switch self {
        case .Start:
            return CallToActionState.START
        case .ShowingAddPhotos:
            return CallToActionState.SHOWING_ADD_PHOTOS
        case .ShowingAllowNotifications:
            return CallToActionState.SHOWING_ALLOW_NOTIFICATIONS
        case .ShowingInviteFriends:
            return CallToActionState.SHOWING_INVITE_FRIENDS
        case .ShowingMakeMovie:
            return CallToActionState.SHOWING_MAKE_MOVIE
        case .WaitingToShowMakeMovie:
            return CallToActionState.WAITING_TO_SHOW_MAKE_MOVIE
        case .End:
            return CallToActionState.END
        }
    }
}
