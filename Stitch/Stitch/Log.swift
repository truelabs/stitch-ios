//
//  Log.swift
//  Stitch
//
//  Created by Doug Richardson on 10/8/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import XCGLogger
import CoreFoundation

let log : XCGLogger = {
    let l = XCGLogger.defaultInstance()
    #if DEBUG
        let d = Int64(CFAbsoluteTimeGetCurrent())
        let logPath = cacheDirectory().URLByAppendingPathComponent("stitch-log-\(d).txt")
        l.setup(writeToFile: logPath)
    #endif
    return l
}()