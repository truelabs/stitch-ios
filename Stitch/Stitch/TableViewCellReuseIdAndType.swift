//
//  TableViewCellReuseIdAndType.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class TableViewCellReuseIdAndType<T : UITableViewCell> {
    let reuseId : String
    
    init(reuseId: String) {
        self.reuseId = reuseId
    }
    
    final func dequeueFromTableView(tableView : UITableView) -> T {
        if let cell = tableView.dequeueReusableCellWithIdentifier(reuseId) as? T {
            return cell
        }
        return T(style: .Default, reuseIdentifier: reuseId)
    }
}

class NibTableViewCellReuseIdAndType<T where T:NibTableViewCell, T:UITableViewCell> : TableViewCellReuseIdAndType<T> {
    
    override init(reuseId: String) {
        super.init(reuseId: reuseId)
    }

    final func registerNib(tableView : UITableView) {
        tableView.registerNib(UINib(nibName: T.nibName(), bundle: nil), forCellReuseIdentifier: reuseId)
    }
}

protocol NibTableViewCell {
    static func nibName() -> String
}
