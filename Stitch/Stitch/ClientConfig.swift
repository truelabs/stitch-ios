//
//  ClientConfig.swift
//  Stitch
//
//  Created by Douglas Richardson on 7/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class ClientConfig : NSObject, NSURLSessionDelegate {
    
    static let sharedInstance = ClientConfig()
    
    func iAdEnabled(completion : (enabled : Bool) -> ()) {
        boolForKey("iad", defaultValue: false, completion: completion)
    }
    
    func admobEnabled(completion : (enabled : Bool) -> ()) {
        boolForKey("admob", defaultValue: false, completion: completion)
    }
    
    func videoTimeLimit(completion : (time: Double) -> ()){
        doubleForKey("max_video_duration", defaultValue: 15, completion: completion)
    }
    
    private let session : NSURLSession = {
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPMaximumConnectionsPerHost = 1
        config.timeoutIntervalForResource = 15
        return NSURLSession(configuration: config)
    }()
    
    private var clientConfigJSONFingerprint : HTTPEntityFingerprint?
    private var clientConfigJSON = JSON(NSNull())
    private let userPrefs = UserPreferences()
    
    private func getConfig(completion : (json : JSON?) -> ()) {
        
        // Rely on the underlying NSURLCache and the server setting appropriate Cache-Control max-age
        // headers to control caching of the client config. This allows the server to control client
        // config update policy.
        
        assert(NSThread.isMainThread())
        
        let prodConfigURL = "https://storage.googleapis.com/stitch-client-config/client_config.json"
        let devConfigURL = "https://storage.googleapis.com/stitch-client-config/dev_client_config.json"
        //let devConfigURL = "http://localhost:8000/client_config.json" // go run server.go in stitch-backend/client_config
        let configURL = userPrefs.useDeveloperClientConfig ? devConfigURL : prodConfigURL
        
        // When no result is cached, if multiple requests are made (e.g., to get multiple client config values)
        // then multiple requests would needelessly be made to the backend. To prevent this, create a queue of
        // closures which delay the request from being scheduled until previous requests have returned.
        
        let theURL = NSURL(string: configURL)!
        
        requestQueue.append({
            
            let task = self.session.dataTaskWithURL(theURL) { (data, res, err) -> Void in
                if let e = err {
                    log.error("ClientConfig: An error occurred retreiving config: \(e)")
                } else if let d = data {
                    
                    // Get a fingerprint of the response, so we can limit deserialization of the JSON
                    // to only when the resource has changed.
                    
                    let fingerprint = HTTPEntityFingerprint(response: res)
                    let fingerprintsMatch = HTTPEntityFingerprint.match(self.clientConfigJSONFingerprint, second: fingerprint)
                    
                    if !fingerprintsMatch {
                        let fingerprintDebugStr = fingerprint?.debugString() ?? "nil"
                        log.debug("ClientConfig: Fingerprints don't match. Deserializing data with fingerprint: \(fingerprintDebugStr)")
                        var e : NSError?
                        let j = JSON(data: d, options: NSJSONReadingOptions(), error: &e)
                        if let theE = e {
                            log.error("ClientConfig: Error deserializing json: \(theE)")
                        } else if j.type != .Null {
                            self.clientConfigJSONFingerprint = fingerprint
                            self.clientConfigJSON = j
                        } else {
                            log.error("ClientConfig: Got unexpected null JSON type")
                        }
                    } else {
                        // log.debug("ClientConfig: Fingerprints match. Using cached deserialized JSON")
                    }
                    
                    completion(json: self.clientConfigJSON)
                }
                
                self.processNextRequest()
            }
            
            task.resume()
        })
        
        if currentRequest == nil {
            processNextRequest()
        }
    }
    
    private func urlForKey(key : String, completion : (urlFragment : String?) -> ()) {
        getConfig { (json) -> () in
            completion(urlFragment: json?["urls"][key].string)
        }
    }
    
    private func boolForKey(key : String, defaultValue : Bool, completion : (boolValue : Bool) -> ()) {
        getConfig { (json) -> () in
            completion(boolValue : json?[key].bool ?? defaultValue)
        }
    }
    
    private func doubleForKey(key : String, defaultValue : Double, completion : (doubleValue : Double) -> ()) {
        getConfig { (json) -> () in
            completion(doubleValue : json?[key].double ?? defaultValue)
        }
    }
    
    // Make sure only one request is made for the client configuration at a time. You can't just rely on
    // NSURLSession HTTPMaxConcurrentConnections because from testing, if the requests are queued up
    // they'll still hit the server instead of using the cached results from the first queued request.
    typealias RequestBlock = () -> ()
    private var requestQueue : [RequestBlock] = []
    private var currentRequest : RequestBlock?
    
    private func processNextRequest() {
        if requestQueue.count > 0 {
            let req = requestQueue.removeAtIndex(0)
            currentRequest = req
            req()
        } else {
            currentRequest = nil
        }
    }
}