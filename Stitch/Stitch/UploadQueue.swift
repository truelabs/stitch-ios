//
//  UploadQueue.swift
//  Stitch
//
//  Created by Doug Richardson on 8/20/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Alamofire
import Foundation
import SQLite
import SwiftyBase64
import SwiftyHex
import SwiftyJSON
import SwiftyUUID

// Provides a way to obtain the completion handler given to the AppDelegate in
// application(_:handleEventsForBackgroundURLSession:completionHandler:).
protocol UploadQueueFinishSessionEventsCompletionHandlerProvider {
    func uploadQueueWifiOnlyBackgroundSessionCompletionHandler() -> (() -> Void)?
    func uploadQueueCellAndWifiBackgroundSessionCompletionHandler() -> (() -> Void)?
}

struct UploadProgress {
    let uploadId : String
    let totalBytesSent : Int64
    let totalBytesExpectedToSend : Int64
}

extension UploadProgress {
    // 0.0-1.0
    var percentage : Float {
        if totalBytesExpectedToSend == 0 {
            return 0
        }
        return Float(totalBytesSent) / Float(totalBytesExpectedToSend)
    }
}

class UploadProgressWrapper : NSObject {
    let uploadProgress : UploadProgress
    
    init(uploadProgress : UploadProgress) {
        self.uploadProgress = uploadProgress
    }
}

struct UploadQueueItem {
    let uploadId : String
    let eventId : String
    let contentMd5 : String
    let state : UploadItemState
    let mime : String
    let file : String
}

class UploadQueueItemWrapper : NSObject {
    let uploadQueueItem : UploadQueueItem
    
    init(uploadQueueItem : UploadQueueItem) {
        self.uploadQueueItem = uploadQueueItem
    }
}

enum UploadItemState {
    case Waiting
    case Uploading
    case Success
    case Failure
}

private enum UploadItemStateErrors : ErrorType {
    case InvalidDatatypeValue
}

extension UploadItemState : Value {
 
    // Database serialization values.
    private static let WAITING = "Waiting"
    private static let UPLOADING = "Uploading"
    private static let SUCCESS = "Success"
    private static let FAILURE = "Failure"
    
    static var declaredDatatype: String {
        return String.declaredDatatype
    }
    static func fromDatatypeValue(datatypeValue: String) -> UploadItemState {
        switch datatypeValue {
        case WAITING:
            return .Waiting
        case UPLOADING:
            return .Uploading
        case SUCCESS:
            return .Success
        case FAILURE:
            return .Failure
        default:
            assert(false, "Invalid datatypeValue \(datatypeValue)")
            return .Failure
        }
    }
    var datatypeValue: String {
        switch self {
        case .Waiting:
            return UploadItemState.WAITING
        case .Uploading:
            return UploadItemState.UPLOADING
        case .Success:
            return UploadItemState.SUCCESS
        case .Failure:
            return UploadItemState.FAILURE
        }
    }
}

// uploads table
private let uploads = Table("uploads")
private let uploadIdColumn = Expression<String>("uploadId")
private let eventIdColumn = Expression<String>("eventId")
private let dateAddedColumn = Expression<Int>("dateAdded") // date added to queue
private let contentMd5Column = Expression<String>("contentMd5") // dataid for the server
private let uploadUrlColumn = Expression<String?>("uploadUrl") // signed upload URL
private let mimeColumn = Expression<String>("mime")
private let fileColumn = Expression<String>("file") // file in our app data container during upload
private let attemptColumn = Expression<Int>("attempt") // count of attempts
private let stateColumn = Expression<UploadItemState>("state")

final class UploadQueue : NSObject {
    
    //
    // Notifications
    //
    
    // progress
    static let ProgressNotificationName = "UploadQueueProgress"
    static let ProgressNotificationInfoKey = "Progress"
    
    // items added/removed from queue
    static let ItemAddedNotificationName = "UploadQueueItemAdded"
    static let ItemRemovedNotificationName = "UploadQueueItemRemoved"
    static let ItemStateChangedNotificationName = "UploadQueueItemStateChanged"
    static let ItemKey = "Item"
    
    //
    // NSURLSession background session support
    //
    private let finishEventsHandlerProvider : UploadQueueFinishSessionEventsCompletionHandlerProvider
    
    //
    // Queue Processing State
    //
    private var isProcessing = false
    private var currentProgress : UploadProgress?
    private var currentUploadTask : NSURLSessionTask?
    
    //
    // Queue Database
    //
    private let db : Connection
    
    //
    // WiFi or Cell Only upload support
    //
    private var urlSessionCellAndWifi : NSURLSession!
    private var urlSessionWifiOnly : NSURLSession!
    
    init(dbpath : String, wifiOnlyBackgroundSessionId : String, cellAndWifiBackgroundSessionId : String, finishEventsHandlerProvider : UploadQueueFinishSessionEventsCompletionHandlerProvider) {
        db = try! Connection(dbpath, readonly: false)
        self.finishEventsHandlerProvider = finishEventsHandlerProvider
        
        super.init()
        
        if db.userVersion == 0 {
            // First migration
            log.debug("First Migration running")
            do {
                try db.transaction() {
                    try self.db.run(uploads.create() { t in
                        t.column(uploadIdColumn, primaryKey : true)
                        t.column(eventIdColumn)
                        t.column(dateAddedColumn)
                        t.column(contentMd5Column)
                        t.column(uploadUrlColumn)
                        t.column(mimeColumn)
                        t.column(fileColumn)
                        t.column(attemptColumn)
                        t.column(stateColumn)
                    })
                    try self.db.run(uploads.createIndex([dateAddedColumn], unique: false, ifNotExists: false))
                    self.db.userVersion = 1
                }
            } catch let e {
                log.error("Error running first migration. \(e)")
            }
        }
        
        let configCellAndWifi = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(cellAndWifiBackgroundSessionId)
        configCellAndWifi.sessionSendsLaunchEvents = true
        configCellAndWifi.discretionary = false
        configCellAndWifi.allowsCellularAccess = true
        urlSessionCellAndWifi = NSURLSession(configuration: configCellAndWifi, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        let configWifiOnly = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(wifiOnlyBackgroundSessionId)
        configWifiOnly.sessionSendsLaunchEvents = true
        configWifiOnly.discretionary = false
        configWifiOnly.allowsCellularAccess = false
        urlSessionWifiOnly = NSURLSession(configuration: configWifiOnly, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
    }
    
    private func postItemNotification(notificationName : String, uploadId : String, eventId : String, contentMd5 : String, state : UploadItemState, mime : String, file : String) {
        let userInfo : [String : AnyObject] = [
            UploadQueue.ItemKey : UploadQueueItemWrapper(uploadQueueItem:UploadQueueItem(uploadId: uploadId, eventId: eventId, contentMd5 : contentMd5, state : state, mime : mime, file : file)),
        ]
        let note = NSNotification(name: notificationName, object: self, userInfo: userInfo)
        NSNotificationCenter.defaultCenter().postNotification(note)
    }
    
    private enum Errors : ErrorType {
        case UploadDirectoryCreationError
        case UnexpectedError(String)
        case MIMETypeCouldNotBeDeterminedError
    }
    
    // All UploadQueues share the same upload queue directory.
    static private func uploadDirectory() throws -> NSURL {
        let uploadDirectory = applicationSupportDirectory().URLByAppendingPathComponent("UploadQueueUploads")
        guard let uploadDirectoryPath = uploadDirectory.path else {
            throw Errors.UploadDirectoryCreationError
        }
        let fm = NSFileManager.defaultManager()
        
        var isDirectory : ObjCBool = false
        var exists = fm.fileExistsAtPath(uploadDirectoryPath, isDirectory: &isDirectory)
        
        if exists && !isDirectory {
            log.info("File exists at upload directory path \(uploadDirectoryPath), but is not a directory. Removing.")
            try fm.removeItemAtPath(uploadDirectoryPath)
            exists = false
        }
        
        if !exists {
            log.debug("Creating upload directory \(uploadDirectory).")
            try fm.createDirectoryAtURL(uploadDirectory, withIntermediateDirectories: true, attributes: nil)
            try uploadDirectory.setResourceValue(true, forKey: NSURLIsExcludedFromBackupKey)
        }
        
        return uploadDirectory
    }
    
    // Add an item to the UploadQueue. The UploadQueue takes ownership of tmpPath, moving it to it's
    // owned managed directory. Once the file is finished uploading, the file will be removed.
    // itemQueueHandler will be called once the item is queued successfully or there is an error
    // queueing the item.
    // UploadQueue only takes ownership if itemQueueHandler is called with a nil error.
    func queueFile(tmpPath : String, uti : String, eventId : String, itemQueuedHandler : (String?, ErrorType?) ->()) {
        assert(NSThread.isMainThread())
        
        log.debug("adding \(tmpPath) with uti \(uti)")
        
        //
        // Move file at tmpPath into a directory managed by UploadQueue.
        //
        
        let uploadDirectory : NSURL
        do {
            uploadDirectory = try UploadQueue.uploadDirectory()
        } catch let e {
            log.error("failed to get upload directory. \(e)")
            itemQueuedHandler(nil, e)
            return
        }
        
        let uploadId = SwiftyUUID.UUID().CanonicalString()
        let uploadFile = uploadDirectory.URLByAppendingPathComponent(uploadId)
        guard let uploadFilePath = uploadFile.path else {
            log.error("couldn't get path for upload file")
            itemQueuedHandler(nil, Errors.UnexpectedError("couldn't get path for upload file"))
            return
        }
        
        do {
            try NSFileManager.defaultManager().moveItemAtPath(tmpPath, toPath: uploadFilePath)
        } catch let e {
            log.error("couldn't move file from \(tmpPath) to \(uploadFilePath). \(e)")
            itemQueuedHandler(nil, e)
            return
        }
        
        guard let mime = UTTypeToMIME(uti) else {
            log.error("couldn't get mime type for uti")
            itemQueuedHandler(nil, Errors.MIMETypeCouldNotBeDeterminedError)
            return
        }
        
        log.debug("using mime type \(mime)")
        
        // Compute MD5 off main thread.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let data : NSData
            do {
                data = try NSData(contentsOfFile: uploadFilePath, options: NSDataReadingOptions.DataReadingMappedAlways)
            } catch let e {
                log.error("error memory mapping \(uploadFilePath) to compute MD5. \(e)")
                itemQueuedHandler(nil, e)
                return
            }
            
            let contentMd5 = SwiftyHex.Encode(data.md5())
            
            // Bounce back to main thread after computing MD5.
            dispatch_async(dispatch_get_main_queue()) {
                log.debug("adding asset")
                do {
                    let initialState = UploadItemState.Waiting
                    try self.db.run(uploads.insert(or: .Fail,
                        uploadIdColumn <- uploadId,
                        eventIdColumn <- eventId,
                        dateAddedColumn <- Int(NSDate().timeIntervalSince1970),
                        fileColumn <- uploadFilePath,
                        mimeColumn <- mime,
                        contentMd5Column <- contentMd5,
                        attemptColumn <- 0,
                        stateColumn <- initialState
                    ))
                    self.postItemNotification(UploadQueue.ItemAddedNotificationName, uploadId: uploadId, eventId: eventId, contentMd5 : contentMd5, state : initialState, mime : mime, file : uploadFilePath)
                    self.start()
                    
                    itemQueuedHandler(uploadId, nil)
                    return
                } catch let e {
                    log.error("error adding file to database. \(e)")
                    itemQueuedHandler(nil, e)
                    return
                }
            }
        }
    }
    
    func removeUploadId(uploadId : String) {
        assert(NSThread.isMainThread())
        
        if currentProgress?.uploadId == uploadId {
            currentUploadTask?.cancel()
        }
        
        deleteEntryByUploadId(uploadId)
    }
    
    func start() {
        assert(NSThread.isMainThread())
        if !isProcessing {
            isProcessing = true
            processNext()
        }
    }
    
    func retryFailedWithUploadId(uploadId : String) {
        assert(NSThread.isMainThread())
        
        let query = uploads.filter(uploadIdColumn == uploadId)
        guard let row = db.pluck(query) else {
            assert(false, "No row for upload ID \(uploadId)")
            return
        }
        
        let state = row.get(stateColumn)
        if state != .Failure {
            assert(false, "Trying to retry entry that hasn't failed. \(state)")
            return
        }
        
        do {
            try db.run(query.update(attemptColumn <- 0))
            setItemState(.Waiting, byQuery: query)
            start()
        } catch let e {
            log.error("Error updating failed entry. \(e)")
        }
    }
    
    private func processNext() {
        
        assert(NSThread.isMainThread())
        
        log.debug("processing next")
        
        currentProgress = nil
        currentUploadTask = nil
        
        if !isProcessing {
            return
        }
        
        // Select the next item whose state is in waiting (or uploading just in case one gets stuck in uploading).
        guard let row = db.pluck(uploads.order(dateAddedColumn.asc).filter(stateColumn == .Waiting || stateColumn == .Uploading)) else {
            log.debug("no more items in queue, stopping")
            isProcessing = false
            return
        }
        
        // Get the next entry from the database.
        log.debug("Got row to process")
        let uploadId = row[uploadIdColumn]
        let eventId = row[eventIdColumn]
        let attempt = row[attemptColumn]
        let file = row[fileColumn]
        let mime = row[mimeColumn]
        let contentMd5 = row[contentMd5Column]
        
        let MaxAttempts = 5
        
        if attempt >= MaxAttempts {
            log.info("reach max attempts \(attempt). Removing \(uploadId) from queue.")
            setFailedEntryByUploadId(uploadId)
            processNext()
            return
        } else {
            // update the attempts counter.
            do {
                try db.run(uploads.filter(uploadIdColumn == uploadId).update(attemptColumn <- (attempt + 1)))
            } catch let e {
                log.error("error updating attempt column. \(e). Removing \(uploadId) from queue.")
                setFailedEntryByUploadId(uploadId)
                processNext()
                return
            }
        }
        
        startUploadWithUploadId(uploadId, eventId: eventId, contentMd5Hex: contentMd5, mime: mime, file: file)
    }
    
    private func startUploadWithUploadId(uploadId : String, eventId : String, contentMd5Hex : String, mime : String, file : String) {
        
        assert(NSThread.isMainThread())
        
        currentProgress = UploadProgress(uploadId: uploadId, totalBytesSent: 0, totalBytesExpectedToSend: 0)
        setItemState(.Uploading, byUploadId: uploadId) // call after setting currentProgress above since this sends a notification
        
        let (md5, ok) = SwiftyHex.Decode(contentMd5Hex)
        if !ok {
            log.error("failed to decode content MD5 hex \(contentMd5Hex)")
            setFailedEntryByUploadId(uploadId)
            processNext()
            return
        }
        
        let fileUrl = NSURL(fileURLWithPath: file)
        
        log.debug("Requesting upload URL for content-md5 = \(contentMd5Hex)")
        WebAPI.DataUploadRequest(eventId, deviceId: DeviceId, contentMd5: contentMd5Hex, contentType: mime).responseSwiftyJSON({(_, _, json, err) -> Void in
            
            assert(NSThread.isMainThread())
            
            if uploadId != self.currentProgress?.uploadId {
                // uploadId was cancelled since we issued the DataUploadRequest. Stop here
                // so that the file doesn't get uploaded. If the user cancels after this point
                // then the upload task itself will get cancelled. All this is serialized on the
                // main thread.
                log.info("upload cancelled, going to next.")
                self.processNext()
                return
            }
            
            log.debug("Upload URL request returned")
            if err != nil {
                log.error("Upload URL request returned error \(err). Will retry.")
                self.processNext()
                return
            }
            guard let urlStr = json?["url"].string else {
                log.error("Upload URL request didn't return URL. Will retry.")
                self.processNext()
                return
            }
            guard let url = NSURL(string: urlStr) else {
                log.error("Failed to parse URL as NSURL \(urlStr). Will retry.")
                self.processNext()
                return
            }
            
            do {
                let update = uploads.filter(uploadIdColumn == uploadId).update(uploadUrlColumn <- urlStr)
                let changes = try self.db.run(update)
                log.debug("Updated \(uploadId) with upload url \(urlStr). \(changes) changes.")
            } catch let e {
                log.error("Failed to update \(uploadId) with upload url. Removing upload. Failure reason: \(e)")
                self.setFailedEntryByUploadId(uploadId)
                self.processNext()
                return
            }
  
            let contentMd5Base64 = SwiftyBase64.EncodeString(md5, alphabet: .Standard)
            
            let req = NSMutableURLRequest(URL: url)
            req.HTTPMethod = "PUT"
            req.allHTTPHeaderFields?["Content-Type"] = mime
            req.allHTTPHeaderFields?["Content-MD5"] = contentMd5Base64
            
            let session : NSURLSession
            if UserPreferences().disableCellularDataForUploads {
                session = self.urlSessionWifiOnly
            } else {
                session = self.urlSessionCellAndWifi
            }
            log.debug("starting upload task on session \(session.configuration.identifier)")
            
            guard let filePath = fileUrl.path else {
                log.error("failed to get file path from file URL \(fileUrl).")
                self.processNext()
                return
            }
            
            // Make sure the file exists before creating an upload task, because uploadTaskWithRequest
            // throws an objective-c exception if no file exists.
            let fm = NSFileManager()
            guard fm.fileExistsAtPath(filePath) else {
                log.error("missing file for upload \(uploadId). Expect file at path \(filePath)")
                self.setFailedEntryByUploadId(uploadId)
                self.processNext()
                return
            }
            
            let uploadTask = session.uploadTaskWithRequest(req, fromFile: fileUrl)
            
            log.debug("resuming upload task")
            self.currentUploadTask = uploadTask
            uploadTask.resume()
        })
    }
    
    private func setFailedEntryByUploadId(uploadId : String) {
        setItemState(.Failure, byUploadId: uploadId)
    }
    
    private func setItemState(state : UploadItemState, byUploadId uploadId : String) {
        setItemState(state, byQuery: uploads.filter(uploadIdColumn == uploadId))
    }
    
    private func setItemState(state : UploadItemState, byQuery query : QueryType) {
        do {
            try db.run(query.update(stateColumn <- state))
            
            guard let row = db.pluck(query) else {
                log.error("ERROR: didn't get expected row for query.")
                return
            }
            
            let uploadId = row[uploadIdColumn]
            let eventId = row[eventIdColumn]
            let contentMd5 = row[contentMd5Column]
            let mime = row[mimeColumn]
            let file = row[fileColumn]
            postItemNotification(UploadQueue.ItemStateChangedNotificationName, uploadId: uploadId, eventId: eventId, contentMd5 : contentMd5, state : state, mime : mime, file : file)
        } catch let e {
            log.error("setItemState \(state) failed. \(e)")
        }
    }
    
    private func deleteEntryByUploadId(uploadId : String) {
        deleteEntryByQuery(uploads.filter(uploadIdColumn == uploadId))
    }
    
    private func deleteEntryByQuery(query : QueryType) {
        
        guard let row = db.pluck(query) else {
            log.error("No row to delete")
            return
        }
        
        let file = row[fileColumn]
        
        do {
            log.debug("removing item \(file)")
            try NSFileManager.defaultManager().removeItemAtPath(file)
        } catch let e {
            log.error("Error removing file at \(file). Error: \(e)")
        }
        
        let uploadId = row[uploadIdColumn]
        let eventId = row[eventIdColumn]
        let contentMd5 = row[contentMd5Column]
        let state = row.get(stateColumn)
        let mime = row[mimeColumn]
        
        do {
            let changes = try db.run(query.delete())
            log.debug("deleted queue entry \(changes) changes.")
        } catch let e {
            log.error("delete failed: \(e)")
        }
        
        postItemNotification(UploadQueue.ItemRemovedNotificationName, uploadId: uploadId, eventId: eventId, contentMd5 : contentMd5, state : state, mime : mime, file : file)
    }
    
    func uploadItemsForEventId(eventId : String) -> [UploadQueueItem] {
        assert(NSThread.isMainThread())
        
        var uploadIds : [UploadQueueItem] = []
        
        let seq = db.prepare(uploads.filter(eventIdColumn == eventId).order(dateAddedColumn))
        for row in seq {
            let uploadId = row[uploadIdColumn]
            let contentMd5 = row[contentMd5Column]
            let state = row.get(stateColumn)
            let mime = row[mimeColumn]
            let file = row[fileColumn]
            let item = UploadQueueItem(uploadId: uploadId , eventId: eventId, contentMd5 : contentMd5, state: state, mime : mime, file : file)
            uploadIds.append(item)
        }
        
        return uploadIds
    }
    
    func uploadProgressForUploadId(uploadId : String) -> UploadProgress? {
        guard let progress = currentProgress else {
            return nil
        }
        
        guard progress.uploadId == uploadId else {
            return nil
        }
        
        return progress
    }
}

extension UploadQueue : NSURLSessionTaskDelegate {
    
    func URLSessionDidFinishEventsForBackgroundURLSession(session: NSURLSession) {
        
        let id = session.configuration.identifier
        
        log.debug("finish event for background session with id \(id)")
        
        if id == urlSessionWifiOnly?.configuration.identifier {
            if let handler = finishEventsHandlerProvider.uploadQueueWifiOnlyBackgroundSessionCompletionHandler() {
                log.debug("calling wifi only handler")
                handler()
            } else {
                log.debug("wifi only handler is nil")
            }
        } else if id == urlSessionCellAndWifi.configuration.identifier {
            if let handler = finishEventsHandlerProvider.uploadQueueCellAndWifiBackgroundSessionCompletionHandler() {
                log.debug("calling cell and wifi handler")
                handler()
            } else {
                log.debug("cell and wifi handler is nil")
            }
        } else {
            log.error("unexpected id \(id)")
        }
    }
    
    func URLSession(session: NSURLSession, didBecomeInvalidWithError error: NSError?) {
        log.debug("URLSession became invalid \(error)")
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        assert(NSThread.isMainThread())
        log.debug("task completed with error: \(error)")
        
        if let err = error {
            log.error("error \(err)")
            processNext()
            return
        }
        
        guard let r = task.response as? NSHTTPURLResponse else {
            log.error("image upload failed. \(error), \(task.response)")
            processNext()
            return
        }
        
        log.debug("upload success")
        guard let url = task.originalRequest?.URL?.absoluteString else {
            log.error("couldn't get original URL as string. Bailing out.")
            assert(false)
            return
        }
        
        let thisUploadQuery = uploads.filter(uploadUrlColumn == url)
        
        log.debug("statusCode = \(r.statusCode)")
        if !(r.statusCode >= 200 && r.statusCode < 300) {
            log.error("failure HTTP response uploading image")
            if r.statusCode == 400 /* bad request */ || r.statusCode == 403 /* forbidden */ {
                log.error("permanent failure, not going to retry.")
                // These status codes indicate the upload shouldn't be retried, so
                // remove the entry from the queue.
                setItemState(.Failure, byQuery: thisUploadQuery)
            }
            processNext()
            return
        }
        
        guard let upload = db.pluck(thisUploadQuery) else {
            log.error("couldn't find row for url \(url)")
            processNext()
            return
        }
        
        let contentMd5Hex = upload[contentMd5Column]
        
        WebAPI.putUploadCompleteEventId(upload[eventIdColumn], contentMd5Hex: contentMd5Hex) { (error) in
            if error != nil {
                log.error("failed to associate upload with event")
                self.processNext()
                return
            }
            
            log.debug("associated upload with event")
            
            self.setItemState(.Success, byQuery: thisUploadQuery)
            self.processNext()
        }
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        assert(NSThread.isMainThread())
        //log.debug("progress sent \(bytesSent) bytes (\(totalBytesSent) of \(totalBytesExpectedToSend))")
        
        guard let uploadId = currentProgress?.uploadId else {
            log.error("got progress without currentProgress being set")
            return
        }
        
        let progress = UploadProgress(uploadId: uploadId, totalBytesSent: totalBytesSent, totalBytesExpectedToSend: totalBytesExpectedToSend)
        currentProgress = progress
        let info = UploadProgressWrapper(uploadProgress:progress)
        
        let userInfo : [NSObject : AnyObject] = [
            UploadQueue.ProgressNotificationInfoKey : info
        ]
        
        let note = NSNotification(name: UploadQueue.ProgressNotificationName, object: self, userInfo: userInfo)
        
        NSNotificationCenter.defaultCenter().postNotification(note)
    }
}
