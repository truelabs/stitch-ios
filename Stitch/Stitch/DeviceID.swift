//
//  DeviceID.swift
//  Stitch
//
//  Created by Douglas Richardson on 7/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

let DeviceId : String = {
    let key = "deviceId"
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    if let id = userDefaults.stringForKey(key) {
        log.debug("Found existing device ID: \(id)")
        return id
    }
    
    let id = WebAPIUniqueId()
    userDefaults.setObject(id, forKey: key)
    log.debug("Created new device Id: \(id)")
    return id
}()

let DevicePassword : String = {
    let key = "devicePassword"
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    if let id = userDefaults.stringForKey(key) {
        log.debug("Found existing device password: \(id)")
        return id
    }
    
    let id = WebAPIUniqueId()
    userDefaults.setObject(id, forKey: key)
    log.debug("Created new device password: \(id)")
    return id
}()

private let deviceUsernameKey = "deviceUsername"

var DeviceUsername : String? {
    get {
        return NSUserDefaults.standardUserDefaults().stringForKey(deviceUsernameKey)
    }

    set {
        NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: deviceUsernameKey)
    }
}

private let deviceTokenKey = "deviceToken"

var DeviceToken : String? {
get {
    return NSUserDefaults.standardUserDefaults().stringForKey(deviceTokenKey)
}

set {
    NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: deviceTokenKey)
}
}

