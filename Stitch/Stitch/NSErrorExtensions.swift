//
//  NSErrorExtensions.swift
//  Stitch
//
//  Created by Doug Richardson on 9/24/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import Foundation

func NSErrorFromErrorType(errorType : ErrorType?) -> NSError? {
    if let e = errorType as? NSError {
        return e
    }
    
    if let e = errorType {
        log.error("Couldn't get NSError from \(e). Using generic error message.")
        return makeError("UNKNOWN_ERROR")
    }
    
    return nil
}

