//
//  UserPreferences.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

class UserPreferences {
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    private let useDeveloperClientConfigKey = "useDeveloperClientConfig"
    var useDeveloperClientConfig : Bool {
        get {
            return userDefaults.boolForKey(useDeveloperClientConfigKey)
        }
        set {
            userDefaults.setBool(newValue, forKey: useDeveloperClientConfigKey)
        }
    }
    
    private let disableCellularDataForUploadsKey = "disableCellularDataForUploads"
    var disableCellularDataForUploads : Bool {
        get {
            return userDefaults.boolForKey(disableCellularDataForUploadsKey)
        }
        set {
            userDefaults.setBool(newValue, forKey: disableCellularDataForUploadsKey)
        }
    }
}
