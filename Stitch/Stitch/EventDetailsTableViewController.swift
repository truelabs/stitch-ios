//
//  EventDetailsTableViewController.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

protocol EventDetailsTableViewControllerDelegate : class {
    func eventDetails(controller : EventDetailsTableViewController, didUpdateEvent : Event)
    func eventDetailsDidCancel(controller : EventDetailsTableViewController)
}

class EventDetailsTableViewController: UITableViewController {
    
    var eventName : String?
    var notificationsEnabled = true
    var anyoneCanPost = true
    
    weak var delegate : EventDetailsTableViewControllerDelegate?
    
    enum Mode {
        case Create
        case Edit(Event)
    }
    
    var mode : Mode
    
    private enum Row {
        case Name
        case Notifications
        case AnyoneCanPost
    }
    
    private struct Section {
        let rows : [Row]
        let footerText : String?
    }
    
    private var saveButton : UIBarButtonItem?
    
    private let textFieldCell = NibTableViewCellReuseIdAndType<LabelTextFieldTableViewCell>(reuseId: "TextField")
    private let switchCell = NibTableViewCellReuseIdAndType<LabelSwitchTableViewCell>(reuseId: "Switch")
    
    private var sectionsOwner : [Section] = [
        Section(rows: [.Name], footerText: nil),
        Section(rows: [.Notifications], footerText: LocalizedString("NOTIFICATIONS_HELP")),
        Section(rows: [.AnyoneCanPost], footerText: LocalizedString("ANYONE_CAN_POST_HELP"))
    ]
    
    private var sectionsNonowner : [Section] = [
        Section(rows: [.Notifications], footerText: LocalizedString("NOTIFICATIONS_HELP"))
    ]
    
    private var sections : [Section] = []
    
    private func rowForIndexPath(indexPath : NSIndexPath) -> Row {
        return sections[indexPath.section].rows[indexPath.row]
    }
    
    init(mode : Mode) {
        self.mode = mode
        super.init(style: .Grouped)
        self.mode = mode // Set the mode after the stupid nibNameOrNil constructor overrides it.
    }
    
    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        mode = .Create
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title : String?
        switch mode {
        case .Create:
            sections = sectionsOwner
            title = LocalizedString("CREATE_EVENT")
        case .Edit(let event):
            title = LocalizedString("EDIT_EVENT_SETTINGS")
            eventName = event.name
            anyoneCanPost = event.anyoneCanPost
            sections = event.isOwner(WebAPI.deviceId) ? sectionsOwner : sectionsNonowner
            notificationsEnabled = RecentEvents.sharedInstance.getSubscribedForEventId(event.id)
        }
        
        let cancel = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancel"))
        let save = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: Selector("save"))
        saveButton = save
        updateSaveEnabled()
        navigationItem.leftBarButtonItem = cancel
        navigationItem.rightBarButtonItem = save
        
        navigationItem.title = title
        
        textFieldCell.registerNib(tableView)
        switchCell.registerNib(tableView)
    }

    override func viewDidAppear(animated: Bool) {
        
        var screenName = "Event-Details"
        switch mode {
        case .Create:
            screenName = "Event-Details-New"
        case .Edit( _):
            screenName = "Event-Details-Existing"
        }
        
        // Google Analytics
        Analytics.sharedInstance.screenView(screenName)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    private func switchCellWithLabelKey(labelKey : String, value : Bool, selector : String) -> UITableViewCell {
        let cell = switchCell.dequeueFromTableView(tableView)
        cell.label.text = LocalizedString(labelKey)
        cell.switchView.on = value
        cell.switchView.removeTarget(self, action: nil, forControlEvents: .AllEvents)
        cell.switchView.addTarget(self, action: Selector(selector), forControlEvents: .ValueChanged)
        return cell
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let row = rowForIndexPath(indexPath)
        
        switch(row) {
        case .Name:
            let c = textFieldCell.dequeueFromTableView(tableView)
            c.label.text = LocalizedString("EVENT_NAME")
            c.textField.placeholder = LocalizedString("EVENT_NAME_PLACEHOLDER")
            c.textField.text = eventName
            c.textField.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            c.textField.addTarget(self, action: Selector("eventNameChanged:"), forControlEvents: .EditingChanged)
            c.textField.addTarget(self, action: Selector("eventNameEditingDidEnd:"), forControlEvents: .EditingDidEnd)
            c.textField.addTarget(self, action: Selector("eventNameEditingDidEndOnExit:"), forControlEvents: .EditingDidEndOnExit)
            c.textField.becomeFirstResponder()
            c.accessoryType = .None
            return c
        case .Notifications:
            return switchCellWithLabelKey("NOTIFICATIONS", value: notificationsEnabled, selector: "notificationsSwitchChanged:")
        case .AnyoneCanPost:
            return switchCellWithLabelKey("ANYONE_CAN_POST", value: anyoneCanPost, selector: "anyoneCanPostSwitchChanged:")
        }
    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return sections[section].footerText
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var shouldEndEditing = true
        
        switch rowForIndexPath(indexPath) {
        case .Name:
            makeTextFieldCellFirstResponderAtIndexPath(indexPath)
            shouldEndEditing = false
        case .Notifications:
            break
        case .AnyoneCanPost:
            break
        }
        
        if shouldEndEditing {
            view.endEditing(true)
        }
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    
    // MARK: UIControl on change handlers
    
    func notificationsSwitchChanged(switchControl : UISwitch) {
        notificationsEnabled = switchControl.on
    }
    
    func anyoneCanPostSwitchChanged(switchControl : UISwitch) {
        anyoneCanPost = switchControl.on
    }
    
    func eventNameChanged(sender : UITextField) {
        let trimmedEventName = sender.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        eventName = trimmedEventName
        updateSaveEnabled()
    }
    
    func eventNameEditingDidEnd(sender : UITextField) {
    }
    
    func eventNameEditingDidEndOnExit(sender : UITextField) {
        sender.resignFirstResponder()
    }
    
    func updateSaveEnabled() {
        saveButton?.enabled = (eventName ?? "").characters.count > 0
    }
    
    
    // MARK: Misc
    
    private func makeTextFieldCellFirstResponderAtIndexPath(indexPath : NSIndexPath) {
        // This is a new event, so make event first responder, since it's the only required field.
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? LabelTextFieldTableViewCell {
            cell.textField.becomeFirstResponder()
        }
    }
    
    func save() {
        let event : Event
        let ga_label : String
        let name = eventName ?? ""
        
        switch (mode) {
        case .Create:
            event = Event(id : WebAPIUniqueId(), name: name, owner : WebAPI.deviceId, created: NSDate(), anyoneCanPost: anyoneCanPost)
            ga_label = "new"
        case .Edit(let existingEvent):
            event = Event(id : existingEvent.id, name: name, owner : existingEvent.owner, created: existingEvent.created, anyoneCanPost: anyoneCanPost)
            ga_label = "existing"
        }
        
        if notificationsEnabled {
            AppDelegate.sharedInstance.requestDesiredUserNotificationSettings()
        }
        
        if event.isOwner(WebAPI.deviceId) {
            WebAPI.putEvent(event) { (error) -> Void in
                if let e = error {
                    let titleKey : String
                    switch(self.mode) {
                    case .Create:
                        titleKey = "FAILED_TO_CREATE_EVENT_TITLE"
                    default:
                        titleKey = "FAILED_TO_UPDATE_EVENT_TITLE"
                    }
                    let alert = alertWithTitle(LocalizedString(titleKey), error: e)
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
                
                //Google Analytics
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "add_event_details", label: ga_label)
                self.delegate?.eventDetails(self, didUpdateEvent: event)
            }
        } else {
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "add_event_details", label: ga_label)
            self.delegate?.eventDetails(self, didUpdateEvent: event)
        }
    }
    
    func cancel() {
        delegate?.eventDetailsDidCancel(self)
    }
}
