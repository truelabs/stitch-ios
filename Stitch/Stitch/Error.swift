//
//  Error.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

let StitchErrorDomain = "com.funsocialapps.stitch"

func makeError(key : String, code : Int = 1) -> NSError {
    return makeErrorWithLocalizedDescription(LocalizedString(key), code: code)
}

func makeErrorWithLocalizedDescription(localizedDescription : String, code : Int = 1) -> NSError {
    let userInfo : [NSObject : AnyObject] = [
        NSLocalizedDescriptionKey : localizedDescription,
    ]
    return NSError(domain: StitchErrorDomain, code: code, userInfo: userInfo)
}
