//
//  ActivityFeedViewController.swift
//  Stitch
//
//  Created by John Hwang on 8/21/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Alamofire
import AlamofireImage
import AVKit
import MediaPlayer
import MessageUI
import MobileCoreServices
import Photos
import ReachabilitySwift
import SwiftyJSON
import UIKit
import MWPhotoBrowser
import AssetsLibrary

//Google Analytics
private let ga_action_use_camera = "use_camera"
private let ga_action_add_photos = "add_photos"
private let ga_action_display_atv = "display_on_atv"
private let ga_action_invite_friends = "invite_friends"
private let ga_action_make_movie = "make_movie"
private let ga_action_allow_notifications = "allow_notifications"

class ActivityFeedViewController: UIViewController {
    
    var addButton: UIBarButtonItem!
    
    //
    // Sections
    //
    private enum Section : Int {
        case UploadProgress = 0
        case CallToAction
        case Captures
        
        
        // Always last
        case Count
    }
    
    //
    // Section.UploadProgress
    //
    private let UploadCellConfig = NibCollectionViewCellReuseIdAndType<UploadCell>(reuseId: "Upload")
    private var uploadItems : [UploadQueueItem] = []
    
    //
    // Section.CallToAction
    //
    private let CallToActionCellConfig = NibCollectionViewCellReuseIdAndType<CallToActionCell>(reuseId: "CallToAction")
    private enum CallToAction {
        case AddPhotosVideos, InviteFriends, MakeMovie, AllowNotifications
    }
    // There can only be 1 call to action at a time. callToAction
    // contains the current CallToAction, if any.
    private var callToAction : CallToAction?
    
    private var callToActionState = CallToActionState.Start
    
    //
    // Section.Captures
    //
    private let CaptureCellConfig = NibCollectionViewCellReuseIdAndType<CaptureCell>(reuseId: "Capture")
    
    
    private var collectionView : UICollectionView!
    private var collectionViewLayout : UICollectionViewFlowLayout!
    
    private var event : Event
    var captures : [Capture] = []
    
    private let refreshControl = UIRefreshControl()
    
    private let reachability = Reachability.reachabilityForInternetConnection()!
    private var lastRefreshSucceeded = true
    
    init(event : Event) {
        self.event = event
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        reachability.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create/update the event record before anything else to make sure the record
        // is created.
        RecentEvents.sharedInstance.accessedEvent(event)
        
        uploadItems = AppDelegate.sharedInstance.uploadQueue.uploadItemsForEventId(event.id)
        
        //Set call to action state
        if let state = RecentEvents.sharedInstance.callToActionStateForEventId(event.id) {
            setCallToActionState(state)
        } else {
            handleCallToActionEvent(.Begin)
        }
        
        collectionViewLayout = ActivityFeedFlowLayout()
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: collectionViewLayout)
        collectionView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        
        navigationItem.title = event.name
        addButton = UIBarButtonItem(image: UIImage(named: "menu"), landscapeImagePhone: nil, style: .Plain, target: self, action: Selector("addButtons"))
        navigationItem.rightBarButtonItem = addButton
        
        CallToActionCellConfig.register(collectionView)
        UploadCellConfig.register(collectionView)
        CaptureCellConfig.register(collectionView)
        
        refreshControl.addTarget(self, action: Selector("refreshControlValueChanged:"), forControlEvents: .ValueChanged)
        collectionView?.addSubview(refreshControl)
        collectionView?.alwaysBounceVertical = true
        
        collectionView?.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        observeUploadQueueNotifications()
        observeReachabilityChangedNotification()
        observeEventUpdatedNotification()
        
        let ok = reachability.startNotifier()
        if !ok {
            log.error("Failed to start reachability notifier")
        }
        
        refreshEvent(nil)
        
        // Subscribe to event changes to get push notifications.
        log.debug("Subscribing to event")
        let eventId = event.id
        WebAPI.putEventSubscriber(eventId) { (err) -> Void in
            if let e = err {
                log.error("Error subscribing to event ID \(eventId). \(e)")
            }
        }
    }
    
    var previousToolbarHidden : Bool?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        previousToolbarHidden = navigationController?.toolbarHidden
        //navigationController?.setToolbarHidden(false, animated: true)
        
        // Google Analytics
        Analytics.sharedInstance.screenView("Event-Activity-Feed")
        
        // Invalidate layout on appear, in case the view was covered up during a rotation
        // as is common after video playback.
        collectionViewLayout.invalidateLayout()
        
        // Once the user has seen an event, clear the outstanding local notifications flag.
        RecentEvents.sharedInstance.setTimeOutstandingLocalNotificationPresented(nil, forEventId: event.id)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let previous = previousToolbarHidden {
            navigationController?.setToolbarHidden(previous, animated: true)
            previousToolbarHidden = nil
        }
        
        // Fixes issue #58, which left the UIRefreshControl unanimated after adding photos.
        refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        collectionViewLayout.invalidateLayout()
    }
    
    func refreshControlValueChanged(refreshControl : UIRefreshControl) {
        log.debug("Refresh control initiating refreshEvent")
        refreshEvent { () -> () in
            assert(NSThread.isMainThread())
            log.debug("refreshEvent completed after request from refresh control")
            self.refreshControl.endRefreshing()
        }
    }
    
    private func refreshEventFailed(error : NSError) {
        lastRefreshSucceeded = false
        let title = error.localizedDescription
        showTemporaryBannerWithTitle(title, overView: collectionView)
    }
    
    
    private func refreshEvent(completion : (()->())? = nil) {
        
        log.debug("Refreshing event")
        
        weak var weakSelf = self
        
        WebAPI.getEventById(event.id, completion: { (event, captures, error) -> () in
            
            defer {
                completion?()
            }
            
            assert(NSThread.isMainThread())
            
            log.debug("getEventById returned \(error)")
            
            guard let myself = weakSelf else {
                log.debug("weakSelf is nil")
                return
            }
            
            if let e = error {
                log.error("Error getting event: \(e)")
                myself.refreshEventFailed(e)
                return
            }
            
            myself.lastRefreshSucceeded = true
            
            if let e = event {
                myself.setEvent(e)
            }
            
            if let caps = captures {
                myself.setCaptures(caps)
            }
        })
    }
    
    private func setEvent(event : Event) {
        self.event = event
        RecentEvents.sharedInstance.accessedEvent(event)
        navigationItem.title = event.name
    }
    
    private func setCaptures(newCaptures : [Capture]) {
        captures = newCaptures.sort {$0.time.compare($1.time) == .OrderedDescending}
        handleCallToActionEvent(.CapturesUpdated(captures.count))
        
        if captures.count == 0 {
            let l = UILabel()
            l.font = UIFont.systemFontOfSize(20)
            l.numberOfLines = 0
            l.textAlignment = .Center
            if event.canCurrentUserPost() {
                l.text = LocalizedString("EVENT_EMPTY_MESSAGE_CAN_POST")
            } else {
                l.text = LocalizedString("EVENT_EMPTY_MESSAGE_NOT_POST")
            }
            collectionView?.backgroundView = l
            //Set thumbnail to nil
            RecentEvents.sharedInstance.setThumbnailURL(nil, forEventId: event.id)
        } else {
            collectionView?.backgroundView = nil
            //Set thumbnail URL for Event List page
            let thumbnail = collectionView.bestAssetForPointWidth(50, candidates: captures[0].thumbnails)
            if let t = thumbnail?.URL {
                RecentEvents.sharedInstance.setThumbnailURL(t, forEventId: event.id)
            }
        }
        
        collectionView.reloadSections(NSIndexSet(index: Section.Captures.rawValue))
        
        //
        // Look for items that have been uploaded successfully, but that have not appeared in the feed
        // so that we can remove any UploadCell's that have items that finally make it into the feed.
        //
        let itemsInProcess = uploadItems.filter { $0.state == UploadItemState.Success }
        
        for cap in captures {
            for item in itemsInProcess {
                if cap.contentMD5 == item.contentMd5 && cap.ownerId == WebAPI.deviceId {
                    AppDelegate.sharedInstance.uploadQueue.removeUploadId(item.uploadId)
                }
            }
        }
    }
    
    func addButtons() {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        weak var weakSelf = self

        let addPhotosAction = UIAlertAction(title: LocalizedString("ADD_PHOTOS"), style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
            weakSelf?.pickMedia(self.addButton, gaLabel: "menu_button")
        })
        let takePhotoRecordVideoAction = UIAlertAction(title: LocalizedString("TAKE_PHOTOS"), style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
            weakSelf?.captureMedia(self.addButton, gaLabel: "menu_button")
        })
        let shareEventAction = UIAlertAction(title: LocalizedString("SHARE_EVENT"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            weakSelf?.inviteFriends(self.addButton, gaLabel: "menu_button")
            
        })
        
        let displayATVEventAction = UIAlertAction(title: LocalizedString("DISPLAY_ON_ATV"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            weakSelf?.displayOnATV(self.addButton, gaLabel: "menu_button")
            
        })
        //TODO: add back from movies
        /*
        let makeMovieAction = UIAlertAction(title: LocalizedString("MAKE_MOVIE"), style: .Default, handler: {
        (alert: UIAlertAction!) -> Void in
        log.debug("Making movie...")
        self.makeMovieButtonPressed(self.addButton)
        })
        */
        let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction) -> Void in
            log.debug("Cancelled")
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "bar_button", label: nil)
        })

        if event.canCurrentUserPost(){
            optionMenu.addAction(addPhotosAction)
        }
        
        if event.canCurrentUserPost() && canCapturePhotoOrVideo() {
            optionMenu.addAction(takePhotoRecordVideoAction)
        }

        optionMenu.addAction(shareEventAction)
        optionMenu.addAction(displayATVEventAction)
        //TODO: add back for movies
        //optionMenu.addAction(makeMovieAction)
        optionMenu.addAction(cancelAction)
        
        //Popover location for iPad
        let popOver = optionMenu.popoverPresentationController
        popOver?.barButtonItem = addButton
        
        presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func inviteFriendsButtonPressed(sender : UIBarButtonItem) {
        inviteFriends(sender, gaLabel: "bar_button")
    }
    
    func addPhotosButtonPressed(sender : UIBarButtonItem) {
        pickMedia(sender, gaLabel: "bar_button")
    }
    
    func makeMovieButtonPressed(sender : UIBarButtonItem) {
        makeMovie(sender, gaLabel: "bar_button")
    }
    
    private func saveMediaTask(url : String) {
        
        requestPhotosLibraryAccess() { (hasAccess) in
            assert(NSThread.isMainThread())
            
            if !hasAccess {
                log.debug("Photos Access Disabled")
                let vc = photosDisabledAlert()
                self.presentViewController(vc, animated: true, completion: nil)
                return
            }
        
            weak var weakSelf = self
            
            DownloadToPhotos(url) { (error) -> () in
                
                if let e = error as? NSError {
                    weakSelf?.showAlertTitle(LocalizedString("DOWNLOAD_FAILURE_TITLE"), message: e.localizedDescription)
                } else if let e = error {
                    log.error("Got an error I don't know how to localize: \(e)")
                    weakSelf?.showAlertTitle(LocalizedString("DOWNLOAD_FAILURE_TITLE"))
                } else {
                    weakSelf?.showAlertTitle(LocalizedString("DOWNLOAD_COMPLETE_TITLE"), message: LocalizedString("DOWNLOAD_COMPLETE_MESSAGE"))
                }
            }
        }
    }
}

// MARK: UICollectionViewController overrides
extension ActivityFeedViewController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return Section.Count.rawValue
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch Section(rawValue: section)! {
        case .UploadProgress:
            return uploadItems.count
        case .CallToAction:
            return callToAction == nil ? 0 : 1
        case .Captures:
            return captures.count
            
        case .Count:
            log.severe("Count should never be requested as a section.")
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(1, 0, 5, 0);
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch Section(rawValue: indexPath.section)! {
        case .UploadProgress:
            let item = uploadItems[indexPath.row]
            let cell = UploadCellConfig.dequeueCell(collectionView, indexPath: indexPath)
            
            let labelKey : String
            var progressPercent = Float(0)
            var progressHidden = true
            var buttonImageName = "cancel_filled"
            
            switch item.state {
            case .Waiting:
                labelKey = "UPLOAD_PENDING"
            case .Uploading:
                labelKey = "UPLOAD_UPLOADING"
                if let progress = AppDelegate.sharedInstance.uploadQueue.uploadProgressForUploadId(item.uploadId) {
                    progressPercent = progress.percentage
                    progressHidden = false
                }
            case .Failure:
                labelKey = "UPLOAD_FAILED"
                buttonImageName = "restart_filled"
            case .Success:
                labelKey = "UPLOAD_PROCESSING"
            }
            
            let image : UIImage?
            
            if item.mime.hasPrefix("image/") {
                log.debug("Using size \(cell.imageView.bounds.size)")
                image = UIImage(contentsOfFile: item.file)?.af_imageAspectScaledToFillSize(cell.imageView.bounds.size)
            } else if item.mime.hasPrefix("video/") {
                
                if let uti = UTIFromMime(item.mime), let ext = ExtensionFromUTI(uti), let fileWithExtension = temporaryFilename().URLByAppendingPathExtension(ext).path {
                    
                    do {
                        // Create a hard link to the file with the extension set to something AVFoundation likes,
                        // otherwise AVFoundation will complain that it can't open the file because it doesn't know
                        // how to handle the file type.
                        try NSFileManager.defaultManager().linkItemAtPath(item.file, toPath: fileWithExtension)
                        
                        extractImageFromVideo(fileWithExtension, onComplete: { (image, error) -> () in
                            assert(NSThread.isMainThread())
                            
                            do {
                                try NSFileManager.defaultManager().removeItemAtPath(fileWithExtension)
                            } catch let e {
                                log.error("Error removing hard link \(fileWithExtension). Error: \(e)")
                            }
                            
                            if let e = error {
                                log.error("Error extracting video thumbnail for upload cell. \(e). File: \(item.file).")
                            }
                            
                            if cell.uploadItem?.contentMd5 ==  item.contentMd5 {
                                cell.imageView.image = image
                            }
                        })
                    } catch let e {
                        log.error("Error hard linking \(fileWithExtension) to \(item.file). Error: \(e)")
                    }
                }
                
                image = nil
            } else {
                assert(false, "Unexpected content type \(item.mime)")
                image = nil
            }
            
            cell.label.text = LocalizedString(labelKey)
            cell.imageView.image = image
            cell.progress.setProgress(progressPercent, animated: false)
            cell.progress.hidden = progressHidden
            cell.delegate = self
            cell.uploadItem = item
            cell.button.setImage(UIImage(named: buttonImageName), forState: .Normal)
            
            return cell
            
        case .CallToAction:
            assert(callToAction != nil, "Shouldn't get here unless callToAction != nil")
            let title : String
            let imageName : String
            let ga_action : String
            switch(callToAction!) {
            case .AddPhotosVideos:
                title = "ADD_MEDIA"
                imageName = "plus"
                //Google Analytics
                ga_action = ga_action_add_photos
            case .InviteFriends:
                title = "INVITE_FRIENDS"
                imageName = "plus"
                //Google Analytics
                ga_action = ga_action_invite_friends
            case .MakeMovie:
                title = "MAKE_MOVIE"
                imageName = "plus"
                //Google Analytics
                ga_action = ga_action_make_movie
            case .AllowNotifications:
                title = "ALLOW_NOTIFICATIONS"
                imageName = "Notifications"
                //Google Analytics
                ga_action = ga_action_allow_notifications
            }
            let cell = CallToActionCellConfig.dequeueCell(collectionView, indexPath: indexPath)
            cell.titleLabel.text = LocalizedString(title)
            cell.ga_action = ga_action
            let img = UIImage(named: imageName)?.imageWithRenderingMode(.AlwaysTemplate)
            cell.icon.image = img
            cell.delegate = self
            return cell
            
        case .Captures:
            let cap = captures[indexPath.row]
            
            let cell = CaptureCellConfig.dequeueCell(collectionView, indexPath: indexPath)
            cell.delegate = self
            cell.capture = cap
            
            let thumbnail = collectionView.bestAssetForPointWidth(collectionView.bounds.width, candidates: cap.thumbnails)
            cell.imageView.setImageURL(thumbnail?.URL)
            cell.playButton.hidden = cap.transcodes.count == 0
            
            let sharedByText : String
            if cap.ownerName.isEmpty {
                sharedByText = LocalizedFormatString("SHARED_BY_FORMAT", strings: LocalizedString("ANONYMOUS"))
            } else {
                sharedByText = LocalizedFormatString("SHARED_BY_FORMAT", strings: cap.ownerName)
            }
            cell.label.text = sharedByText
            return cell
            
        case .Count:
            fatalError("ERROR: Invalid section, can't be .Count. IndexPath=\(indexPath)")
        }
    }
    
    
}

extension ActivityFeedViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let height : CGFloat
        switch Section(rawValue: indexPath.section)! {
            
        case .UploadProgress:
            height = UploadCellConfig.nibViewSize.height
            
        case .CallToAction:
            height = CallToActionCellConfig.nibViewSize.height
            
        case .Captures:
            let cap = captures[indexPath.row]
            let b = collectionView.bounds
            if let t = collectionView.bestAssetForPointWidth(b.width, candidates: cap.thumbnails) {
                let tw = CGFloat(t.width)
                let th = CGFloat(t.height)
                let scaleW = b.width / tw
                // Make the height so that the image fills the width and leaves
                // enough room for the CaptureCell chrome (i.e., shared by X field at the top).
                height = th * scaleW + CaptureCell.chromeHeight
            } else {
                log.info("Fallback to capture call nib view size")
                height = CaptureCellConfig.nibViewSize.height
            }
            
        case .Count:
            assert(false, "ERROR: got .Count section for indexPath=\(indexPath)")
            height = 0
        }
        
        return CGSizeMake(collectionView.bounds.width, height)
    }
}

extension ActivityFeedViewController : CallToActionCellDelegate {
    func callToActionActionPressed(cell : CallToActionCell) {
        
        assert(callToAction != nil, "Shouldn't get called if callToAction == nil")
        
        switch callToAction! {
            
        case .AddPhotosVideos:
            pickMedia(cell, gaLabel: "call_to_action")
        case .InviteFriends:
            inviteFriends(cell, gaLabel: "call_to_action")
        case .MakeMovie:
            makeMovie(cell, gaLabel: "call_to_action")
        case .AllowNotifications:
            allowNotifications(cell, gaLabel: "call_to_action")
        }
        
        // It's up to each action type to call:
        // handleCallToActionEvent(.ActionPerformed)
        // when the action is complete.
    }
    
    func callToActionDismissPressed(cell : CallToActionCell) {
        assert(callToAction != nil, "Shouldn't get called if callToAction == nil")
        handleCallToActionEvent(.ActionDismissed)
        
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "dismiss_call_to_action", label: cell.ga_action)
    }
    
    private func setCallToActionState(newState : CallToActionState) {
        
        callToActionState = newState
        RecentEvents.sharedInstance.setCallToActionState(newState, forEventId: event.id)
        
        let newCallToAction : CallToAction?
        switch newState {
        case .ShowingAddPhotos:
            newCallToAction = .AddPhotosVideos
        case .ShowingInviteFriends:
            newCallToAction = .InviteFriends
        case .ShowingAllowNotifications:
            newCallToAction = .AllowNotifications
        case .ShowingMakeMovie:
            newCallToAction = .MakeMovie
            
        case .Start:
            newCallToAction = nil
        case .End:
            newCallToAction = nil
        case .WaitingToShowMakeMovie:
            newCallToAction = nil
        }
        
        callToAction = newCallToAction
        collectionView?.reloadSections(NSIndexSet(index: Section.CallToAction.rawValue))
    }
    
    private func handleCallToActionEvent(event : CallToActionStateMachineEvents) {
        
        let newState : CallToActionState?
        
        switch event {
        case .Begin:
            if self.event.isOwner(DeviceId) {
                newState = .ShowingAddPhotos
            } else {
                newState = .ShowingAllowNotifications
            }
        case .ActionDismissed:
            switch callToActionState {
            case .ShowingAddPhotos:
                newState = .ShowingInviteFriends
            case .ShowingInviteFriends:
                // TODO: add with make movie newState = .ShowingAllowNotifications
                newState = .End
            case .ShowingAllowNotifications:
                if self.event.canCurrentUserPost(){
                    newState = .ShowingAddPhotos
                } else {
                    newState = .ShowingInviteFriends
                }
            case .WaitingToShowMakeMovie:
                newState = .End
            default:
                newState = nil
            }
            
        case .PickMediaActionPerformed:
            switch callToActionState {
            case .ShowingAddPhotos:
                newState = .ShowingInviteFriends
            default:
                newState = nil
            }
            
        case .InviteFriendsActionPerformed:
            switch callToActionState {
            case .ShowingInviteFriends:
                // TODO: Add back for movies
                // newState = .WaitingToShowMakeMovie
                newState = .End
            default:
                newState = nil
            }
            
        case .EnableNotificationsActionPerformed:
            switch callToActionState {
            case .ShowingAllowNotifications:
                if self.event.canCurrentUserPost(){
                    newState = .ShowingAddPhotos
                } else {
                    newState = .ShowingInviteFriends
                }
            default:
                newState = nil
            }
            
        case .MakeMovieActionPerformed:
            switch callToActionState {
            case .ShowingMakeMovie:
                newState = .End
            default:
                newState = nil
            }
            
        case .CapturesUpdated(let n):
            switch callToActionState {
            case .WaitingToShowMakeMovie:
                if n > 5 {
                    newState = .ShowingMakeMovie
                } else {
                    newState = nil
                }
            default:
                newState = nil
            }
            
        }
        
        if let state = newState {
            log.debug("Call To Action Transition State(\(callToActionState))+Event(\(event)) => State(\(state))")
            setCallToActionState(state)
        } else {
            log.debug("Call To Action Transition State(\(callToActionState))+Event(\(event)) => NO CHANGE")
        }
    }
}

extension ActivityFeedViewController : CaptureCellDelegate {
    func captureCellActionPressed(cell: CaptureCell) {
        
        guard let capture = cell.capture else {
            assert(false, "CaptureCell does not have capture .")
            return
        }
        
        let url = WebAPI.CaptureData(event.id, deviceId: capture.ownerId, contentMd5: capture.contentMD5)
        log.debug("Capture URL: \(url)")
        let shareURL = WebAPI.ShareURL(event.id, deviceId: capture.ownerId, contentMd5: capture.contentMD5)
        log.debug("Share URL: \(shareURL)")
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let reportAction = UIAlertAction(title: LocalizedString("REPORT_ABUSE"), style: .Destructive, handler: {
            (alert: UIAlertAction) -> Void in
            let vc = MFMailComposeViewController()
            vc.setSubject(LocalizedString("REPORT_ABUSE"))
            let body = LocalizedString("REPORT_ABUSE_BODY") + "\n\n" + url.URLString
            vc.setMessageBody(body as String, isHTML: false)
            vc.setToRecipients(["abuse@funsocialapps.com"])
            vc.mailComposeDelegate = self
            self.presentViewController(vc, animated: true, completion: nil)
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "report_item", label: nil)
        })
        let shareAction = UIAlertAction(title: LocalizedString("SHARE"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            log.debug("Sharing...")
            //Get short URL
            /*
            WebAPI.getShortURL(shareURL.URLString, completion: { (shortURL, error) -> () in
            
            if error != nil {
            log.debug("Error getting URL: \(error)")
            }
            
            if let sURL = shortURL {
            let newURL = sURL
            log.debug("Short URL: \(newURL)")
            let vc = UIActivityViewController(activityItems: [newURL], applicationActivities: nil)
            
            //Popover location for iPad
            let popOver = vc.popoverPresentationController
            popOver?.sourceView  = cell.button
            popOver?.sourceRect = cell.button.bounds
            
            self.presentViewController(vc, animated: true, completion: nil)
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "share_item", label: nil)
            } else {
            //Alert user error occurred
            let alert = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: LocalizedString("OK"), style: UIAlertActionStyle.Default, handler: nil))
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }
            
            })
            */
            let vc = UIActivityViewController(activityItems: [shareURL.URLString], applicationActivities: nil)
            
            //Popover location for iPad
            let popOver = vc.popoverPresentationController
            popOver?.sourceView  = cell.button
            popOver?.sourceRect = cell.button.bounds
            
            self.presentViewController(vc, animated: true, completion: nil)
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "share_item", label: nil)
            
        })
        let saveAction = UIAlertAction(title: LocalizedString("SAVE_TO_PHOTOS"), style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
            log.debug("Saving...")
            self.saveMediaTask(url.URLString)
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "save_item", label: nil)
        })
        let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction) -> Void in
            log.debug("Cancelled")
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "cancel_item", label: nil)
        })
        let deleteAction = UIAlertAction(title: LocalizedString("DELETE_ITEM"), style: .Destructive, handler: {
            (alert: UIAlertAction) -> Void in
            log.debug("Deleted")
            
            let alert = UIAlertController(title: LocalizedString("CONFIRM_DELETION"), message: LocalizedString("DELETE_ITEM_QUESTION"), preferredStyle: .Alert)
            let dontDeleteAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: { (_) -> Void in
                log.debug("Don't delete pressed.")
            })
            let deleteAction = UIAlertAction(title: LocalizedString("DELETE"), style: .Destructive, handler: { (_) -> Void in
                self.deleteCapture(capture)
                //Google Analytics
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "delete_item", label: nil)
            })
            alert.addAction(dontDeleteAction)
            alert.addAction(deleteAction)
            self.presentViewController(alert, animated: true, completion: nil)
        })
        
        if capture.ownerId != WebAPI.deviceId {
            // can only report other's photos.
            optionMenu.addAction(reportAction)
        }
        optionMenu.addAction(shareAction)
        optionMenu.addAction(saveAction)
        
        if event.amIOwner() || capture.ownerId == WebAPI.deviceId {
            // can only delete things you own or in events you own.
            optionMenu.addAction(deleteAction)
        }
        
        optionMenu.addAction(cancelAction)
        
        //Popover location for iPad
        let popOver = optionMenu.popoverPresentationController
        popOver?.sourceView  = cell.button
        popOver?.sourceRect = cell.button.bounds
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func captureCellPlayPressed(cell: CaptureCell) {
        guard let cap = cell.capture else {
            assert(false, "CaptureCell does not have capture in play pressed.")
            return
        }
        guard let collectionView = self.collectionView else {
            return
        }
        guard let best = collectionView.bestAssetForPointWidth(collectionView.bounds.width, candidates: cap.transcodes) else {
            log.debug("No transcode found.")
            return
        }
        guard let url = NSURL(string:best.URL) else {
            log.error("Couldn't convert transcode URL to NSURL. \(best.URL)")
            return
        }
        log.debug("Using video URL: \(url)")
        let vc = AVPlayerViewController()
        vc.player = AVPlayer(URL: url)
        vc.player?.play()
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func deleteCapture(capture : Capture) {
        weak var weakSelf = self
        WebAPI.deleteCaptureByEventId(event.id, deviceId: capture.ownerId, contentMd5: capture.contentMD5) { (err) -> () in
            assert(NSThread.isMainThread())
            
            log.debug("Delete capture returned \(err)")
            
            guard let strongSelf = weakSelf else {
                return
            }
            
            if let e = err {
                log.error("Error deleting capture. \(e)")
                strongSelf.showAlertTitle(LocalizedString("ERROR_DELETING_CAPTURE"), error: e)
                return
            }
            
            guard let index = strongSelf.captures.indexOf({$0.ownerId == capture.ownerId && $0.contentMD5 == capture.contentMD5}) else {
                log.error("Didn't find capture index needed to remove row from collection view.")
                return
            }
            
            strongSelf.captures.removeAtIndex(index)
            strongSelf.collectionView.deleteItemsAtIndexPaths([NSIndexPath(forRow: index, inSection: Section.Captures.rawValue)])
        }
    }
}

extension ActivityFeedViewController : MPMediaPickerControllerDelegate {
    func pickMusic(sender : UIView) {
        let picker = MPMediaPickerController(mediaTypes: .Music)
        picker.showsCloudItems = false
        picker.delegate = self
        presentViewController(picker, animated: true, completion: nil)
    }
    
    func mediaPicker(mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        weak var weakSelf = self
        weak var weakMediaPicker = mediaPicker
        
        let showError = { (error : NSError?) -> Void in
            weakMediaPicker?.showAlertTitle(LocalizedString("CANNOT_USE_SONG"), error: error)
        }
        
        guard let item = mediaItemCollection.items.first else {
            showError(makeError("NO_SONG_SELECTED"))
            return
        }
        
        // item.assetURL is only to be used with AVFoundation framework.
        let assetURLObj : AnyObject? = item[MPMediaItemPropertyAssetURL]
        guard let assetURL = assetURLObj as? NSURL else {
            log.error("Error getting URL for asset")
            showError(makeError("ERROR_DRM_CONTENT"))
            return
        }
        
        exportAudio(assetURL) { (error : NSError?, audioPath : String?, mime: String?) -> () in
            log.debug("Exported audio: \(error), \(audioPath), \(mime)")
            if error != nil {
                log.error("ERROR exporting audio: \(error)")
                showError(error)
                return
            }
            
            log.debug("TODO: Cleanup exported audio file.")
            
            
            // Share for testing
            //let assetForSharing = AVURLAsset(URL: NSURL(fileURLWithPath: audioPath!), options: nil)
            let assetForSharing : NSData
            do {
                assetForSharing = try NSData(contentsOfFile: audioPath!, options: NSDataReadingOptions.DataReadingMappedIfSafe)
            } catch let e {
                log.debug("Error mapping file: \(e)")
                showError(NSErrorFromErrorType(e))
                return
            }
            
            weakMediaPicker?.dismissViewControllerAnimated(true, completion: nil)
            weakSelf?.sendEmailWithSubject("audio file", message: "Test audio file. Size: \(assetForSharing.length) bytes", attachments: [EmailAttachment(data: assetForSharing, mime: mime!, fileName: "test.m4a")])
        }
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController) {
        log.debug("media picker picked items")
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension ActivityFeedViewController {
    func inviteFriends(sender : AnyObject?, gaLabel: String?) {
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_invite_friends, label: gaLabel)
        let eventActivityItem = EventActivityItem(event: event)
        let vc = UIActivityViewController(activityItems: [eventActivityItem], applicationActivities: nil)
        if let popoverController = vc.popoverPresentationController {
            //Determine if sender is UIBarButtonItem or UIView (from call-to-action card)
            if let b = sender as? UIBarButtonItem {
                popoverController.barButtonItem = b
            } else if let v = sender as? UIView {
                //Popover location for iPad
                popoverController.sourceView = v
                popoverController.sourceRect = v.bounds
            }
        }
        presentViewController(vc, animated: true, completion: nil)
        vc.completionWithItemsHandler = {
            (activityType, completed, returnedItems, activityError) in
            if completed {
                self.handleCallToActionEvent(.InviteFriendsActionPerformed)
            }
        }
    }
    
    func displayOnATV(sender : AnyObject?, gaLabel: String?){
        let vc = PairAppleTVTableViewController()
        vc.eventId = event.id
        let nav = UINavigationController(rootViewController: vc)
        presentViewController(nav, animated: true, completion: nil)
        
        // Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_display_atv, label: gaLabel)
    }
    
    func makeMovie(sender : AnyObject?, gaLabel: String?) {
        //Google Analytics
        //        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_make_movie, label: gaLabel)
        //        let vc = MakeMovieTableViewController()
        //        vc.delegate = self
        //        let nav = UINavigationController(rootViewController: vc)
        //        presentViewController(nav, animated: true, completion: nil)
    }
    
    func allowNotifications(sender : UIView, gaLabel: String?) {
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_allow_notifications, label: gaLabel)
        
        RecentEvents.sharedInstance.setSubscribed(true, forEventId: event.id)
        AppDelegate.sharedInstance.requestDesiredUserNotificationSettings()
        handleCallToActionEvent(.EnableNotificationsActionPerformed)
    }
}

struct EmailAttachment {
    let data : NSData
    let mime : String
    let fileName : String
}

extension ActivityFeedViewController : MFMailComposeViewControllerDelegate {
    func sendEmailWithSubject(subject : String, message : String, attachments : [EmailAttachment]? = nil) {
        
        let vc = MFMailComposeViewController()
        if let attachs = attachments {
            for a in attachs {
                vc.addAttachmentData(a.data, mimeType: a.mime, fileName: a.fileName)
            }
        }
        vc.setMessageBody(message, isHTML: false)
        vc.setSubject(subject)
        vc.mailComposeDelegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        log.debug("Mail compse controller finished: \(result), error: \(error)")
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        log.debug("TODO: Cleanup attachments?")
    }
}

func requestCameraAccess(mediaType: String, completion : (Bool) -> ()) {
    AVCaptureDevice.requestAccessForMediaType(mediaType, completionHandler: { (granted: Bool) -> Void in
        if (granted) {
            dispatch_async(dispatch_get_main_queue()) {
                completion(true)
            }
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                completion(false)
            }
        }
    })
}

// MARK: UIImagePickerControllerDelegate
extension ActivityFeedViewController : UIImagePickerControllerDelegate {
    
    private func canCapturePhotoOrVideo() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(.Camera)
    }
    
    private func captureMedia(sender : AnyObject?, gaLabel: String?) {
        assert(canCapturePhotoOrVideo(), "Can't select capture media")
        
        //Check camera access
        
        switch AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) {
        case .Authorized:
            log.debug("Authorized for camera use")
        case .NotDetermined:
            log.debug("Not determined camera use. Go ahead and try")
        case .Denied, .Restricted:
            log.debug("Camera access disabled. Alerting user.")
            let vc = cameraDisabledAlert()
            presentViewController(vc, animated: true, completion: nil)
            return
        }
        
        switch AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeAudio) {
        case .Authorized:
            log.debug("Authorized for microphone use")
        case .NotDetermined:
            log.debug("Not determined microphone use. Go ahead and try")
        case .Denied, .Restricted:
            log.debug("Microphone access disabled. Alerting user.")
            let vc = microphoneDisabledAlert()
            presentViewController(vc, animated: true, completion: nil)
            return
        }
    
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.sourceType = .Camera
        vc.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(.Camera) ?? []
        vc.videoQuality = .TypeHigh
        vc.videoMaximumDuration = AppDelegate.sharedInstance.videoLimit
        vc.allowsEditing = false
        
        self.presentViewController(vc, animated: true, completion: nil)
        
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_use_camera, label: gaLabel)
    }
    
    private func displayImagePickerCaptureError(debugMessage : String) {
        log.error("Image picker capture error: \(debugMessage)")
        showAlertTitle(LocalizedString("CAPTURE_ERROR"))
    }
    
    private func displayImagePickerCaptureErrorWithError(error : ErrorType, _ debugMessage : String) {
        log.error("Image picker capture error: \(debugMessage). Error: \(error)")
        showAlertTitle(LocalizedString("CAPTURE_ERROR"), error: error)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        weak var weakSelf = self
        
        guard let mediaType = info[UIImagePickerControllerMediaType] as? String else {
            displayImagePickerCaptureError("No media type for picked image.")
            return
        }
        
        handleCallToActionEvent(.PickMediaActionPerformed)
        
        if UTTypeConformsTo(mediaType, kUTTypeImage) {
            
            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
                displayImagePickerCaptureError("No image.")
                return
            }
            
            guard let tmpPath = temporaryFilename().URLByAppendingPathExtension("jpg").path else {
                displayImagePickerCaptureError("Couldn't get tmp path for file.")
                return
            }
            
            UIImageWriteToSavedPhotosAlbum(image, self, Selector("image:didFinishSavingWithError:contextInfo:"), nil);
            
            guard let imageData = UIImageJPEGRepresentation(image, 0.8) else {
                displayImagePickerCaptureError("Couldn't get JPEG representation of image.")
                return
            }
            
            do {
                try imageData.writeToFile(tmpPath, options: [])
            } catch let e {
                displayImagePickerCaptureErrorWithError(e, "Failed to write image data.")
                return
            }
            
            AppDelegate.sharedInstance.uploadQueue.queueFile(tmpPath, uti: kUTTypeJPEG as String, eventId: event.id) { (_, error) -> () in
                if let e = error {
                    weakSelf?.displayImagePickerCaptureErrorWithError(e, "queueFile for image capture failed.")
                    return
                }
            }
            
        } else if UTTypeConformsTo(mediaType, kUTTypeMovie) {
            
            guard let path = (info[UIImagePickerControllerMediaURL] as? NSURL)?.path else {
                displayImagePickerCaptureError("No video path from URL")
                return
            }
            
            // First save the video to the camera roll, then after that succeeds
            // hand it to the upload queue (which moves the file to another location).
            UISaveVideoAtPathToSavedPhotosAlbum(path, self, Selector("video:didFinishSavingWithError:contextInfo:"), nil)
            
            logFileSize(path)
            
        } else {
            displayImagePickerCaptureError("Unexpected media type \(mediaType)")
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        print("User cancelled image picker")
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if let e = error {
            log.error("Error saving image to camera roll. \(e)")
            return
        }
    }
    
    func video(videoPath: String, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if let e = error {
            log.error("Error saving video to camera roll. \(e)")
            return
        }
        
        guard let uti = UTIFromExtension((videoPath as NSString).pathExtension, conformingToUTI: kUTTypeMovie as String) else {
            displayImagePickerCaptureError("Couldn't get UTI from file extension")
            return
        }
        
        weak var weakSelf = self
        AppDelegate.sharedInstance.uploadQueue.queueFile(videoPath, uti: uti, eventId: event.id) { (_, error) -> () in
            if let e = error {
                weakSelf?.displayImagePickerCaptureErrorWithError(e, "queueFile for video capture failed.")
                return
            }
        }
    }
    
}

func requestPhotosLibraryAccess(completion : (Bool) -> ()) {
    PHPhotoLibrary.requestAuthorization() { (PHAuthorizationStatus status) -> Void in
        let hasAccess : Bool
        switch (status)
        {
        case .Authorized:
            hasAccess = true
        case .Denied:
            hasAccess = false
        case .Restricted:
            hasAccess = false
        case .NotDetermined:
            hasAccess = false
        }
        dispatch_async(dispatch_get_main_queue()) {
            completion(hasAccess)
        }
    }
}

// MARK: MWPDelegateDelegate

var mwpDelegate : MWPDelegate?

extension ActivityFeedViewController : MWPDelegateDelegate {
    
    func photoPicker(gaLabel: String?) {
        
        var assets = [PHAsset]()
        
        let screen : UIScreen
        if let windowScreen = self.view.window?.screen {
            screen = windowScreen
        } else {
            log.debug("Couldn't get window's screen. Falling back to main screen.")
            screen = UIScreen.mainScreen()
        }
        
        //Get photos from photos library
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        // TODO: set predicate to only include Image and Videos
        let fetchResults = PHAsset.fetchAssetsWithOptions(options)
        
        fetchResults.enumerateObjectsUsingBlock({ (assetObj, _, _) -> Void in
            assets.append(assetObj as! PHAsset)
        })
        
        let scale = CGFloat(screen.scale)
        // Sizing is very rough... more thought required in a real implementation
        let imageTargetSize = PHImageManagerMaximumSize // CGSizeMake(imageSize * scale, imageSize * scale)
        let targetThumbWidth = scale*(screen.bounds.size.width / 3.0)
        let thumbTargetSize = CGSizeMake(targetThumbWidth, targetThumbWidth)
        
        mwpDelegate = MWPDelegate(photos: assets, photoSize: imageTargetSize, thumbSize: thumbTargetSize)
        mwpDelegate?.delegate = self
        
        let browser = MWPhotoBrowser(delegate: mwpDelegate)
        browser.displayActionButton = false
        browser.displaySelectionButtons = true
        browser.enableGrid = true
        browser.startOnGrid = true
        browser.alwaysShowControls = true
        browser.displayNavArrows = true
        browser.enableSwipeToDismiss = false
        
        let nav = UINavigationController(rootViewController: browser)
        self.presentViewController(nav, animated: true, completion: nil)
        
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: ga_action_add_photos, label: gaLabel)
    }
    
    func pickMedia(sender : AnyObject?, gaLabel: String?) {
        
        requestPhotosLibraryAccess() { (hasAccess) in
            assert(NSThread.isMainThread())
            
            if !hasAccess {
                log.debug("Photos Access Disabled")
                let vc = photosDisabledAlert()
                self.presentViewController(vc, animated: true, completion: nil)
                return
            }

            if DeviceUsername == nil || DeviceUsername == "" {
                let vc = SingleTextFieldController()
                let nav = UINavigationController(rootViewController: vc)
                vc.textFieldPlaceholder = LocalizedString("USERNAME_SETUP")
                vc.navigationItem.title = LocalizedString("USERNAME")
                vc.footerText = LocalizedString("USERNAME_DESC_SETUP")
                vc.newSetup = true
                vc.gaScreen = "Username-Setup-New"
                vc.textChangedCallback = { (newValue) -> () in
                    let username = newValue ?? ""
                    DeviceUsername = username
                    WebAPI.registerDeviceWithRetries()
                    nav.dismissViewControllerAnimated(false, completion: nil)
                    
                    if DeviceUsername != nil && DeviceUsername != "" {
                        self.photoPicker(gaLabel)
                    }
                }
                self.presentViewController(nav, animated: true, completion: nil)
            } else {
                self.photoPicker(gaLabel)
            }
        }
    }
    
    func delegateDelegate(del : MWPDelegate, didSelectItems selectedItems: Set<UInt>) {
        log.debug("Selected \(selectedItems.count) items")
        
        //Google Analytics
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "item_selected", label: nil, value: selectedItems.count)
        
        let assets = selectedItems.map { del.assets[Int($0)] }.sort {
            (a1, a2) -> Bool in
            guard let d2 = a2.creationDate else {
                // treat nil as old
                return true
            }
            guard let d1 = a1.creationDate else {
                return false
            }
            return d1.earlierDate(d2) == d1
        }
        
        for asset in assets {
            weak var weakSelf = self
            
            let genericCaptureError = { (debugMessage : String) -> Void in
                log.debug("Generic Capture Error: \(debugMessage)")
                weakSelf?.showAlertTitle(LocalizedString("CAPTURE_ERROR"))
            }
            
            let captureError = { (error : ErrorType, debugMessage : String) -> Void in
                log.debug("Capture Error: \(debugMessage). Error: \(error)")
                weakSelf?.showAlertTitle(LocalizedString("CAPTURE_ERROR"), error: error)
            }

            let mediaType = asset.mediaType
            
            handleCallToActionEvent(.PickMediaActionPerformed)
            
            if mediaType == .Image {
                
                asset.requestContentEditingInputWithOptions(PHContentEditingInputRequestOptions()) { (input, _) in
                    guard let pathExtension = input?.fullSizeImageURL?.pathExtension else {
                        genericCaptureError("No reference URL for image")
                        return
                    }
                    
                    PHImageManager.defaultManager().requestImageDataForAsset(asset, options: nil) {
                        imageData,dataUTI,orientation,info in
                        
                        guard let uti = UTIFromExtension(pathExtension, conformingToUTI: dataUTI) else {
                            genericCaptureError("Couldn't get UTI from file extension")
                            return
                        }
                        
                        guard let img = imageData else {
                            genericCaptureError("Image isn't an NSData \(imageData).")
                            return
                        }
                        
                        guard let image = UIImage(data: img) else {
                            genericCaptureError("Couldn't get original image in picker result.")
                            return
                        }
                        
                        let data : NSData
                        if UTTypeConformsTo(uti, kUTTypeJPEG) {
                            log.debug("Using JPEG representation")
                            guard let d = UIImageJPEGRepresentation(image, 0.8) else {
                                genericCaptureError("Couldn't get JPEG representation.")
                                return
                            }
                            data = d
                        } else {
                            log.debug("Using PNG representation")
                            guard let d = UIImagePNGRepresentation(image) else {
                                genericCaptureError("Couldn't get PNG representation.")
                                return
                            }
                            data = d
                        }
                        
                        let file = temporaryFilename()
                        guard let filePath = file.path else {
                            genericCaptureError("Couldn't get path from file URL.")
                            return
                        }
                        
                        do {
                            try data.writeToURL(file, options: [])
                        } catch let e {
                            captureError(e, "Failed to write image to temporary file.")
                            return
                        }
                        
                        AppDelegate.sharedInstance.uploadQueue.queueFile(filePath, uti: uti, eventId: self.event.id) { (_, error) -> () in
                            if let e = error {
                                captureError(e, "queueFile for image failed")
                            }
                        }
                        
                        //Google Analytics
                        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "item_uploaded", label: "photo")
                    }
                }
                
            } else if mediaType == .Video {
                
                asset.requestContentEditingInputWithOptions(PHContentEditingInputRequestOptions()) { (input, _) in
                    
                    let options = PHVideoRequestOptions()
                    options.networkAccessAllowed = true
                    let preset = AVAssetExportPresetPassthrough
                    PHImageManager.defaultManager().requestExportSessionForVideo(asset, options: options, exportPreset: preset) { (exportSession, info) -> Void in
                        
                        guard let session = exportSession else {
                            genericCaptureError("Session isn't an AVAssetExportSession \(exportSession).")
                            return
                        }
                        print("Got video export session")
                        transcodeFromExportSession(session, outputUTI: AVFileTypeMPEG4, onComplete: { (error, path, mime) -> () in
                            print("Video export complete")
                            if error != nil {
                                genericCaptureError("Failed to export video. \(error)")
                                return
                            }
                            
                            guard let p = path else {
                                genericCaptureError("Path is not valid \(path).")
                                return
                            }
                            dispatch_async(dispatch_get_main_queue()) {
                                logFileSize(p)
                                AppDelegate.sharedInstance.uploadQueue.queueFile(p, uti: AVFileTypeMPEG4, eventId: self.event.id) { (_, error) -> () in
                                    if let e = error {
                                        captureError(e, "queueFile for video failed")
                                    }
                                }
                            }
                            
                            //Google Analytics
                            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "item_uploaded", label: "video")
                        })
                        
                    }
                
                }
                
            } else {
                genericCaptureError("Unexpected media type \(mediaType)")
            }
        }

    }
}

extension ActivityFeedViewController : UINavigationControllerDelegate {
    //Do nothing for now
}

private enum CallToActionStateMachineEvents {
    case Begin
    case ActionDismissed
    case PickMediaActionPerformed
    case InviteFriendsActionPerformed
    case MakeMovieActionPerformed
    case EnableNotificationsActionPerformed
    case CapturesUpdated(Int)
}

enum CallToActionState {
    case Start
    case ShowingAddPhotos
    case ShowingInviteFriends
    case ShowingAllowNotifications
    case WaitingToShowMakeMovie
    case ShowingMakeMovie
    case End
}

//extension ActivityFeedViewController : MakeMovieTableViewControllerDelegate {
//
//    func makeMovieDidComplete(controller : MakeMovieTableViewController) {
//        log.debug("Make Movie")
//        handleCallToActionEvent(.MakeMovieActionPerformed)
//        controller.dismissViewControllerAnimated(true, completion: nil)
//    }
//
//    func makeMovieDidCancel(controller: MakeMovieTableViewController) {
//        controller.dismissViewControllerAnimated(true, completion: nil)
//    }
//}

extension ActivityFeedViewController : EventDetailsTableViewControllerDelegate {
    func eventDetails(controller: EventDetailsTableViewController, didUpdateEvent event : Event) {
        setEvent(event)
        RecentEvents.sharedInstance.setSubscribed(controller.notificationsEnabled, forEventId: event.id)
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func eventDetailsDidCancel(controller: EventDetailsTableViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension ActivityFeedViewController : UploadCellDelegate {
    func uploadCellButtonPressed(cell: UploadCell) {
        guard let item = cell.uploadItem else {
            assert(false, "UploadCell doesn't have uploadItem")
            return
        }
        
        switch item.state {
        case .Failure:
            AppDelegate.sharedInstance.uploadQueue.retryFailedWithUploadId(item.uploadId)
        default:
            AppDelegate.sharedInstance.uploadQueue.removeUploadId(item.uploadId)
        }
    }
}

// MARK: UploadQueue Notifications
extension ActivityFeedViewController {
    
    func observeUploadQueueNotifications() {
        let nc = NSNotificationCenter.defaultCenter()
        
        nc.removeObserver(self, name: UploadQueue.ProgressNotificationName, object: nil)
        nc.addObserver(self, selector: Selector("uploadQueueProgressNotification:"), name: UploadQueue.ProgressNotificationName, object: AppDelegate.sharedInstance.uploadQueue)
        
        nc.removeObserver(self, name: UploadQueue.ItemAddedNotificationName, object: nil)
        nc.addObserver(self, selector: Selector("uploadQueueItemAddedNotification:"), name: UploadQueue.ItemAddedNotificationName, object: AppDelegate.sharedInstance.uploadQueue)
        
        nc.removeObserver(self, name: UploadQueue.ItemRemovedNotificationName, object: nil)
        nc.addObserver(self, selector: Selector("uploadQueueItemRemovedNotification:"), name: UploadQueue.ItemRemovedNotificationName, object: AppDelegate.sharedInstance.uploadQueue)
        
        nc.removeObserver(self, name: UploadQueue.ItemStateChangedNotificationName, object: nil)
        nc.addObserver(self, selector: Selector("uploadQueueItemStateChangeNotification:"), name: UploadQueue.ItemStateChangedNotificationName, object: AppDelegate.sharedInstance.uploadQueue)
    }
    
    func uploadQueueProgressNotification(notification : NSNotification) {
        assert(NSThread.isMainThread())
        
        guard let progress = (notification.userInfo?[UploadQueue.ProgressNotificationInfoKey] as? UploadProgressWrapper)?.uploadProgress else {
            log.debug("Didn't get expected UploadQueueProgressNotificationInfo")
            return
        }
        
        guard let row = uploadItems.indexOf({$0.uploadId == progress.uploadId}) else {
            // This can happen if an upload for another event is progressing.
            return
        }
        
        let indexPath = NSIndexPath(forRow: row, inSection: Section.UploadProgress.rawValue)
        
        guard let cell = collectionView.cellForItemAtIndexPath(indexPath) as? UploadCell else {
            // This happens if the cell isn't visible.
            return
        }
        
        cell.progress.setProgress(progress.percentage, animated: true)
    }
    
    func uploadQueueItemAddedNotification(notification : NSNotification) {
        assert(NSThread.isMainThread())
        
        guard let item = (notification.userInfo?[UploadQueue.ItemKey] as? UploadQueueItemWrapper)?.uploadQueueItem else {
            assert(false, "Didn't get UploadQueueItem in item added")
            return
        }
        
        guard item.eventId == event.id else {
            // upload not for this event, ignore.
            return
        }
        
        let beforeCount = uploadItems.count
        uploadItems.insert(item, atIndex: 0)
        assert(beforeCount + 1 == uploadItems.count)
        let indexPath = NSIndexPath(forRow: 0, inSection: Section.UploadProgress.rawValue)
        collectionView.insertItemsAtIndexPaths([indexPath])
    }
    
    func uploadQueueItemRemovedNotification(notification : NSNotification) {
        assert(NSThread.isMainThread())
        
        guard let item = (notification.userInfo?[UploadQueue.ItemKey] as? UploadQueueItemWrapper)?.uploadQueueItem else {
            assert(false, "Didn't get UploadQueueItem in item removed")
            return
        }
        
        guard item.eventId == event.id else {
            // upload not for this event, ignore.
            return
        }
        
        guard let index = uploadItems.indexOf({ $0.uploadId == item.uploadId }) else {
            // not tracking this upload
            return
        }
        
        let beforeCount = uploadItems.count
        uploadItems.removeAtIndex(index)
        assert(beforeCount - 1 == uploadItems.count)
        let indexPath = NSIndexPath(forRow: index, inSection: Section.UploadProgress.rawValue)
        collectionView.deleteItemsAtIndexPaths([indexPath])
    }
    
    func uploadQueueItemStateChangeNotification(notification : NSNotification) {
        assert(NSThread.isMainThread())
        
        guard let item = (notification.userInfo?[UploadQueue.ItemKey] as? UploadQueueItemWrapper)?.uploadQueueItem else {
            assert(false, "Didn't get UploadQueueItem in item state changed")
            return
        }
        
        guard item.eventId == event.id else {
            // upload not for this event, ignore.
            return
        }
        
        guard let index = uploadItems.indexOf({ $0.uploadId == item.uploadId }) else {
            // not tracking this upload
            return
        }
        
        uploadItems[index] = item
        
        collectionView.reloadItemsAtIndexPaths([NSIndexPath(forRow: index, inSection: Section.UploadProgress.rawValue)])
    }
}

extension ActivityFeedViewController {
    
    func observeReachabilityChangedNotification() {
        let nc = NSNotificationCenter.defaultCenter()
        let name = ReachabilityChangedNotification
        nc.removeObserver(self, name: name, object: nil)
        nc.addObserver(self, selector: "reachabilityChanged:", name: name, object: reachability)
    }
    
    func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable() {
            log.debug("Is reachable.")
            if !lastRefreshSucceeded {
                // Only try to refresh the event if the last refresh failed, and the network is now reachable.
                refreshEvent()
            }
        } else {
            log.debug("Not reachable.")
        }
    }
}

extension ActivityFeedViewController {
    func observeEventUpdatedNotification() {
        let nc = NSNotificationCenter.defaultCenter()
        let name = EventUpdatedNotificationName
        nc.removeObserver(self, name: name, object: nil)
        nc.addObserver(self, selector: "eventUpdatedNotification:", name: name, object: nil)
    }
    
    func eventUpdatedNotification(notification : NSNotification) {
        guard let eventId = notification.userInfo?[EventUpdatedNotificationEventIDKey] as? String else {
            log.error("No event id for event updated notification. \(notification)")
            return
        }
        
        if eventId == event.id {
            log.debug("Refreshing event in response to event updated notification")
            refreshEvent()
        }
    }
}
