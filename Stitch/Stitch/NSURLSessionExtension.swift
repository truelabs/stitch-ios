//
//  NSURLSessionExtension.swift
//  Stitch
//
//  Created by Douglas Richardson on 7/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

extension NSURLSession {
    func jsonDataTaskWithURL(url: NSURL, completionHandler: ((JSON?, NSURLResponse?, NSError?) -> Void)) -> NSURLSessionDataTask {
        return dataTaskWithURL(url, completionHandler: { (data, res, error) -> Void in
            if data == nil || error != nil {
                log.error("jsonDataTaskWithURL: data == nil or error != nil (\(error))")
                completionHandler(nil, res, error)
                return
            }
            
            var e : NSError?
            let json = JSON(data: data!, options: NSJSONReadingOptions(), error: &e)
            completionHandler(json, res, e)
        })
    }
}
