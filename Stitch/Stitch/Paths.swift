//
//  Paths.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/16/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SwiftyUUID

func applicationSupportDirectory() -> NSURL {
    return directoryURL(.ApplicationSupportDirectory)
}

func cacheDirectory() -> NSURL {
    return directoryURL(.CachesDirectory)
}

func temporaryFilename() -> NSURL {
    let path = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent(UUID().CanonicalString())
    return NSURL(fileURLWithPath: path)
}

private func directoryURL(directory : NSSearchPathDirectory) -> NSURL {
    let fm = NSFileManager.defaultManager()
    let urls = fm.URLsForDirectory(directory, inDomains: .UserDomainMask)
    guard let url = urls.first else {
        fatalError("Couldn't get url for \(directory)")
    }
    guard let bundleId = NSBundle.mainBundle().bundleIdentifier else {
        fatalError("Couldn't get bundle ID for \(directory)")
    }
    do {
        let result = url.URLByAppendingPathComponent(bundleId)
        try fm.createDirectoryAtURL(result, withIntermediateDirectories: true, attributes: nil)
        return result
    } catch let e as NSError {
        fatalError("Error creating \(directory) directory. \(e)")
    }
}

func logFileSize(path : String) {
    var error : NSError?
    let attrs: [NSObject: AnyObject]?
    do {
        attrs = try NSFileManager.defaultManager().attributesOfItemAtPath(path)
    } catch let error1 as NSError {
        error = error1
        attrs = nil
    }
    if let fileSize = attrs?[NSFileSize] as? Int {
        let fileSizeKB = Double(fileSize) / 1024.0
        log.debug("File Size: \(fileSizeKB)KB, Path: \(path)")
    } else {
        log.error("Couldn't get file size of \(path). Error: \(error)")
    }
}