//
//  LabelDetailTableViewCell.swift
//  Stitch
//
//  Created by Doug Richardson on 10/3/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelDetailTableViewCell: UITableViewCell, NibTableViewCell {
    @IBOutlet var label : UILabel!
    @IBOutlet var detailLabel : UILabel!
    
    static func nibName() -> String {
        return "LabelDetailTableViewCell"
    }
}
