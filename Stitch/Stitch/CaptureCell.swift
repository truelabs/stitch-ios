//
//  CaptureCell.swift
//  Stitch
//
//  Created by Doug Richardson on 9/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol CaptureCellDelegate : class {
    func captureCellActionPressed(cell : CaptureCell)
    func captureCellPlayPressed(cell : CaptureCell)
}

class CaptureCell: UICollectionViewCell {
    
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var button : UIButton!
    @IBOutlet var playButton : UIButton!
    var capture : Capture?
    
    weak var delegate : CaptureCellDelegate?
    
    @IBAction func actionPressed() {
        delegate?.captureCellActionPressed(self)
    }
    
    @IBAction func playPressed() {
        delegate?.captureCellPlayPressed(self)
    }
    
    override func prepareForReuse() {
        imageView.setImageURL(nil)
    }
    
    static let chromeHeight : CGFloat = {
        let nib = UINib(nibName: CaptureCell.nibName(), bundle: nil)
        let topLevelObjects = nib.instantiateWithOwner(nil, options: nil)
        assert(topLevelObjects.count == 1, "nibs contain 1 top level UICollectionViewCell object")
        let cell = topLevelObjects.first as! CaptureCell
        return cell.bounds.size.height - cell.imageView.bounds.size.height
    }()
}

extension CaptureCell : NibCollectionViewCell {
    static func nibName() -> String {
        return "CaptureCell"
    }
}

