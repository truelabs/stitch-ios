//
//  Alerts.swift
//  Stitch
//
//  Created by Douglas Richardson on 6/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

func alertWithTitle(title : String, error : NSError?) -> UIAlertController {
    let vc = UIAlertController(title: title, message: error?.localizedDescription, preferredStyle: .Alert)
    
    let ok = UIAlertAction(title: LocalizedString("OK"), style: .Default) { (_) -> Void in
        vc.dismissViewControllerAnimated(true, completion: nil)
    }
    vc.addAction(ok)
    
    return vc
}

func alertWithTitle(title : String, error : ErrorType?) -> UIAlertController {
    return alertWithTitle(title, error: NSErrorFromErrorType(error))
}

func openSettingsAlertWithTitle(title : String, message : String, completion : (() -> ())?) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    // If we can get a URL to open settings, use that to give the user a convenient
    // option for getting to settings.
    if let settingsURL = NSURL(string:UIApplicationOpenSettingsURLString) {
        let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel) { (_) in
            completion?()
        }
        let openSettingsAction = UIAlertAction(title: LocalizedString("OPEN_SETTINGS"), style: .Default) {
            (_) -> Void in
            UIApplication.sharedApplication().openURL(settingsURL)
            completion?()
        }
        alert.addAction(openSettingsAction)
        alert.addAction(cancelAction)
    } else {
        // settings URL not available, so just show an OK button.
        let okAction = UIAlertAction(title: LocalizedString("OK"), style: .Default) { (_) in
            completion?()
        }
        alert.addAction(okAction)
    }
    return alert
}

func notificationsDisabledAlert(completion: (()->())?) -> UIAlertController {
    let title = LocalizedString("NOTIFICATIONS_DISABLED_TITLE")
    let message = LocalizedString("NOTIFICATIONS_DISABLED_MESSAGE")
    return openSettingsAlertWithTitle(title, message: message, completion: completion)
}

func photosDisabledAlert() -> UIAlertController {
    let title = LocalizedString("PHOTOS_DISABLED_TITLE")
    let message = LocalizedString("PHOTOS_DISABLED_MESSAGE")
    return openSettingsAlertWithTitle(title, message: message, completion: nil)
}

func cameraDisabledAlert() -> UIAlertController {
    let title = LocalizedString("CAMERA_DISABLED_TITLE")
    let message = LocalizedString("CAMERA_DISABLED_MESSAGE")
    return openSettingsAlertWithTitle(title, message: message, completion: nil)
}

func microphoneDisabledAlert() -> UIAlertController {
    let title = LocalizedString("MICROPHONE_DISABLED_TITLE")
    let message = LocalizedString("MICROPHONE_DISABLED_MESSAGE")
    return openSettingsAlertWithTitle(title, message: message, completion: nil)
}