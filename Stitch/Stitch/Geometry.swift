//
//  Geometry.swift
//  Stitch
//
//  Created by Douglas Richardson on 4/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import CoreGraphics

extension CGRect {
    var centerPoint : CGPoint {
        get {
            return CGPointMake(CGRectGetMidX(self), CGRectGetMidY(self))
        }
    }
}
