//
//  AppDelegate.swift
//  Stitch
//
//  Created by Douglas Richardson on 4/29/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import AlamofireImage
import Branch
import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var videoLimit : Double = 60
    private var pushNotificationManager : PushNotificationManager!
    
    static var sharedInstance : AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        log.debug("did finish launching")
        
        ConfigureGoogleAPI()
        
        BranchInstance().initSessionWithLaunchOptions(launchOptions) { (params, err) -> Void in
            if params != nil {
                if let eventId = params["event-id"] as? String {
                    self.loadActivityFeedForEventId(eventId)
                } else {
                    log.debug("Unhandled branch link. params: \(params), err: \(err)")
                }
            }
        }
        
        pushNotificationManager = PushNotificationManager()
        pushNotificationManager.delegate = self
        application.registerForRemoteNotifications()
        
        if let url = launchOptions?[UIApplicationLaunchOptionsURLKey] as? NSURL {
            log.debug("Got URL: \(url)")
        }
        
        setupNewNav()
        checkVideoMaxDuration()
        
        WebAPI.registerDeviceWithRetries()
        
        uploadQueue.start()
        
        if let localNotification = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as? UILocalNotification {
            log.debug("Started because of local notification. \(localNotification)")
            if let eventId = localNotification.userInfo?[EventUpdatedNotificationEventIDKey] as? String {
                loadActivityFeedForEventId(eventId)
            } else {
                log.error("Didn't understand local notification. \(localNotification)")
            }
        }
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        log.debug("Open URL: \(url), Source App: \(sourceApplication)")
        
        if BranchInstance().handleDeepLink(url) {
            log.debug("URl handled by branch. \(url)")
            return true
        }
        
        let urlString = url.absoluteString
        var components = urlString.characters.split { $0 == "/" }.map { String($0) }
        
        guard let scheme = components.first else {
            log.debug("Missing scheme")
            return false
        }
        
        log.debug("scheme is \(scheme)")
        
        components.removeAtIndex(0)
            
        guard let command = components.first else {
            log.error("Missing command")
            return false
        }
        
        guard command == "event" else {
            log.error("Unknown command \(command)")
            return false
        }
        
        components.removeAtIndex(0)
        
        guard let eventId = components.first else {
            log.error("Missing event id string")
            return false
        }
        
        loadActivityFeedForEventId(eventId)
        
        return true
    }
    
    func queryParamsFromURL(url : NSURL) -> [String:String] {
        var result = [String : String]()
        
        guard let query = url.query else {
            return result
        }
        
        let keyEqualValue = query.characters.split { $0 == "&" } // [ k=v ]
        
        for kv in keyEqualValue {
            let pair = kv.split("=")

            if (pair.endIndex - pair.startIndex) == 2 {
                if let first = pair.first, let second = pair.last {
                    result[String(first)] = String(second)
                }
            }
        }
        
        return result
    }
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let webpageURL = userActivity.webpageURL {

                guard let path = webpageURL.path else {
                    log.debug("No Path")
                    return false
                }
                
                if path != "/" {
                    log.debug("Don't know how to handle path \(path)")
                    return false
                }
                
                let params = queryParamsFromURL(webpageURL)
                
                guard let eventId = params["event"] else {
                    log.error("Missing event id string")
                    return false
                }
                print("eventID:\(eventId)")
                loadActivityFeedForEventId(eventId)
                return true
            }
        }
        return false
    }
    
    private func loadActivityFeedForEventId(eventId : String) {
        log.debug("Loading activity feed for event \(eventId)")
        
        WebAPI.getEventById(eventId) { (event, captures, error) -> () in
            if error != nil {
                log.error("Error getting event: \(error)")
            }
            
            guard let e = event else {
                log.error("No event")
                return
            }
            
            let vc = ActivityFeedViewController(event: e)
            if let caps = captures {
                vc.captures = caps
            }
            let rootVC = self.setupNewNav()
            rootVC.pushViewController(vc, animated: false)
        }
    }
    
    private func setupNewNav() -> UINavigationController {
        let eventListController = EventListTableViewController(style: .Grouped)
        let nav = UINavigationController(rootViewController: eventListController)
        
        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window.rootViewController = nav
        window.addSubview(nav.view)
        window.makeKeyAndVisible()
        
        self.window = window
        
        return nav
    }
    
    func checkVideoMaxDuration() {
        ClientConfig.sharedInstance.videoTimeLimit { (time) -> () in
            self.videoLimit = time
        }
    }
    
    private var uploadQueueWifiOnlyHandleEventsCompletionHandler : (() -> Void)?
    private var uploadQueueCellAndWifiHandleEventsCompletionHandler : (() -> Void)?
    
    private let sharedInstanceBackgroundIdWifiOnly = "com.funsocialapps.stitch.upload-queue.wifi-only"
    private let sharedInstanceBackgroundIdCellAndWifi = "com.funsocialapps.stitch.upload-queue.cell-and-wifi"
    
    lazy var uploadQueue : UploadQueue! = {
        let path = applicationSupportDirectory().URLByAppendingPathComponent("uploadQueue.db").absoluteString
        return UploadQueue(dbpath : path, wifiOnlyBackgroundSessionId : self.sharedInstanceBackgroundIdWifiOnly, cellAndWifiBackgroundSessionId : self.sharedInstanceBackgroundIdCellAndWifi, finishEventsHandlerProvider : self)
    }()
    
    func application(application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: () -> Void) {
        log.debug("application:handleEventsForBackgroundURLSession:identifier:completionHandler: \(identifier)")
        if identifier == sharedInstanceBackgroundIdWifiOnly {
            uploadQueueWifiOnlyHandleEventsCompletionHandler = completionHandler
        } else if identifier == sharedInstanceBackgroundIdCellAndWifi {
            uploadQueueCellAndWifiHandleEventsCompletionHandler = completionHandler
        } else {
            log.error("ERROR: unhandled background session ID: \(identifier)")
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        log.debug("will resign active")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        log.debug("entering background")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        log.debug("entering foreground")
        
        // Workaround for: https://github.com/Alamofire/Alamofire/issues/872
        WebAPI = WebAPISessionFromUsersEnvironment()
        
        // Workaround for: https://github.com/Alamofire/AlamofireImage/issues/38
        UIImageView.af_sharedImageDownloader = ImageDownloader()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        log.debug("did become active")
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        log.debug("will terminate")
    }
    
    func applicationSignificantTimeChange(application: UIApplication) {
        log.debug("significant time change")
    }
}

extension AppDelegate : UploadQueueFinishSessionEventsCompletionHandlerProvider {
    func uploadQueueWifiOnlyBackgroundSessionCompletionHandler() -> (() -> Void)? {
        return uploadQueueWifiOnlyHandleEventsCompletionHandler
    }
    
    func uploadQueueCellAndWifiBackgroundSessionCompletionHandler() -> (() -> Void)? {
        return uploadQueueCellAndWifiHandleEventsCompletionHandler
    }
}

let EventUpdatedNotificationName = "EventUpdated"
let EventUpdatedNotificationEventIDKey = "EventID"

extension AppDelegate {
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        log.debug("device token \(deviceToken)")
        pushNotificationManager.registerAPNSDeviceToken(deviceToken)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        log.error("Failed to register for remote notifications. \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        log.debug("received remote notification: \(userInfo)")
        
        guard let op = userInfo["op"] as? String else {
            log.error("op is not a string")
            completionHandler(.Failed)
            return
        }
        
        if op == "event_updated" {
            
            guard let eventId = userInfo["event_id"] as? String else {
                log.error("event_id is not a string \(userInfo)")
                completionHandler(.Failed)
                return
            }
            
            guard let deviceId = userInfo["device_id"] as? String else {
                log.error("device_id is not a string \(userInfo)")
                completionHandler(.Failed)
                return
            }
            
            let userInfo : [NSObject : AnyObject] = [EventUpdatedNotificationEventIDKey : eventId]
            let note = NSNotification(name: EventUpdatedNotificationName, object: self, userInfo: userInfo)
            NSNotificationCenter.defaultCenter().postNotification(note)
            
            
            if shouldPostEventChangedLocalNotification(eventId, deviceId: deviceId) {
                postEventChangedLocalNotification(eventId, userInfo : userInfo)
            }
            
            completionHandler(.NewData)
            return
            
        } else {
            log.warning("unhandled op \(op)")
            completionHandler(.NoData)
            return
        }
    }
}

extension AppDelegate : PushNotificationManagerDelegate {
    func pushNotificationManager(manager: PushNotificationManager, tokenDidUpdate token: String) {
        log.debug("Push notification token updated: \(token)")
        DeviceToken = token
        WebAPI.registerDeviceWithRetries()
    }
    
    func pushNotificationManager(manager: PushNotificationManager, tokenUpdateError error : NSError) {
        log.error("Error updating push notification token. \(error)")
    }
}

extension AppDelegate {
    // Requests user notifications settings for the app. Response returned in
    // ApplicationDidRegisterUserNotificationSettingsNotification.
    func requestDesiredUserNotificationSettings() {
        let settings = UIUserNotificationSettings(forTypes: [.Badge, .Alert, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if !notificationSettings.types.contains(.Alert) {
            log.debug("User requested notifications but app is not allowed to display them.")
            
            let window : UIWindow! = UIWindow(frame: self.window!.frame)
            window.makeKeyAndVisible()
            let rootVC = UIViewController()
            window.rootViewController = rootVC
            window.windowLevel = UIWindowLevelAlert + 1
            
            let alert = notificationsDisabledAlert() {
                window.removeFromSuperview()
            }
            
            rootVC.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func shouldPostEventChangedLocalNotification(eventId : String, deviceId : String) -> Bool {
        
        if deviceId == DeviceId {
            // Don't post local notifications if the change was caused by this device.
            log.debug("push notification due to activity from this device, ignore.")
            return false
        }
        
        if !RecentEvents.sharedInstance.getSubscribedForEventId(eventId) {
            // User not interested in notifications for this event.
            log.debug("push notification activity on event user is not interested in, ignore.")
            return false
        }
        
        if let outstandingNotificationPresentedAt = RecentEvents.sharedInstance.timeOutstandingLocalNotificationPresentedForEventId(eventId) {
            log.debug("outstanding notification scheduled at \(outstandingNotificationPresentedAt)")
            let expirationSeconds = 86400.0 // 1 day
            let outstandingExpiration = outstandingNotificationPresentedAt.dateByAddingTimeInterval(expirationSeconds)
            let now = NSDate()
            if now.laterDate(outstandingExpiration) == outstandingExpiration {
                // Local notification for event is outstanding. Don't spam user with another one.
                log.debug("already have outstanding local notification that has not expired, ignore.")
                return false
            } else {
                log.debug("outstanding local notification has expired. OK to post another one.")
            }
        }
        
        log.debug("should post a local notification")
        return true
    }
    
    func postEventChangedLocalNotification(eventId : String, userInfo : [NSObject : AnyObject]) {
        let eventName = RecentEvents.sharedInstance.nameForEventId(eventId)
        
        let body : String
        if let name = eventName {
            body = LocalizedFormatString("EVENT_UPDATED_FORMAT", strings: name)
        } else {
            body = LocalizedString("EVENT_UPDATED_NO_TITLE_FALLBACK")
        }
        
        log.debug("Posting event changed local notification for event \(eventId)")
        let local = UILocalNotification()
        local.alertBody = body
        local.userInfo = userInfo
        UIApplication.sharedApplication().presentLocalNotificationNow(local)
        
        RecentEvents.sharedInstance.setTimeOutstandingLocalNotificationPresented(NSDate(), forEventId: eventId)
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        log.debug("received local notification: \(notification)")
        
        if UIApplication.sharedApplication().applicationState != .Active {
            log.debug("Application not active, but running.")
            if let eventId = notification.userInfo?[EventUpdatedNotificationEventIDKey] as? String {
                loadActivityFeedForEventId(eventId)
            } else {
                log.error("Didn't understand local notification. \(notification)")
            }
        } else {
            // TODO: How to tell the user new item in a feed if the app is open but they aren't currently looking at that feed?
            log.warning("TODO: got local notification but not handling")
        }
        
    }
}

