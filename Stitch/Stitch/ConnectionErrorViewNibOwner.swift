//
//  ConnectionErrorView.swift
//  Stitch
//
//  Created by Doug Richardson on 10/8/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class ConnectionErrorViewNibOwner: NSObject {
    @IBOutlet var label : UILabel!
}

func showTemporaryBannerWithTitle(title : String, overView : UIView) {
    let owner = ConnectionErrorViewNibOwner()
    UINib(nibName: "ConnectionErrorView", bundle: nil).instantiateWithOwner(owner, options: nil)
    let v = owner.label
    v.text = title
    let b = overView.bounds
    v.frame = CGRectMake(0, 0, b.size.width, v.frame.size.height)
    v.alpha = 0
    overView.addSubview(v)
    
    UIView.animateWithDuration(0.25, animations: { () -> Void in
        //v.frame = CGRectMake(0, 0, b.size.width, height)
        v.alpha = 1
        }) { (_) -> Void in
            UIView.animateWithDuration(0.25, delay: 2.0, options: [], animations: { () -> Void in
                v.alpha = 0
                }, completion: { (_) -> Void in
                    v.removeFromSuperview()
            })
    }
}
