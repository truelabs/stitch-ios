//
//  PairAppleTVTableViewController.swift
//  Stitch
//
//  Created by John Hwang on 11/13/15.
//  Copyright © 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON

class PairAppleTVTableViewController: UITableViewController {

    var pairCode = ""
    
    var eventId = ""
    
    private var subscribeButton : UIBarButtonItem?
    
    private let textFieldCell = NibTableViewCellReuseIdAndType<TextFieldTableViewCell>(reuseId: "TextField")
    
    init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancel = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancel"))
        let subscribe = UIBarButtonItem(title: LocalizedString("ATV_SUBSCRIBE"), style: .Done, target: self, action: Selector("subscribe"))
        subscribeButton = subscribe
        updateSubscribeEnabled()
        navigationItem.leftBarButtonItem = cancel
        navigationItem.rightBarButtonItem = subscribe
        navigationItem.title = LocalizedString("ATV_TITLE")
        
        textFieldCell.registerNib(tableView)
    }
    
    override func viewDidAppear(animated: Bool) {
        // Google Analytics
        Analytics.sharedInstance.screenView("Apple-TV-Subscribe")
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = textFieldCell.dequeueFromTableView(tableView)
        cell.textField.placeholder = LocalizedString("ATV_PLACEHOLDER")
        cell.textField.text = pairCode
        cell.textField.clearButtonMode = .Always
        cell.textField.autocorrectionType = .No
        cell.textField.keyboardType = .NumberPad
        cell.textField.addTarget(self, action: Selector("pairCodeChanged:"), forControlEvents: .EditingChanged)
        cell.textField.becomeFirstResponder()
        return cell
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return LocalizedString("ATV_HEADER")
    }
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return LocalizedString("ATV_FOOTER")
    }

    func updateSubscribeEnabled() {
        subscribeButton?.enabled = (pairCode ?? "").characters.count > 0
    }
    
    // MARK: UIControl on change handlers
    
    func pairCodeChanged(sender : UITextField) {
        pairCode = sender.text ?? ""
        updateSubscribeEnabled()
    }
    
    func subscribe() {
        
        log.debug("Apple TV code entered: \(pairCode)")
        WebAPI.putPairWithTV(eventId, code: pairCode) { (error) -> Void in
            if let e = error {
                let titleKey : String
                titleKey = "FAILED_TO_PAIR_ATV_TITLE"
                let alert = alertWithTitle(LocalizedString(titleKey), error: e)
                self.presentViewController(alert, animated: true, completion: nil)
                //Google Analytics
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "subscribe_atv_fail", label: nil)
                return
            }
            
            //Google Analytics
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "subscribe_atv_success", label: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func cancel() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}