//
//  PlaceholderImageView.swift
//  Stitch
//
//  Created by Douglas Richardson on 5/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

final class PlaceholderImageView: UIView {
    
    var placeholder : UIImage? {
        didSet {
            imageView.image = image ?? placeholder
        }
    }
    
    var image : UIImage? {
        didSet {
            imageView.image = image ?? placeholder
        }
    }
    
    let imageView = UIImageView()

    required init() {
        super.init(frame: CGRectZero)
        addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        assert(false, "Not implemented")
    }
    
    override func layoutSubviews() {
        imageView.frame = bounds
    }
}
